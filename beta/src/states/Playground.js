// Copyright (c) 2016-2021, VIMinO
//
// This source code file is under the GPLv3 license, which can be found at:
// https://www.gnu.org/licenses/gpl-3.0.en.html
// Or the attached LICENSE file

import PIXI from 'expose-loader?PIXI!phaser-ce/build/custom/pixi.js' // eslint-disable-line
import p2 from 'expose-loader?p2!phaser-ce/build/custom/p2.js' // eslint-disable-line
import Phaser from 'expose-loader?Phaser!phaser-ce/build/custom/phaser-split.js' // eslint-disable-line

import Label from '../objects/Label'
import Player from '../objects/Player'
import { Ship, ShipManager } from '../objects/Ship'
import Background from '../objects/Background'
import StarManager from '../objects/Star'

let player, starma, upgrades, shima

const getRandomColor = function () {
  const chars = '0123456789abcdef'
  let color = ''
  for (let i = 0; i < 6; ++i) {
    color += chars[Math.floor(Math.random() * chars.length)]
  }
  return color
}

export default class Playground extends Phaser.State {
  create () {
    upgrades = {
      power: 0,
      // 'structure': 0,
      skill: 0
      // 'score': 0
    }

    this.rotate = true // Rotate to Direction

    this.bg_keys = Object.keys(window.index.background)
    this.bg_a = 0
    this.bg_b = 0
    this.bg_speed = 4
    this.background = new Background(this.game, window.index.background[this.bg_keys[this.bg_a]][this.bg_b], Phaser.ANGLE_LEFT, this.bg_speed)

    player = new Player(this.game, 0, this.game.world.centerX - 66, this.game.world.centerY, 4)
    player.warp(false)

    this.keyboard = {
      previous: { x: player.x, y: player.y + 0.5 }
    }
    this.guideCharge = 0
    this.invert = window.game.invert
    this.updatePreviousPosition = function (x, y) {
      this.keyboard.previous.x = player.position.x - x
      this.keyboard.previous.y = player.position.y - y
    }

    this.pointer = this.game.input.activePointer

    this.controler = 0 // 0 = Keyboard, 1 = Pointer

    this.switchControler = function SwitchControler (value) {
      this.controler = value
    }

    this.game.input.addMoveCallback(function Move (event, fromClick) {
      if (this.controler === 1) { return }
      if (event.withinGame) {
        this.switchControler(1)
      }
    }, this)

    starma = new StarManager(this.game, player, 1, 0)
    // console.log('A', starma.children.length)
    // starma.maximum(3)
    // console.log('B', starma.children.length)
    starma.cooldown = 0
    starma.limit = 1
    this.maxCharge = 1

    this.ships = Object.keys(window.index.ship)
    // console.log(this.ships)
    this.shipIndex = 0
    this.spriteLevel = 1
    this.ship = new Ship(this.game, window.index.ship[this.ships[this.shipIndex]], this.spriteLevel, Phaser.ANGLE_DOWN, 10, 20, 4)
    this.ship.body.collideWorldBounds = true

    shima = new ShipManager(this.game, player.direction, 10)

    this.label = new Label(this.game, 2, 2, 'M:R')
    this.label2 = new Label(this.game, 17, 2, 'D:E')
    this.label3 = new Label(this.game, 30, 2, 'U:0,0')
    this.label4 = new Label(this.game, 50, 2, 'S:1')

    this.cursors = this.game.input.keyboard.createCursorKeys()
    this.ctrl = this.game.input.keyboard.addKey(Phaser.Keyboard.CONTROL)
    this.enter = this.game.input.keyboard.addKey(Phaser.Keyboard.ENTER)
    this.backspace = this.game.input.keyboard.addKey(Phaser.Keyboard.BACKSPACE)
    this.comma = this.game.input.keyboard.addKey(Phaser.Keyboard.COMMA)
    this.showBodies = window.game.toggle[1]
    this.tab = this.game.input.keyboard.addKey(Phaser.Keyboard.TAB)
    this.period = this.game.input.keyboard.addKey(Phaser.Keyboard.PERIOD)

    this.q = this.game.input.keyboard.addKey(Phaser.Keyboard.Q)

    this.z = this.game.input.keyboard.addKey(Phaser.Keyboard.Z)
    this.x = this.game.input.keyboard.addKey(Phaser.Keyboard.X)

    this.v = this.game.input.keyboard.addKey(Phaser.Keyboard.V)
    this.b = this.game.input.keyboard.addKey(Phaser.Keyboard.B)

    // this.n = this.game.input.keyboard.addKey(Phaser.Keyboard.N)
    this.m = this.game.input.keyboard.addKey(Phaser.Keyboard.M)

    this.w = this.game.input.keyboard.addKey(Phaser.Keyboard.W)
    this.a = this.game.input.keyboard.addKey(Phaser.Keyboard.A)
    this.s = this.game.input.keyboard.addKey(Phaser.Keyboard.S)
    this.d = this.game.input.keyboard.addKey(Phaser.Keyboard.D)

    this.j = this.game.input.keyboard.addKey(Phaser.Keyboard.J)
    this.k = this.game.input.keyboard.addKey(Phaser.Keyboard.K)
    this.l = this.game.input.keyboard.addKey(Phaser.Keyboard.L)

    this.r = this.game.input.keyboard.addKey(Phaser.Keyboard.R)
    this.f = this.game.input.keyboard.addKey(Phaser.Keyboard.F)
  }

  update () {
    if (player.warping) { return }

    // One click at a time please

    if (this.enter.isDown) { this.enter.ready = true }
    if (this.enter.ready && this.enter.isUp) {
      this.enter.ready = false

      this.rotate = !this.rotate
      if (this.rotate) {
        this.label.text = 'M:R'
      } else {
        this.label.text = 'M:G'
      }
    }

    if (this.backspace.isDown) { this.backspace.ready = true }
    if (this.backspace.ready && this.backspace.isUp) {
      this.backspace.ready = false
      this.invert = !this.invert
      if (this.invert) {
        this.label2.text = 'D:I'
      } else {
        this.label2.text = 'D:E'
      }
    }

    if (this.j.isDown) { this.j.ready = true }
    if (this.j.ready && this.j.isUp) {
      this.j.ready = false
      ++upgrades.power
      if (upgrades.power > 5) { upgrades.power = 0 }
      this.label3.text = 'U:' + upgrades.power.toString() + ',' + upgrades.skill.toString()
    }

    if (this.k.isDown) { this.k.ready = true }
    if (this.k.ready && this.k.isUp) {
      this.k.ready = false
      ++upgrades.skill
      if (upgrades.skill > 5) { upgrades.skill = 0 }

      const newMaxStars = 1 + Math.ceil(upgrades.skill / window.game.diff)
      // if (upgrades.skill === 5) { newMaxStars = 5 }
      starma.maximum = newMaxStars

      starma.cooldown = Phaser.Timer.QUARTER * (6 - upgrades.skill)

      // starma.limit = upgrades.skill + 1
      player.limitGuide(upgrades.skill + 1)

      this.label3.text = 'U:' + upgrades.power.toString() + ',' + upgrades.skill.toString()
    }

    if (this.l.isDown) { this.l.ready = true }
    if (this.l.ready && this.l.isUp) {
      this.l.ready = false
      ++this.maxCharge
      if (this.maxCharge > 6) { this.maxCharge = 0 }
      this.label4.text = 'S:' + this.maxCharge.toString()
      starma.limit = this.maxCharge
    }

    if (this.tab.isDown) { this.tab.ready = true }
    if (this.tab.ready && this.tab.isUp) {
      this.tab.ready = false
      this.showBodies = !this.showBodies
    }

    if (this.period.isDown) { this.period.ready = true }
    if (this.period.ready && this.period.isUp) {
      this.period.ready = false

      const d = player.damage + 1
      player.damage = d > 2 ? 0 : d
    }

    if (this.comma.isDown) { this.comma.ready = true }
    if (this.comma.ready && this.comma.isUp) {
      this.comma.ready = false

      const newColor = parseInt(getRandomColor(), 16)
      player.color = newColor
      starma.color = newColor
    }

    // if (this.m.isDown) { this.m.ready = true }
    // if (this.m.ready && this.m.isUp) {
    //   this.m.ready = false
    //   var g = player.glow() + 0.2
    //   g = g > 1.0 ? 0 : g
    //   player.glow(g)
    // }

    if (this.v.isDown) {
      this.bg_speed = 100
    } else {
      this.bg_speed = 4
    }
    this.background.speed = this.bg_speed

    if (this.m.isDown) { this.m.ready = true }
    if (this.m.ready && this.m.isUp) {
      this.m.ready = false
      var self = this
      player.afterWarp(function () {
        self.background.warp(false)
        this.warp(false) // warp in
      })

      player.guide()
      player.glow = 0.0
      starma.decharge()
      starma.catchAll()
      shima.clear()
      self.background.warp(true)
      player.warp(true) // warp out
    }

    if (this.b.isDown) { this.b.ready = true }
    if (this.b.ready && this.b.isUp) {
      this.b.ready = false
      // var keys = Object.keys(window.index['background'])
      // var a = this.game.rnd.integerInRange(0, keys.length - 1)
      // var b = this.game.rnd.integerInRange(0, window.index['background'][keys[a]].length - 1)
      ++this.bg_b
      if (this.bg_b >= window.index.background[this.bg_keys[this.bg_a]].length) {
        this.bg_b = 0
        ++this.bg_a
        if (this.bg_a >= this.bg_keys.length) {
          this.bg_a = 0
        }
      }
      this.background.set(window.index.background[this.bg_keys[this.bg_a]][this.bg_b], true)
    }

    if (this.q.isDown) { this.state.start('Menu') }

    if (this.z.isDown) { this.z.ready = true }
    if (this.z.ready && this.z.isUp) {
      this.z.ready = false
      --this.spriteLevel
      if (this.spriteLevel < 1) {
        --this.shipIndex
        if (this.shipIndex < 0) {
          this.shipIndex = this.ships.length - 1
        }
        this.spriteLevel = window.index.ship[this.ships[this.shipIndex]].sprite.length // - 1
      }

      this.ship.reset(window.index.ship[this.ships[this.shipIndex]], this.spriteLevel) //, true)
    }

    if (this.x.isDown) { this.x.ready = true }
    if (this.x.ready && this.x.isUp) {
      this.x.ready = false
      ++this.spriteLevel
      if (this.spriteLevel > window.index.ship[this.ships[this.shipIndex]].sprite.length) {
        this.spriteLevel = 1
        ++this.shipIndex
        if (this.shipIndex >= this.ships.length) {
          this.shipIndex = 0
        }
      }

      this.ship.reset(window.index.ship[this.ships[this.shipIndex]], this.spriteLevel) //, true)
    }

    // Player Movement

    var x = 0
    var y = 0

    if (this.controler === 0) {
      player.stop()
    }

    if (this.pointer.withinGame && this.controler === 1) {
      if (Math.abs(player.x - this.pointer.x) > 2 || Math.abs(player.y - this.pointer.y) > 2) {
        player.moveToPointer(player.speed)
      } else {
        player.stop()
      }
    }

    if (this.cursors.up.isDown) {
      // console.log('up', player.angle)
      if (this.rotate) { // Rotate Ship according to its Direction
        // this.background.direction(Phaser.ANGLE_DOWN)
        this.background.beforeSet(() => {
          player.direction = Phaser.ANGLE_UP
        })
        this.background.set(undefined, true, Phaser.ANGLE_DOWN)
      }
      this.switchControler(0)
      y = -1
    } else if (this.cursors.down.isDown) {
      // console.log('down', player.angle)
      if (this.rotate) { // Rotate Ship according to its Direction
        // this.background.direction(Phaser.ANGLE_UP)
        this.background.beforeSet(() => {
          player.direction = Phaser.ANGLE_DOWN
        })
        this.background.set(undefined, true, Phaser.ANGLE_UP)
      }
      this.switchControler(0)
      y = 1
    }

    if (this.cursors.left.isDown) {
      // console.log('left', player.angle)
      if (this.rotate) { // Rotate Ship according to its Direction
        this.background.beforeSet(() => {
          player.direction = Phaser.ANGLE_LEFT
        })
        // this.background.direction(Phaser.ANGLE_RIGHT)
        this.background.set(undefined, true, Phaser.ANGLE_RIGHT)
      }
      this.switchControler(0)
      x = -1
    } else if (this.cursors.right.isDown) {
      // console.log('right', player.angle)
      if (this.rotate) { // Rotate Ship according to its Direction
        this.background.beforeSet(() => {
          player.direction = Phaser.ANGLE_RIGHT
        })
        // this.background.direction(Phaser.ANGLE_LEFT)
        this.background.set(undefined, true, Phaser.ANGLE_LEFT)
      }
      this.switchControler(0)
      x = 1
    }

    if (this.controler === 0 && (x !== 0 || y !== 0)) {
      if (x !== 0 || y !== 0) {
        this.updatePreviousPosition(x, y)
      }
      if (this.rotate) {
        shima.direction = player.direction
      } else {
        player.move(x, y)
      }
    }

    const angle = starma.getAngle(this.controler === 1 ? this.pointer : null, this.keyboard.previous, this.invert)

    if (!this.rotate) {
      if (this.ctrl.isDown || this.pointer.isDown) {
        this.throwReady = true

        if (this.maxCharge === 0) {
          player.guide(angle, 0)
          return
        }

        // ++this.guideCharge
        // if (this.guideCharge > this.maxCharge * 10) { this.guideCharge = this.maxCharge * 10 }
        // let value = Math.floor(this.guideCharge / 10) // 0 to 6 (S)
        // console.log(value / this.maxCharge)
        // player.glow(value / this.maxCharge) // 0 to 1
        // starma.charge = value
        if (starma && starma.charge) {
          starma.charge(1) // Charge with Infinite Energy
        } else {
          console.log('Star Manager is missing!')
        }
        // starma.charge()
        // console.log(starma.charged)
        player.guide(angle, starma.charged)
      } else if (this.throwReady && (this.ctrl.isUp || this.pointer.isUp)) {
        this.throwReady = false
        starma.throw(angle, upgrades.power, upgrades.skill)
        player.guide()
        // player.glow(0.0)
        // this.guideCharge = 9
      }
    }

    // Ship Movement

    x = 0
    y = 0

    if (this.w.isDown) {
      this.ship.direction = Phaser.ANGLE_UP
      y = -1
    } else if (this.s.isDown) {
      this.ship.direction = Phaser.ANGLE_DOWN
      y = 1
    }

    if (this.a.isDown) {
      this.ship.direction = Phaser.ANGLE_LEFT
      x = -1
    } else if (this.d.isDown) {
      this.ship.direction = Phaser.ANGLE_RIGHT
      x = 1
    }

    this.ship.move(x, y)

    if (this.r.isDown) {
      this.clearReady = true
    }

    if (this.clearReady && this.r.isUp) {
      shima.clear()
      console.log('Clear all ships')
      this.clearReady = false
    }

    if (this.f.isDown) {
      const waveLevel = this.game.rnd.integerInRange(1, 6)
      shima.sendWave(waveLevel)
      // this.sendReady = true
    }

    // if (this.sendReady && this.f.isUp) {
    //   let waveLevel = this.game.rnd.integerInRange(1, 24)
    //   shima.sendWave(waveLevel, true)
    //   console.log('Send wave level', waveLevel)
    //   this.sendReady = false
    // }
  }

  render () {
    if (this.showBodies) {
      this.game.debug.body(player)
      this.game.debug.body(this.ship)
      starma.forEach((star) => {
        if (star.alive) {
          this.game.debug.body(star)
        }
      })
      shima.forEach((ship) => {
        if (ship.alive) {
          this.game.debug.body(ship)
        }
      })
    }
  }
}
