// Copyright (c) 2016-2021, VIMinO
//
// This source code file is under the GPLv3 license, which can be found at:
// https://www.gnu.org/licenses/gpl-3.0.en.html
// Or the attached LICENSE file
// PhaserJS CE Libraries
import PIXI from 'expose-loader?PIXI!phaser-ce/build/custom/pixi.js' // eslint-disable-line
import p2 from 'expose-loader?p2!phaser-ce/build/custom/p2.js' // eslint-disable-line
import Phaser from 'expose-loader?Phaser!phaser-ce/build/custom/phaser-split.js' // eslint-disable-line

import Background from '../objects/Background'
import Label from '../objects/Label'
import Gauge from '../objects/Gauge'
import Player from '../objects/Player'
import StarManager from '../objects/Star'
import ShipManager from '../objects/Ship'
// // import PickupManager from '../objects/Pickup'

import GameOver from '../objects/GameOver'
import ShopSlider from '../objects/ShopSlider'

// Constants
Math.PIx180 = 180.0 * Math.PI

window.level = 1

// Game variables
let upgrades, slider
let background, gauge, player, starma, shima
let keyboard, pointer

export default class Game extends Phaser.State {
  constructor (game) {
    super(game)

    slider = {}
  }

  preload () {
    // Unset these variables when (re)starting the state
    upgrades = undefined
    background = undefined
    gauge = undefined
    player = undefined
    starma = undefined
    shima = undefined
    keyboard = undefined
    pointer = undefined

    if (window.game.toggle[0]) {
      this.game.time.advancedTiming = true
    }
  }

  create () {
    upgrades = {
      power: 0,
      structure: 0,
      skill: 0,
      score: -1
    }

    this.updateStats = function UpdateStats (charge = false) {
      // gauge.size = 10 + 2 * upgrades.structure
      // player.damage(0)

      // this.eneGenDelay = Math.round((4 * window.game.diff) - (upgrades.power * 0.5))
      // this.shiGenDelay = Math.round((12 * window.game.diff) - (upgrades.structure * 0.5))

      // starma.cooldown((0.5 - (0.05 * upgrades.skill)) * window.game.diff)
      // // Skill 0,1: 2 Stars, 2,3: 3 Stars, 4,5: 4 Stars, 6: 5 Stars
      // let newMaxStars = Math.floor(upgrades.skill / 2) + 2 // + (3 - window.game.diff)
      // if (upgrades.skill === 5) { newMaxStars = 5 }
      // starma.maximum(newMaxStars)

      // player.limitGuide(upgrades.skill < 2 ? 2 : upgrades.skill + 1)
      // // starma.limit(Math.floor(1 + (3 - window.game.diff) + (upgrades.skill / 2)))

      const modifier = 0.5 * window.game.diff
      const energyRegenDelay = (8 - upgrades.skill) * modifier
      const shieldRegenDelay = (12 - upgrades.structure) * modifier

      gauge.size = 10 + 2 * upgrades.structure
      gauge.setEnergyRegen(1, energyRegenDelay)
      gauge.setShieldRegen(1, shieldRegenDelay)

      player.damage = 0

      gauge.overchargeWarmup = Phaser.Timer.SECOND * 2
      gauge.overchargeDelay = Phaser.Timer.SECOND *
        (6 - upgrades.structure) + (Phaser.Timer.HALF * window.game.diff)

      // Skill 0,1: 2 Stars, 2,3: 3 Stars, 4,5: 4 Stars (+1/2 on Normal/Easy)
      starma.maximum = 2 + Math.floor(upgrades.skill / 2) + (3 - window.game.diff)
      starma.cooldown = Phaser.Timer.QUARTER * (6 - upgrades.skill) // + (100 * (window.game.diff - 1)) // TODO
      starma.limit = Math.floor(upgrades.skill / 2) + 2 // + (3 - window.game.diff) // upgrades.skill + 1 // TODO
      player.limitGuide(upgrades.skill < 2 ? 2 : upgrades.skill + 1)
    }

    // Background

    window.scenario = {
      planet: false,
      index: this.game.rnd.integerInRange(0,
        window.index.background.space.length - 1)
    }

    this.exitScenario = function exitScenario () {
      // console.log('afterWarp: ', window.scenario.cycle, window.scenario.planet)

      starma.decharge()
      starma.catchAll()
      shima.clear()

      player.afterWarp(() => {
        if (window.scenario.planet) {
          upgrades.score = this.score.get()
          slider.shop.show(upgrades, true)
        } else {
          this.switchScenario(shima)
          player.warp(false)
          background.warp(false)
        }
      })

      player.warp(true)
      background.warp(true)
    }

    this.switchScenario = function switchScenario (ShipManager) {
      if (window.scenario.cycle === undefined) {
        window.scenario.cycle = 0
      } else {
        window.scenario.cycle += 1
        if (window.scenario.cycle === 2) {
          window.scenario.cycle = 0
        }
      }

      window.scenario.planet = (window.scenario.cycle === 1)

      // Choose a new Background
      const bgType = window.scenario.planet ? 'world' : 'space'
      let bgIndex = this.game.rnd.integerInRange(0, window.index.background[bgType].length - 1)
      while (window.index.background[bgType][bgIndex] === window.scenario.index) {
        bgIndex = this.game.rnd.integerInRange(0, window.index.background[bgType].length - 1)
      }
      window.scenario.index = bgIndex

      let newDir = this.game.rnd.integerInRange(0, 3) * 90
      if (window.scenario.cycle === 0) {
        if (player !== undefined) {
          const oldDir = player.direction
          while (oldDir === newDir) {
            newDir = this.game.rnd.integerInRange(0, 3) * 90
          }
        }
      } else {
        if (player !== undefined) {
          newDir = player.direction
        }
      }

      if (background === undefined) {
        background = new Background(
          this.game,
          window.index.background[bgType][bgIndex],
          newDir - 180,
          4 * window.game.diff
        )
      } else {
        background.afterSet(() => {
          this.transition = false
        })
        // this.set = function set (sprite = undefined, animate = false, newdirection = undefined, newspeed = undefined) {
        background.set(
          window.index.background[bgType][bgIndex],
          false, // window.scenario.cycle === 1,
          newDir - 180.0
        )
      }

      if (player === undefined) {
        player = new Player(this.game, newDir, 34, 43, 15)
      } else {
        player.game = this.game
        player.direction = newDir
        player.position.setTo(34, 43)
        player.speed = 15
      }

      /*
      if (window.scenario.cycle === undefined) {
        player.warp(false) // warp in
        background.warp(false) // warp in
        this.game.time.events.add()
      } else if (window.scenario.cycle === 0) {
        background.set(
          window.index.background[bgType][bgIndex],
          false,
          newDir - 180.0
        )

        player.afterWarp(() => {
          this.transition = false
          player.afterWarp() // clear the afterWarp action
        })
        player.warp(false) // warp in
        background.warp(false) // warp in
      } else {
        background.afterSet(() => {
          this.transition = false
        })
      }
*/
      player.warp(false) // warp in
      background.warp(false) // warp in

      window.audio.stopAll()
      if (window.scenario.planet) { // bgIndex // TODO
        window.audio.loop('msWorld')
      } else {
        window.audio.loop('msSpace')
      }

      // this.waveLimit += 2 // *= 2 // HACK

      if (ShipManager) {
        console.log('SendWave', window.level)
        ShipManager.sendWave(window.level)
      }
    }

    // Switch Scenario and create the Background and Player if they don't exist
    this.switchScenario()

    // Add a Star Manager (requires a Player)
    starma = new StarManager(this.game, player)
    starma.onThrow(() => {
      if (!slider.gameover.visible) {
        window.audio.play('fxThrow')
      }
    })
    starma.onCatch(() => {
      if (!slider.gameover.visible) {
        window.audio.play('fxCatch')
      }
    })

    // Damage the Player and update the sprite according to remaining Armor
    this.damagePlayer = function DamagePlayer (value) {
      if (window.game.toggle[7]) { return }
      // console.log('damagePlayer', value)
      const dead = gauge.damage(value)

      // Update the Players' visible damage [0,2]
      const size = gauge.size
      const armor = gauge.armor
      if (armor > (size * (2 / 3))) {
        player.damage = 0
      } else if (armor > (size * (1 / 3))) {
        player.damage = 1
      } else { // if (armor <= (size * (1 / 3))) {
        player.damage = 2
      }

      return dead
    }

    shima = new ShipManager(this.game, player.direction, 10)
    shima.onDeath((x, y, type, level) => {
      if (type > 1) {
        const curse = this.game.rnd.integerInRange(0, 9)
        if (curse === 9) {
          window.audio.play('fxCurses')
        }
      }
      window.audio.play('fxExplosion')

      const explosion = this.game.add.sprite(x, y, 'explosion')
      explosion.anchor.setTo(0.5, 0.5)
      explosion.animations.add('boom', [0, 1, 2, 3, 4], 5, false).onComplete.add(function () {
        explosion.kill()
      }, this)
      explosion.animations.play('boom')
    })

    // Place the Player group over the Background and Stars/Projectiles
    this.world.bringToTop(player)
    this.world.bringToTop(shima)

    // UI

    this.ui = this.game.add.group()
    this.add.existing(this.ui)

    gauge = new Gauge(this.game, 26, 56)
    this.ui.add(gauge)

    this.updateStats()

    this.score = new Label(this.game, 4, 4, '0')
    this.ui.add(this.score)

    // Input Handlers

    keyboard = this.game.input.keyboard.addKeys({
      up: Phaser.KeyCode.UP,
      down: Phaser.KeyCode.DOWN,
      left: Phaser.KeyCode.LEFT,
      right: Phaser.KeyCode.RIGHT,
      w: Phaser.KeyCode.W,
      s: Phaser.KeyCode.S,
      a: Phaser.KeyCode.A,
      d: Phaser.KeyCode.D,
      ctrl: Phaser.KeyCode.CONTROL,

      z: Phaser.KeyCode.Z,
      x: Phaser.KeyCode.X,
      c: Phaser.KeyCode.C,
      v: Phaser.KeyCode.V,
      b: Phaser.KeyCode.B,
      n: Phaser.KeyCode.N
    })
    this.game.input.keyboard.addKeyCapture(Object.keys(keyboard).map(
      key => keyboard[key].keyCode
    ))
    keyboard.last = { x: player.x, y: player.y }
    keyboard.setLast = function KeyboardLastPosition (x, y) {
      keyboard.last = { x: x, y: y }
    }

    // function Keyboard (event) {
    //   if (this.controler === 0) { return }
    //
    //   console.log(event)
    // }
    // this.game.input.onDownCallback = Keyboard

    pointer = this.game.input.activePointer

    // This determines which controler moves the Player
    this.controler = 0 // 0 = Keyboard, 1 = Pointer

    // Overcharge timer
    this.lastInputTime = this.game.time.now // last time there was a controler change

    // Have a function to switch the controler (used to cancel Overcharge)
    this.switchControler = function SwitchControler (value, update = true) {
      this.controler = value
      if (update) {
        this.lastInputTime = this.game.time.now
      }
    }

    // Switch the controler to the Pointer if it moves/clicks
    this.game.input.addMoveCallback(function Move (event, fromClick) {
      this.lastInputTime = this.game.time.now
      if (this.controler === 1) { return }
      if (event.withinGame) {
        this.switchControler(1, false)
      }
    }, this)

    // Sliders

    if (slider.gameover) {
      slider.gameover.reset()
    } else {
      slider.gameover = GameOver(this.game)
      slider.gameover.onShow(() => {
        window.audio.stopAll()
        window.audio.loop('msGameOver')
      })
      slider.gameover.onHide(() => {
        window.audio.stopAll()
      })
    }

    if (slider.shop) {
      slider.shop.reset()
    } else {
      slider.shop = ShopSlider(this.game)
      slider.shop.onShow(() => {
        window.audio.stopAll()
        window.audio.play('fxSlide')
        window.audio.loop('msShop')

        starma.decharge()
        starma.catchAll()
        player.stop()
        background.stop()

        this.transition = false
      })
      // slider.shop.afterShow(() => { // TODO
      //  background.speed = 0
      // })
      slider.shop.onHide((upgs, advance) => {
        window.audio.stopAll()
        window.audio.play('fxUnslide')

        upgrades.power = upgs.power
        upgrades.structure = upgs.structure
        upgrades.skill = upgs.skill
        this.score.set(upgs.score)

        this.updateStats()

        starma.decharge()
        starma.catchAll()
        shima.clear()

        if (advance) {
          window.level += 1
          this.switchScenario(shima)
        } else {
          window.audio.stopAll()
          if (window.scenario.planet) { // bgIndex // TODO
            window.audio.loop('msWorld')
          } else {
            window.audio.loop('msSpace')
          }
        }
      })
    }

    // Optional frame counter

    if (window.game.toggle[0]) {
      this.counter = new Label(this.game, 61, 4)
      this.counter.anchor.setTo(1, 0)
      this.ui.add(this.counter)
    }

    // red
    // player.color = 0xff5555
    // starma.color = 0xff5555

    // yellow
    // player.color = 0xffaa55
    // starma.color = 0xffaa55

    // green
    // player.color = 0x55ff55
    // starma.color = 0x55ff55

    // blue
    // player.color = 0x55aaff
    // starmacolor = 0x55aaff
  }

  getRandomColor () {
    const chars = '789abcdef' // 0123456
    let color = ''
    for (let i = 0; i < 6; ++i) {
      color += chars[Math.floor(Math.random() * chars.length)]
    }
    return color
  }

  update () {
    if (window.game.toggle[0]) {
      this.counter.text = this.game.time.fps
    }

    if (player.warping || slider.shop.active || slider.gameover.visible) {
      player.stop()
      return
    }

    // /////////////////////////////////////////////////////////////////////

    // upgrades.score = 10000
    // slider['shop'].show(upgrades)

    // if (this.game.input.activePointer.isDown) {
    //   // gauge.shield -= 1
    //   gauge.energy -= 1
    //   let dead = this.damagePlayer(1)
    //   if (dead && !slider['gameover'].visible) {
    //     slider['gameover'].show()
    //   }
    //
    //   // gauge.toggleOvercharge(true)
    // }

    // /////////////////////////////////////////////////////////////////////

    // Handle Input

    // this.calculateAngle()

    // If the Player is Alive
    if (player.alive) {
      // Follow the Pointer if it moved/clicked/tapped
      if (pointer.withinGame && this.controler === 1) {
        // Move the Player to the Pointer
        if (Math.abs(player.x - pointer.x) > 2 || Math.abs(player.y - pointer.y) > 2) {
          player.moveToPointer(player.speed)
        } else {
          player.stop()
        }
      }

      // Follow the Keyboard if it's pressed down and stop otherwise

      let x = 0
      let y = 0

      if (this.controler === 0) {
        player.stop()
      }

      if (keyboard.up.isDown || keyboard.w.isDown) {
        this.switchControler(0)
        y = -1
      } else if (keyboard.down.isDown || keyboard.s.isDown) {
        this.switchControler(0)
        y = 1
      }

      if (keyboard.left.isDown || keyboard.a.isDown) {
        this.switchControler(0)
        x = -1
      } else if (keyboard.right.isDown || keyboard.d.isDown) {
        this.switchControler(0)
        x = 1
      }

      if (this.controler === 0 && (x !== 0 || y !== 0)) {
        player.move(x, y)
      }

      const angle = starma.getAngle(this.controler === 1 ? pointer : null, keyboard.last, window.game.invert)
      if (pointer.isDown || keyboard.ctrl.isDown) {
        this.throwReady = true

        if (window.game.toggle[7]) {
          starma.charge(gauge.energy)
        } else {
          gauge.energy -= starma.charge(gauge.energy)
        }

        this.lastInputTime = this.game.time.now

        // Easy: Full guide, Normal: Direction guide, Hard: Nothing
        if (starma.charged > 0) {
          if (window.game.diff < 3) {
            player.guide(angle, starma.charged)
          } else {
            player.guide(angle, 0)
          }
        }
      }

      if (pointer.isUp && keyboard.ctrl.isUp) {
        if (this.throwReady) {
          this.throwReady = false
          if (starma.throw(angle, upgrades.power, upgrades.skill)) {
            player.guide()
            this.lastInputTime = this.game.time.now
          }
        }
      }

      // Fade-in then Overcharge after 1 second

      const idleInput = this.game.time.now - this.lastInputTime
      if (idleInput > gauge.overchargeWarmup) {
        const overtime = (idleInput - Phaser.Timer.SECOND)
        if (overtime > gauge.overchargeDelay) {
          player.glow = 1.0
          gauge.overcharge = 1.0
        } else {
          player.glow = overtime / gauge.overchargeDelay
          gauge.overcharge = overtime / gauge.overchargeDelay
        }
      } else {
        player.glow = 0.0
        gauge.overcharge = 0.0
      }

      // Collisions

      // Check for Player-Star collision (if it can be catched)
      this.game.physics.arcade.overlap(player, starma, function (ship, star) {
        if (star.catchable) {
          gauge.energy += star.catch(false)
          // this.score.add(Math.floor(energy / 2.0))
        }
      })

      // Check for Enemy-Star collision
      this.game.physics.arcade.overlap(shima, starma, function (ship, star) {
        if (!star.alive) { return }

        const energy = star.energy
        const structure = ship.structure

        star.hit(structure)

        let prize = 0

        // If it destroys the enemy
        if (ship.damage(energy)) {
          prize = 5 * ship.level * ship.type
          this.killCount += ship.level
        } else {
          prize = energy
        }

        this.score.add(prize * window.game.diff)
      }, null, this)

      // Check for Enemy-Player collisions

      this.game.physics.arcade.overlap(shima, player, function (good, bad) {
        const health = gauge.defense

        // Damage the Player (overcharging multiplies the damage)
        if (this.damagePlayer(bad.structure * (gauge.overcharge ? 1 + window.game.diff : 1))) {
          player.die()
          starma.decharge()
          this.score.alpha = 1
          slider.gameover.show()
        }

        // Damage the Enemy
        if (bad.damage(health)) {
          // this.playRandomExplosion(bad.type === 0)
          let prize = bad.level + bad.type
          if (bad.type === 6) {
            prize += 10 * bad.level
          }
          this.score.add(prize)

          this.killCount += bad.level
        }
      }, null, this)

      //     if (badguy.damage(gauge.defense)) {
      //       this.playRandomExplosion(badguy.getType() === 0)
      //       let prize = badguy.getLevel() + badguy.getType()
      //       if (badguy.getType() === 6) {
      //         prize += 10 * badguy.getLevel()
      //       }
      //       self.score.add(prize)
      //
      //       this.killCount += badguy.level
      //       // console.log('kill count', this.killCount)
      //       // if (this.killCount > this.killsRequired) {
      //       //   this.pickup.pop()
      //       //   this.killCount -= this.killsRequired
      //       // }
      //     }
      //   }, null, this)
      //
      //   for (let e in shima.children) { // enema.children) {
      //     let enemy = shima.children[e] // enema.children[e]
      //
      //     let isAlive = shima.health() > 0.0 // enemy.getHealth() > 0.0
      //     // Kill enemies that went too far beyond the Scrreen, given their direction
      //     if (isAlive) {
      //       // console.log(enemy.angle, Phaser.ANGLE_UP, enemy.y, 0 - enemy.height)
      //       // console.log(enemy.angle, enemy.x, enemy.y)
      //       if (enemy.direction === Phaser.ANGLE_RIGHT && enemy.x > 74 + enemy.width) {
      //         enemy.damage()
      //       } else if (enemy.direction === Phaser.ANGLE_DOWN && enemy.y > 74 + enemy.height) {
      //         enemy.damage()
      //       } else if (enemy.direction === Phaser.ANGLE_LEFT && enemy.x < -10 - enemy.width) {
      //         enemy.damage()
      //       } else if (enemy.direction === Phaser.ANGLE_UP && enemy.y < -10 - enemy.height) {
      //         enemy.damage()
      //       }
      //     }
      //     // if (isAlive && enemy.y > 64 + enemy.height) {
      //     //   enemy.damage()
      //     // }
      //
      //     for (let j in enemy.weapons) {
      //       let weapon = enemy.weapons[j]
      //
      //       // If the Enemy is dead, destroy all Weapons and respective Bullets
      //       if (!isAlive) {
      //         weapon.destroy()
      //         continue
      //       }
      //
      //       // Check for EnemyBullet-Star collisions
      //       self.game.physics.arcade.overlap(weapon.bullets, starma, function (bullet, star) {
      //         if (!star.alive) {
      //           return
      //         }
      //
      //         // console.log('bullet-star', bullet.parent.level, star.getEnergy())
      //         // console.log(bullet)
      //         // // let bulletEnergy = bullet.level // Math.round(enemy.getLevel() / 2)
      //         //
      //         // let starEn = star.getEnergy()
      //         //
      //         // x('bullet-star', bullet.energy, starEn)
      //
      //         // if (starEn > bullet.health) {
      //         //   bullet.kill()
      //         //   star.catch(bullet.health)
      //         // } else {
      //         //   bullet.energy -= starEn
      //         // }
      //
      //         bullet.kill()
      //         star.gain(1.0)
      //       })
      //
      //       // Check for EnemyBullet-Player collisions
      //       self.game.physics.arcade.overlap(weapon.bullets, player, function (goodguy, bullet) {
      //         // console.log('bullet-player', gauge.armor + gauge.shield, bullet.health)
      //
      //         if (self.damagePlayer(1)) { // bullet.health)) {
      //           self.playRandomExplosion()
      //           goodguy.die()
      //           starma.throw()
      //
      //           starma.energy = 0
      //           gauge.visible = false
      //           self.score.setAlpha(1.0)
      //
      //           // self.msSpace.stop()
      //           // self.msWorld.stop()
      //           // self.msGameOver.loopFull()
      //           window.audio.stopAll()
      //           window.audio.loop('msGameOver')
      //
      //           // self.fxGameOver.play()
      //           self.GameOverSlider.show()
      //         } else {
      //           bullet.kill()
      //         }
      //       })
      //     }
      //
      //     if (!isAlive) {
      //       // Clean Enemies up
      //       enema.remove(enemy)
      //       enemy.destroy()
      //     } else {
      //       if (player.alive) {
      //         // Fire all Enemy weapons
      //         enemy.fire()
      //       }
      //     }
      //   }
      //
      //   if (self.pickup.alive && player.touches(self.pickup)) {
      //     // console.log(self.pickup.getType())
      //     switch (self.pickup.getType()) {
      //       case 0:
      //         gauge.shield += 2
      //         // self.fxPickup1.play()
      //         window.audio.play('fxPickup1')
      //         self.score.add(4)
      //         break
      //       case 1:
      //         gauge.shield += 4
      //         // self.fxPickup1.play()
      //         window.audio.play('fxPickup1')
      //         self.score.add(8)
      //         break
      //       case 2:
      //         gauge.shield += 6
      //         // self.fxPickup1.play()
      //         window.audio.play('fxPickup1')
      //         self.score.add(12)
      //         break
      //       case 3:
      //         gauge.energy += 2
      //         // self.fxPickup2.play()
      //         window.audio.play('fxPickup2')
      //         self.score.add(4)
      //         break
      //       case 4:
      //         gauge.energy += 4
      //         // self.fxPickup2.play()
      //         window.audio.play('fxPickup2')
      //         self.score.add(8)
      //         break
      //       case 5:
      //         gauge.energy += 6
      //         // self.fxPickup2.play()
      //         window.audio.play('fxPickup2')
      //         self.score.add(12)
      //         break
      //     }
      //
      //     self.pickup.stop()
      //   }
      //
      //   // Send a new Wave if there are no Enemies
      //   if (enema.children.length === 0 && player.alive && !player.warping) {
      //     self.sendWave()
      //   }
      //
      //   self.game.world.bringToTop(self.ui)
      //   self.game.world.bringToTop(self.shopSlider)
      //   self.game.world.bringToTop(self.GameOverSlider)
      // }

      if (keyboard.c.isDown) {
        this.colorReady = true
      }

      if (window.game.toggle[3]) {
        if (this.colorReady && keyboard.c.isUp) {
          const color = this.getRandomColor()
          const newColor = parseInt(color, 16)
          console.log('newColor', '0x' + color.toString())
          player.color = newColor
          starma.color = newColor
          this.colorReady = false
        }

        if (keyboard.v.isDown) {
          this.clearReady = true
        }

        if (this.clearReady && keyboard.v.isUp) {
          shima.clear()
          this.clearReady = false
        }

        if (keyboard.b.isDown) {
          shima.sendWave(1) // , true)
          // this.sendReady = false
        }

        if (keyboard.n.isDown) {
          if (!slider.shop.active) {
            upgrades.score = this.score.get()
            slider.shop.show(upgrades, false)
          }
        }

        if (keyboard.z.isDown) {
          gauge.armor += 1
          gauge.shield += 1
        }

        if (keyboard.x.isDown) {
          gauge.energy += 1
        }
      }
    }

    if (window.level === 1 && upgrades.score < 0) {
      upgrades.score = 0
      shima.sendWave(window.level)
    }

    if (shima.count < 1) {
      window.level += 1
      shima.level = window.level
      this.exitScenario()
    }
  }

  render () {
    if (window.game.toggle[2]) {
      if (player.alive) {
        this.game.debug.body(player)
      }

      starma.forEach((star) => {
        if (star.alive) {
          this.game.debug.body(star)
        }
      })

      shima.forEach((ship) => {
        if (ship.alive) {
          this.game.debug.body(ship)
        }
      })
    }
  }
}
