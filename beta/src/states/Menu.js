// A retro shoot'em up where you pilot a Star Juggler (Spacecraft)
//
// Copyright (c) 2016-2021, VIMinO
//
// This source code file is under the GPLv3 license, found at https://www.gnu.org/licenses/gpl-3.0.en.html
// As well as the attached LICENSE file

import PIXI from 'expose-loader?PIXI!phaser-ce/build/custom/pixi.js' // eslint-disable-line
import p2 from 'expose-loader?p2!phaser-ce/build/custom/p2.js' // eslint-disable-line
import Phaser from 'expose-loader?Phaser!phaser-ce/build/custom/phaser-split.js' // eslint-disable-line

import Label from '../objects/Label'
import Background from '../objects/Background'
import OptionSlider from '../objects/OptionSlider'
import AboutSlider from '../objects/AboutSlider'

export default class Menu extends Phaser.State {
  create () {
    // Add a Background (choose a random index if it's not set)
    if (!window.scenario) {
      window.scenario = {
        planet: false,
        index: this.game.rnd.integerInRange(0, window.index.background.space.length - 1)
      }
    }
    this.background = new Background(this.game, window.index.background.space[window.scenario.index], Phaser.ANGLE_DOWN)
    // Stars

    this.stars = []
    for (let i = 0; i < 60; ++i) {
      const size = this.game.rnd.weightedPick([1, 2, 3, 4, 5, 6, 7])

      const star = this.game.add.sprite(0, 0, 'ui')
      star.frameName = 'star' + size.toString()
      star.anchor.setTo(0.5, 0.5)

      star.alpha = 0.5
      star.speed = this.game.rnd.realInRange(0.2, 0.6)
      this.shootStar(star)
      this.stars.push(star)
    }

    // Version label

    this.version = new Label(this.game, 2, 2)
    this.version.set('0.8.9')
    this.version.alpha = 0.1

    this.version.inputEnabled = true
    this.version.events.onInputDown.add(this.clickVersion, this)
    this.version.events.onInputUp.add(this.clickVersion, this)
    this.version.events.onInputOut.add(() => { this.clickVersion(null) }, this)

    // Title

    this.title = this.game.add.sprite(5, 4, 'ui') // window.game.world.centerX
    this.title.frameName = 'title1'
    // this.title.anchor.setTo(0, 0)
    this.title.wait = 0

    // Play Button

    this.play = this.game.add.group()
    this.play.position.setTo(5, 38)

    this.playButton = this.game.add.sprite(0, 0, 'ui')
    this.playButton.frameName = 'play'
    this.play.add(this.playButton)

    this.playPattern = this.game.add.sprite(1, 1, 'ui')
    this.playPattern.frameName = 'playPat1'
    this.playPattern.alpha = 0.4
    this.play.add(this.playPattern)
    this.play.pattern = 1

    this.playButton.inputEnabled = true
    this.playButton.events.onInputDown.add(this.clickPlay, this)
    this.playButton.events.onInputUp.add(this.clickPlay, this)
    this.playButton.events.onInputOut.add(() => { this.clickPlay(null) }, this)

    // Options Button

    this.options = this.game.add.group()
    this.options.position.setTo(35, 38)

    this.optionsButton = this.game.add.sprite(0, 0, 'ui')
    this.optionsButton.frameName = 'options'
    this.options.add(this.optionsButton)

    this.optionsPattern = this.game.add.sprite(1, 1, 'ui')
    this.optionsPattern.frameName = 'optionsPat1'
    this.optionsPattern.alpha = 0.3
    this.options.add(this.optionsPattern)
    this.options.pattern = 1

    this.optionsButton.inputEnabled = true
    this.optionsButton.events.onInputDown.add(this.clickOptions, this)
    this.optionsButton.events.onInputUp.add(this.clickOptions, this)
    this.optionsButton.events.onInputOut.add(
      () => { this.clickOptions(null) },
      this)

    window.audio.loop('msMenu')

    // Sliders

    const slideIn = () => {
      // this.fxSlide.play()
      window.audio.play('fxSlide')
    }
    const slideOut = () => {
      // this.fxUnslide.play()
      window.audio.play('fxUnslide')
    }

    if (this.optionSlider) {
      this.optionSlider.reset()
    } else {
      this.optionSlider = new OptionSlider(this)
      this.optionSlider.onShow(slideIn)
      this.optionSlider.onHide(slideOut)
    }

    if (this.aboutSlider) {
      this.aboutSlider.reset()
    } else {
      this.aboutSlider = AboutSlider(this)
      this.aboutSlider.onShow(slideIn)
      this.aboutSlider.onHide(slideOut)
    }

    // Timers

    this.game.time.events.loop(40, this.animateStars, this)
    this.game.time.events.loop(160, this.animate, this)
    this.game.time.events.loop(Phaser.Timer.SECOND, this.animateTitle, this)
  }

  animate () {
    if (this.optionSlider.active || this.aboutSlider.active) {
      this.background.stop()
      return
    }

    this.background.scroll()

    this.play.pattern++
    if (this.play.pattern > 4) this.play.pattern = 1
    this.playPattern.frameName = ('playPat' + this.play.pattern.toString())

    this.options.pattern++
    if (this.options.pattern > 4) this.options.pattern = 1
    this.optionsPattern.frameName = ('optionsPat' + this.options.pattern.toString())
  }

  animateStars () {
    if (this.optionSlider.active || this.aboutSlider.active) {
      return
    }

    for (const i in this.stars) {
      this.stars[i].y += this.stars[i].speed

      if (this.stars[i].y > 80) {
        this.shootStar(this.stars[i])
      }
    }
  }

  animateTitle () {
    if (this.optionSlider.active || this.aboutSlider.active) {
      return
    }

    if (this.title.frameName === 'title1') {
      this.title.frameName = 'title2'
    } else {
      this.title.frameName = 'title1'
    }
  }

  shootStar (star) {
    star.x = this.game.rnd.integerInRange(0, 64)
    star.y = -this.game.rnd.integerInRange(0, 120)
  }

  clickVersion (element, event) {
    if (this.optionSlider.preactive || this.aboutSlider.preactive) {
      return
    }

    if (event != null && event.isDown) {
      this.version.alpha = 0.4
      this.version.cancel = false
    } else {
      if (event != null && !this.version.cancel) {
        this.aboutSlider.show()
      } else {
        this.version.cancel = true
      }

      this.version.alpha = 0.1
    }
  }

  clickPlay (element, event) {
    if (this.optionSlider.preactive || this.aboutSlider.preactive) {
      return
    }

    if (event != null && event.isDown) {
      this.playButton.frameName = 'playP'
      this.playPattern.alpha = 0.8
      this.playButton.cancel = false
    } else {
      if (event != null && !this.playButton.cancel) {
        // this.msMenu.stop()

        window.audio.stop('msMenu')
        window.scenario = undefined
        this.state.start(window.game.toggle[1] ? 'Playground' : 'Game')
      } else {
        this.playButton.cancel = true
      }

      this.playButton.frameName = 'play'
      this.playPattern.alpha = 0.4
    }
  }

  clickOptions (element, event) {
    if (this.optionSlider.preactive || this.aboutSlider.preactive) {
      return
    }

    if (event != null && event.isDown) {
      this.optionsButton.frameName = 'optionsP'
      this.optionsPattern.alpha = 0.8
      this.optionsButton.cancel = false
    } else {
      // console.log('OptionSlider.show', event != null, !this.optionsButton.cancel)
      if (event != null && !this.optionsButton.cancel) {
        this.optionSlider.show()
      } else {
        this.optionsButton.cancel = true
      }

      this.optionsButton.frameName = 'options'
      this.optionsPattern.alpha = 0.4
    }
  }
}
