// Copyright (c) 2016-2021, VIMinO
//
// This source code file is under the GPLv3 license, which can be found at:
// https://www.gnu.org/licenses/gpl-3.0.en.html
// Or the attached LICENSE file

// Asset lists
import assets from '../assets'

// PhaserJS CE Libraries
import PIXI from 'expose-loader?PIXI!phaser-ce/build/custom/pixi.js' // eslint-disable-line
import p2 from 'expose-loader?p2!phaser-ce/build/custom/p2.js' // eslint-disable-line
import Phaser from 'expose-loader?Phaser!phaser-ce/build/custom/phaser-split.js' // eslint-disable-line

export default class Boot extends Phaser.State {
  preload () {
    // Set centered and scale to fill screen, maintaining the aspect ratio
    this.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL
    this.scale.fullScreenScaleMode = this.scale.scaleMode

    this.scale.pageAlignHorizontally = true
    this.scale.pageAlignVertically = true

    // Make sure to keep Phaser from smoothing the graphics
    Phaser.Canvas.setImageRenderingCrisp(this.game.canvas)
    this.game.renderer.renderSession.roundPixels = true
    this.stage.smoothed = false

    // Keep running if the game canvas loses focus
    // this.stage.disableVisibilityChange = true

    // Set the background color
    this.stage.backgroundColor = '#111111'

    // Load the "loader" assets
    this.load.path = './assets/'
    this.load.pack('loader', null, assets)

    // Set default options
    this.game.sound.volume = 0.0 // 0.5
    window.game = {
      diff: 2, // Normal
      full: false,
      // ?,?,playground < REMOVE,?,?,?,?,?
      toggle: [false, false, false, false, false, false, false, false]
    }
  }

  create () {
    // Switch to the Loader state
    this.state.start('Loader')
  }
}
