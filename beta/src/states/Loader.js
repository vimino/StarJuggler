// Copyright (c) 2016-2021, VIMinO
//
// This source code file is under the GPLv3 license, which can be found at:
// https://www.gnu.org/licenses/gpl-3.0.en.html
// Or the attached LICENSE file

// Asset lists
import assets from '../assets'

// PhaserJS CE Libraries
import PIXI from 'expose-loader?PIXI!phaser-ce/build/custom/pixi.js' // eslint-disable-line
import p2 from 'expose-loader?p2!phaser-ce/build/custom/p2.js' // eslint-disable-line
import Phaser from 'expose-loader?Phaser!phaser-ce/build/custom/phaser-split.js' // eslint-disable-line

// Game Modules
import Background from '../objects/Background'

export default class Loader extends Phaser.State {
  create () {
    // set the asset folder
    this.load.path = './assets/'

    this.back = this.game.add.sprite(5, 15, 'loader', 0)

    this.game.load.onLoadStart.add(this.startLoading, this)
    this.game.load.onFileComplete.add(this.progressLoading, this)
    this.game.load.onLoadComplete.add(this.start, this)

    this.load.pack('main', null, assets)
    this.game.load.start()
  }

  startLoading () {
    this.front = this.game.add.sprite(5, 15, 'loader', 1)
    this.front.crop(new Phaser.Rectangle(0, 0, 0, 33))
  }

  progressLoading (progress, cacheKey, success, totalLoaded, totalFiles) {
    this.front.crop(new Phaser.Rectangle(0, 0, ((progress / 100.0) * 54), 33))
  }

  start () {
    // hide the grayscale Title (back)
    this.back.visible = false

    // Load the index JSON file
    window.index = this.game.cache.getJSON('index')

    // Load all audio into the global context (Window)
    window.audio = {}
    for (var key in window.index.audio) {
      if (typeof (window.index.audio[key]) === 'string') {
        window.audio[key] = this.game.add.audio(window.index.audio[key])
      } else {
        window.audio[key] = []
        for (var id in window.index.audio[key]) {
          window.audio[key].push(this.game.add.audio(window.index.audio[key][id]))
        }
      }
    }

    // Keep a copy of the Game so we don't depend on 'this'
    const game = this.game

    window.audio.loop = function loop (name) {
      window.audio[name].loopFull()
    }

    window.audio.play = function play (name) {
      if (typeof (window.audio[name]) !== 'object') { return }
      if (window.audio[name] instanceof Array) {
        var id = game.rnd.integerInRange(0, window.audio[name].length - 1)
        window.audio[name][id].play()
      } else {
        window.audio[name].play()
      }
    }

    window.audio.stop = function stop (name) {
      if (typeof (window.audio[name]) !== 'object') { return }
      if (window.audio[name] instanceof Array) {
        for (var id in window.audio[name]) {
          window.audio[name][id].stop()
        }
      } else {
        window.audio[name].stop()
      }
    }

    window.audio.stopAll = function stopAll () {
      for (var key in window.audio) {
        if (typeof (window.audio[key]) !== 'object') { continue }
        window.audio.stop(key)
      }
    }
    window.audio.stopAll()

    // Global variables used to keep track of the current Background
    window.scenario = {
      planet: false,
      index: this.game.rnd.integerInRange(0, window.index.background.space.length - 1)
    }

    // Fade in a copy of the Menu Bbackground
    const background = new Background(this.game, window.index.background.space[window.scenario.index], Phaser.ANGLE_DOWN)
    background.stop()
    background.alpha = 0.0
    this.game.add.tween(background).to({ alpha: 1.0 }, 500, Phaser.Easing.Cubic.In, true)

    //  Keep the colored Title (front) on top and tween
    this.world.bringToTop(this.front)

    // Slide the colored Title (front) to the top then switch to the Menu
    this.game.add.tween(this.front).to({ x: 5, y: 4 }, 500, Phaser.Easing.Cubic.In, true).onComplete.add(() => {
      this.state.start('Menu')
    }, this)
  }
}
