// Copyright (c) 2016-2021, VIMinO
//
// This source code file is under the GPLv3 license, which can be found at:
// https://www.gnu.org/licenses/gpl-3.0.en.html
// Or the attached LICENSE file

// Babel implementation of some functionalities
import 'babel-polyfill'

// List of states
import * as states from './states'

// PhaserJS CE Libraries
import PIXI from 'expose-loader?PIXI!phaser-ce/build/custom/pixi.js' // eslint-disable-line
import p2 from 'expose-loader?p2!phaser-ce/build/custom/p2.js' // eslint-disable-line
import Phaser from 'expose-loader?Phaser!phaser-ce/build/custom/phaser-split.js' // eslint-disable-line

// Create a Phaser.Game with the CANVAS context (to avoid visual clipping)
window.game = new Phaser.Game(64, 64, Phaser.CANVAS)

// Add the States defined in states.js
Object
  .entries(states)
  .forEach(([key, state]) => window.game.state.add(key, state))

// Switch to the Boot state
window.game.state.start('Boot')
