// Copyright (c) 2016-2021, VIMinO
//
// This source code file is under the GPLv3 license, which can be found at:
// https://www.gnu.org/licenses/gpl-3.0.en.html
// Or the attached LICENSE file

// PhaserJS CE Libraries
import PIXI from 'expose-loader?PIXI!phaser-ce/build/custom/pixi.js' // eslint-disable-line
import p2 from 'expose-loader?p2!phaser-ce/build/custom/p2.js' // eslint-disable-line
import Phaser from 'expose-loader?Phaser!phaser-ce/build/custom/phaser-split.js' // eslint-disable-line

export class Star extends Phaser.Sprite {
  constructor (game, parent, color = 0xffffff, speed = 0.025) {
    super(game, 0, 0, 'star', '00')
    this.anchor.setTo(0.5, 0.5)
    this.tint = color

    const _parent = parent // Object with center position(x, y)
    const _speed = speed
    let _size = 0 // power
    // let _level = 0
    let _energy = 0
    let _forward = true
    let _absorb = false // overrides the animation
    this._freeze = false

    this.game.physics.arcade.enable(this)
    this.body.collideWorldBounds = false
    this.body.allowGravity = false
    this.body.immovable = true
    this.body.moves = false

    // Animations and Timers

    let _frameIndex = 0
    const _frameCount = 6

    let _baseAngle = 0
    let _shotAngle = 0
    let _startAngle = 0

    let _radius = 0
    let _centerX = 0
    let _centerY = 0

    let _onCatch

    const rotate = function Rotate () {
      if (_size < 0) { return }
      if (_frameIndex >= _frameCount) { _frameIndex = 0 }
      this.frameName = _size.toString() + _frameIndex.toString()
      ++_frameIndex
    }

    const move = function Move () {
      if (_absorb) {
        this.x += this.x > _parent.x ? -1 : 1
        this.y += this.y > _parent.y ? -1 : 1

        this.alpha -= 0.1
        if (this.alpha < 0.1) {
          _baseAngle = undefined
          _shotAngle = undefined
          _startAngle = undefined
          this.reset()
          if (_onCatch) { _onCatch() }
        }

        return
      }

      if (_forward) {
        _shotAngle += Math.PI * _speed
        this.x = _centerX + _radius * (Math.cos(_baseAngle) - Math.cos(_shotAngle))
        this.y = _centerY + _radius * (Math.sin(_baseAngle) - Math.sin(_shotAngle))

        if (_shotAngle > _startAngle + 2 * Math.PI) {
          _forward = false
        }
      } else {
        this.alpha -= _speed / 2.0
        _shotAngle -= Math.PI * _speed
        this.x = _centerX - _radius * (Math.cos(_baseAngle) - Math.cos(_shotAngle))
        this.y = _centerY - _radius * (Math.sin(_baseAngle) - Math.sin(_shotAngle))

        if (this.alpha < 0.1) {
          this.catch()
          _baseAngle = undefined
          _shotAngle = undefined
          _startAngle = undefined
        }
      }
    }

    const rotationTimer = this.game.time.create(false)
    rotationTimer.loop(15, rotate, this)
    rotationTimer.start()
    rotationTimer.pause()

    const movementTimer = this.game.time.create(false)
    movementTimer.loop(20, move, this)
    movementTimer.start()
    movementTimer.pause()

    // Attribute Editors

    // this.__defineGetter__('parent', function _GetParent () { return _parent })
    // this.__defineSetter__('parent', function _SetParent (value) {
    //   if (value !== undefined) { _parent = value }
    // })

    this.__defineGetter__('color', function _GetColor () { return this.tint })
    this.__defineSetter__('color', function _SetColor (value) {
      if (value !== undefined) { this.tint = value }
    })

    this.__defineGetter__('speed', function _GetSpeed () { return speed })
    this.__defineSetter__('speed', function _SetSpeed (value) {
      if (value !== undefined) { speed = value }
    })

    this.__defineGetter__('energy', function _GetEnergy () { return _energy })
    this.__defineSetter__('energy', function _SetEnergy (value) {
      if (value !== undefined) { _energy = value }
    })

    // Methods

    this.reset = function Reset (x, y, power) {
      this.kill() // this.alive = false
      this.visible = false
      this.alpha = 1.0

      rotationTimer.pause()
      movementTimer.pause()
      _absorb = false

      if (x !== undefined && y !== undefined) {
        this.position.setTo(x, y)
      }

      // If the Power changed we need to update the Sprite and its body
      if (power !== undefined && power !== _size) {
        if (power < 0) { power = 0 } // initial adjustment

        this.loadTexture('star', power.toString() + '0')
        _frameIndex = 0

        this.body.reset(x, y)
        this.body.setCircle(Math.max(this.width, this.height) / 2.5)
        let offset = 0.5
        if (power === 1 || power === 2) { offset = 1.5 } else
        if (power === 3) { offset = 1.75 } else
        if (power === 4) { offset = 2.25 } else
        if (power === 5) { offset = 2.5 }
        this.body.offset.setTo(offset, offset)

        _size = power
      }
    }

    this.onCatch = function OnCatch (callback) {
      if (typeof (callback) === 'function') {
        _onCatch = callback
      }
    }

    this.throw = function Throw (level, angle = 0, power = 0, skill = 0) {
      if (level === undefined) { return }

      _baseAngle = angle
      _shotAngle = _baseAngle // reverse baseAngle
      _startAngle = _shotAngle

      _forward = true
      _radius = 5 + level * 2
      _radius += Math.round(skill / 2.0)
      _centerX = _parent.position.x
      _centerY = _parent.position.y

      this.reset(_centerX, _centerY, power)

      _energy = level * (5 - window.game.diff)
      if (power) {
        _energy += power
      }

      // Resume Animations
      rotationTimer.resume()
      movementTimer.resume()

      // Make the Star alive and visible
      this.visible = true
      this.revive() // this.alive = true
    }

    this.catch = function Catch (instant = true) {
      // The Stars' initial energy was multiplied by 2 to 4, depending on difficulty
      let actualEnergy = _energy / (5 - window.game.diff)

      // Picking the Star when it's doing circling behind the initial position only gives half the energy
      if (!_forward) {
        actualEnergy /= 2.0
      }

      if (instant) {
        _absorb = false
        _energy = 0.0
        this.reset()
        if (_onCatch) { _onCatch() }
      } else {
        // this.kill() // it's not alive anymore
        _absorb = true
      }

      return actualEnergy
    }

    this.hit = function Hit (value) {
      let energy = 0

      // The Stars' initial energy was multiplied by 2 to 4, depending on difficulty
      let actualEnergy = _energy / (5 - window.game.diff)

      if (!_forward) {
        actualEnergy /= 2.0
      }

      if (value > 0) {
        energy = Math.abs(actualEnergy - value)
        _energy -= value
      } else {
        energy = actualEnergy
        _energy = 0.0
      }

      if (_energy <= 0.0) {
        this.reset()
      }

      return energy
    }

    this.__defineGetter__('catchable', function _IsCatchable () {
      return (!_absorb && (this.alpha < 1.0 || (_shotAngle > _startAngle + Math.PI)))
    })

    this.reset(0, 0, 0)
    // this.throw()
  }
}

export default class StarManager extends Phaser.Group {
  constructor (game, parent, maximum = 1, cooldown = 0.5, color) {
    super(game)

    const _parent = parent // Juggler
    let _maximum = maximum < 1 ? 1 : maximum // maximum Stars
    let _color = color === undefined ? _parent.color : color
    let _charge = 0.0
    let _limit = 0.0
    let _chargeStart = 0
    let _cooldown = cooldown < 0 ? 0 : cooldown
    const _lock = false

    let _onThrow
    let _onCatch

    for (let i = 0; i < _maximum; ++i) {
      const star = new Star(game, _parent, _color)
      this.add(star)
      if (_onCatch) { star.onCatch(_onCatch) }
    }

    // this.__defineGetter__('parent', function _GetParent () { return _parent })
    // this.__defineSetter__('parent', function _SetParent (value) {
    //   if (value !== undefined) { _parent = value }
    // })

    this.__defineGetter__('maximum', function _GetMaximum () { return _maximum })
    this.__defineSetter__('maximum', function _SetMaximum (value) {
      if (value !== undefined) {
        const difference = value - _maximum
        if (difference > 0) {
          for (let i = 0; i < difference; ++i) {
            const star = new Star(game, _parent, _color)
            this.add(star)
            if (_onCatch) { star.onCatch(_onCatch) }
          }
        } else if (difference < 0) {
          const count = this.children.length - 1
          for (let i = 0; i < -difference; ++i) {
            this.remove(this.children[count - i])
          }
        }

        _maximum += difference
      }
    })

    this.__defineGetter__('cooldown', function _GetCooldown () { return _cooldown })
    this.__defineSetter__('cooldown', function _SetCooldown (value) {
      if (value !== undefined) {
        _cooldown = value < 0 ? 0 : value
      }
    })

    this.__defineGetter__('limit', function _GetCooldown () { return _limit })
    this.__defineSetter__('limit', function _SetCooldown (value) {
      _limit = value
    })

    this.__defineGetter__('color', function _GetColor () { return _color })
    this.__defineSetter__('color', function _SetColor (value) {
      if (value === undefined) {
        value = _parent.color
      }

      this.forEach((star) => {
        star.color = value
      })

      _color = value
    })

    // Get the Angle (limited to 8 directions)
    this.getAngle = function GetAngle (pointer = null, keyboard = null, invert = false) {
      let angle = parent.body.angle
      if (invert) { angle = angle < 0 ? Phaser.Math.reverseAngle(angle) : -Phaser.Math.reverseAngle(-angle) }

      // Limit the Angle to one of 8 directions
      if (angle <= -2.748893571891069) { // left (-PI)
        angle = -3.141592653589793
      } else if (angle > -2.748893571891069 && angle <= -1.9634954084936207) { // up+left
        angle = -2.356194490192345
      } else if (angle > -1.9634954084936207 && angle <= -1.1780972450961724) { // up
        angle = -1.5707963267948966
      } else if (angle > -1.1780972450961724 && angle <= -0.39269908169872414) { // up+right
        angle = -0.7853981633974483
      } else if (angle > -0.39269908169872414 && angle <= 0.39269908169872414) { // right
        angle = 0.0
      } else if ((angle > 0.39269908169872414 && angle <= 1.1780972450961724)) { // down+right
        angle = 0.7853981633974483
      } else if (angle > 1.1780972450961724 && angle <= 1.9634954084936207) { // down
        angle = 1.5707963267948966
      } else if (angle > 1.9634954084936207 && angle <= 2.748893571891069) { // down+left
        angle = 2.356194490192345
      } else if (angle > 2.748893571891069) { // left (PI)
        angle = 3.141592653589793
      }

      return angle
    }

    this.__defineGetter__('charged', function _Charged () { return _charge })
    this.decharge = function Decharge () {
      const tmp = _charge
      _charge = 0
      return tmp
    }
    this.charge = function Charge (energy = 0) {
      if (_lock ||
        _parent.warping ||
        !this.getFirstDead(false) ||
        energy < 1 ||
        (_limit && _charge >= _limit)) { return 0 }

      let drain = 0

      // Start charging
      if (_charge === 0) {
        _charge = 1
        _chargeStart = this.game.time.now
        drain = 1 // _size + 1 // REVIEW
      } else if (this.game.time.now > _chargeStart + _cooldown) {
        _charge += 1
        _chargeStart = this.game.time.now
        drain = 1 // _size + 1 // REVIEW
      }

      // let charge = starma.charge
      // let energy = gauge.energy
      // let cooldown = starma.cooldown

      //   this.armStart = this.game.time.totalElapsedSeconds()
      //   starma.charge += 1
      //   gauge.energy -= 1
      // } else if (charge === 0.0) {
      //   // this is free if there aren't other stars
      //   if (starma.activeStars() > 0) {
      //     if (gauge.energy >= 1.0) {
      //       gauge.energy -= starma.charge = charge
      //       starma.charge += 1
      //     }
      //   } else {
      //     starma.charge += 1
      //   }

      // this.armStart = this.game.time.totalElapsedSeconds()

      // if (value !== undefined && value > 0.0) {
      //   _charge += value
      //   if (_limit !== undefined && _charge > _limit) {
      //     _charge = _limit
      //     return 0
      //   }
      // }

      return drain
    }

    this.activeStars = function activeStars () {
      let active = 0
      for (const star in this.children) {
        if (star.active) {
          ++active
        }
      }
      return active
    }

    this.onThrow = function OnThrow (callback) {
      if (typeof (callback) === 'function') {
        _onThrow = callback
      }
    }

    this.onCatch = function OnCatch (callback) {
      if (typeof (callback) === 'function') {
        _onCatch = callback
        this.forEach((star) => {
          star.onCatch(_onCatch)
        })
      }
    }

    this.throw = function Throw (angle = 0, power = 0, skill = 0) {
      if (_parent.warping) {
        console.log('Star.throw: Parent Warping')
        return
      }

      const star = this.getFirstDead(false)
      if (_lock || !star || _charge < 1) { return false }

      if (_onThrow) { _onThrow() }
      star.throw(_charge, angle, power, skill)
      _charge = 0.0

      // if (_cooldown > 0.0) {
      //   _lock = true
      //   console.log('WAIT', _cooldown, Phaser.Timer.SECOND * _cooldown)
      //   this.game.time.events.add(Phaser.Timer.SECOND * _cooldown, function Unlock () {
      //     _lock = false
      //   }, this)
      // }

      return true
    }

    this.touches = function Touches (object, callback) {
      this.game.physics.arcade.overlap(this, object, callback)
    }

    this.catchAll = function CatchAll () {
      let energy = 0
      let star = this.getFirstAlive()

      while (star) {
        energy += star.catch()
        star = this.getFirstAlive()
      }

      return energy
    }

    this.pause = function Pause (pause = true) {
      this._freeze = pause
    }
  }
}
