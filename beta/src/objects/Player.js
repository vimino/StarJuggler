// Copyright (c) 2016-2021, VIMinO
//
// This source code file is under the GPLv3 license, which can be found at:
// https://www.gnu.org/licenses/gpl-3.0.en.html
// Or the attached LICENSE file

// PhaserJS CE Libraries
import PIXI from 'expose-loader?PIXI!phaser-ce/build/custom/pixi.js' // eslint-disable-line
import p2 from 'expose-loader?p2!phaser-ce/build/custom/p2.js' // eslint-disable-line
import Phaser from 'expose-loader?Phaser!phaser-ce/build/custom/phaser-split.js' // eslint-disable-line

export default class Player extends Phaser.Sprite {
  constructor (game, direction = Phaser.ANGLE_RIGHT, x = 0, y = 0, speed = 10.0) {
    const frames = window.index.player

    // Use a main Sprite as the Body but don't give it an image
    super(game, x, y, 'ships', frames.area)
    this.anchor.setTo(0.5, 0.5)

    const _guide = this.game.add.sprite(0, 0, 'guide', 0)
    _guide.anchor.setTo(-0.2, 0.5)
    _guide.alpha = 0.5
    _guide.visible = false
    this.addChild(_guide)

    let _guideLimit = 0

    let _direction = 0 // 0 = Right, 90 = Down, 270 = -90 = Up, 180 = -180 = Left
    let _speed = 0
    let _damage = 0

    // Thruster sprite and animations
    const _thruster = this.game.add.sprite(-3, 0, 'ships', frames['center-thruster'] + '0')
    _thruster.anchor.setTo(1, 0.5)
    this.addChild(_thruster)

    _thruster.animations.add('center', Phaser.Animation.generateFrameNames(frames['center-thruster'], 0, 1), 5, true, false)
    _thruster.animations.add('left', Phaser.Animation.generateFrameNames(frames['left-thruster'], 0, 1), 5, true, false)
    _thruster.animations.add('right', Phaser.Animation.generateFrameNames(frames['right-thruster'], 0, 1), 5, true, false)
    _thruster.animations.add('warp', Phaser.Animation.generateFrameNames(frames['warp-thruster'], 0, 3), 4, true, false)
    _thruster.animations.add('warpOut', Phaser.Animation.generateFrameNames(frames['warp-thruster'], 2, 3), 6, true, false)
    _thruster.animations.add('warpIn', Phaser.Animation.generateFrameNames(frames['warp-thruster'], 3, 0), 4, true, false)
    _thruster.animations.play('center')

    // Charge sprite and animations
    const _glow = this.game.add.sprite(0, 0, 'ships', frames['center-charge'] + '0')
    _glow.anchor.setTo(0.5, 0.5)
    this.addChild(_glow)

    _glow.animations.add('center', Phaser.Animation.generateFrameNames(frames['center-charge'], 0, 1), 5, true, false)
    _glow.animations.add('left', Phaser.Animation.generateFrameNames(frames['left-charge'], 0, 1), 5, true, false)
    _glow.animations.add('right', Phaser.Animation.generateFrameNames(frames['right-charge'], 0, 1), 5, true, false)
    _glow.animations.play('center')

    _glow.alpha = 0.0

    const _sprite = this.game.add.sprite(0, 0, 'ships', frames.center + '0')
    _sprite.anchor.setTo(0.5, 0.5)
    this.addChild(_sprite)

    // Physics
    this.game.physics.arcade.enable(this)
    this.body.setSize(7, 7, 0, 0)
    // this.body.setCircle(4)
    this.body.immovable = true
    this.body.collideWorldBounds = true
    this.body.allowGravity = false

    // Getters/Setters

    this.__defineGetter__('direction', function _GetDirection () { return _direction })
    this.__defineSetter__('direction', function _SetDirection (value) {
      if (value !== undefined) {
        this.angle = value
        _direction = this.angle
      }
    })

    this.__defineGetter__('speed', function _GetSpeed () { return _speed })
    this.__defineSetter__('speed', function _SetSpeed (value) {
      if (value !== undefined) {
        _speed = value
      }
    })

    this.__defineGetter__('damage', function _GetDamage () { return _damage })
    this.__defineSetter__('damage', function _SetDamage (value = 0) {
      if (value < 0) { value = 0 }
      if (value > 3) { value = 3 }
      _damage = value
    })

    this.__defineGetter__('color', function _GetColor () { return _sprite.tint })
    this.__defineSetter__('color', function _SetColor (value) {
      if (value !== undefined) {
        _sprite.tint = value
        _glow.tint = 0xffffff - value
        if (_glow.tint < 0x555555) {
          _glow.tint += 0x555555
        }
      }
    })

    this.__defineGetter__('glow', function _GetGlow () { return _glow.alpha })
    this.__defineSetter__('glow', function _SetGlow (value) {
      if (value !== undefined) {
        if (value < 0.0) { value = 0.0 }
        if (value > 1.0) { value = 1.0 }
        _glow.alpha = value
      }
    })

    // Methods

    this.stop = function Stop () {
      if (this.warping) {
        _sprite.frameName = frames.center + _damage.toString()
      }
      this.body.stop() // .velocitysetTo(0, 0)
      // _sprite.frameName = frames['center'] + _damage.toString()
    }

    this.move = function Move (x = 0.0, y = 0.0) {
      if (this.warping) { return }
      this.body.velocity.setTo(_speed * x, _speed * y)
    }

    this.moveToPointer = function MoveToPointer () {
      if (this.warping) { return }
      this.game.physics.arcade.moveToPointer(this, _speed)
    }

    this.touches = function Touches (object, callback) {
      this.game.physics.arcade.overlap(this, object, callback)
    }

    this.onDeath = function OnDeath (callback) {
      if (typeof (callback) === 'function') {
        this.onDeath = callback
      }

      const explosion = this.game.add.sprite(this.x, this.y, 'explosion')
      explosion.anchor.setTo(0.5, 0.5)
      explosion.animations.add('boom', [0, 1, 2, 3, 4], 5, false).onComplete.add(function () {
        explosion.kill()
        explosion.destroy()
      }, this)
      explosion.animations.play('boom')
    }

    this.die = function Die () {
      if (this.onDeath) { this.onDeath() }
      this.kill()
    }

    // Doesn't work since it uses animations?
    this.limitGuide = function LimitGuide (limit) {
      if (limit !== undefined) {
        _guideLimit = limit
        const size = limit < 1 ? 3 : limit * 3 + 2
        const rect = new Phaser.Rectangle(0, 0, size, 19)
        _guide.crop(rect)
      }

      return _guideLimit
    }

    this.afterWarp = function AfterWarp (callback = undefined) {
      this.postWarpCall = callback
    }

    this.warp = function Warp (out = true) {
      // Make sure we only Warp Out if we're not warping and can only Warp In otherwise
      if (out && this.warping) { return }

      this.warping = true
      this.body.collideWorldBounds = false
      this.glow = 0.0
      this.guide()
      this.stop()

      let warpStart = {}

      if (out) {
        let warpPosition = {}
        if (this.angle === 0) { // Right
          warpStart = { x: this.x - 4 }
          warpPosition = { x: 128 + this.width }
        } else if (this.angle === 90) { // Down
          warpStart = { y: this.y - 4 }
          warpPosition = { y: 128 + this.height }
        } else if (Math.abs(this.angle) === 180) { // Left
          warpStart = { x: this.x + 4 }
          warpPosition = { x: -64 - this.width }
        } else if (this.angle === 270 || this.angle === -90) { // Up
          warpStart = { y: this.y + 4 }
          warpPosition = { y: -64 - this.height }
        }

        const chargeWarp = this.game.add.tween(this).to(warpStart, 500, Phaser.Easing.Circular.Out, false)
        chargeWarp.onStart.add(function () {
          _thruster.x = -3
          _thruster.y = 0
          _thruster.animations.play('warp')
        })

        const warpOut = this.game.add.tween(this).to(warpPosition, 2000, Phaser.Easing.Circular.In, false)
        warpOut.onStart.add(function () {
          // _sprite.frameName = frames['center'] + _damage.toString()
          this.body.collideWorldBounds = false
          _thruster.x = -3
          _thruster.y = 0
          _thruster.animations.play('warpOut')
        }, this)
        warpOut.onComplete.add(function () {
          // this.warping = false
          if (this.postWarpCall) { this.postWarpCall() }
        }, this)

        chargeWarp.chain(warpOut)
        chargeWarp.start()
      } else {
        if (this.angle === 0) { // Right
          warpStart = { x: -64, y: 32 }
        } else if (this.angle === 90) { // Down
          warpStart = { x: 32, y: -64 }
        } else if (Math.abs(this.angle) === 180) { // Left
          warpStart = { x: 128, y: 32 }
        } else if (this.angle === 270 || this.angle === -90) { // Up
          warpStart = { x: 32, y: 128 }
        }

        this.position.setTo(warpStart.x, warpStart.y)

        const warpIn = this.game.add.tween(this).to({ x: 32, y: 32 }, 1000, Phaser.Easing.Circular.Out, false)
        warpIn.onStart.add(function () {
          // _thruster.x = -4
          // _thruster.y = 0
          // _thruster.animations.play('center')
          _thruster.x = -3
          _thruster.y = 0
          _thruster.animations.play('warpIn')
        })
        warpIn.onComplete.add(() => {
          _thruster.x = -4
          _thruster.y = 0
          _thruster.animations.play('center')
          this.body.collideWorldBounds = true
          this.warping = false
          // if (this.postWarpCall) { this.postWarpCall() }
        }, this)

        warpIn.start()
      }
    }

    this.guide = function Guide (angle = undefined, charge = 0) {
      // _sprite.alpha = 0.2
      // _thruster.visible = false
      // _glow.visible = false

      if (angle === undefined) {
        _guide.visible = false
      } else {
        _guide.visible = true
        _guide.frame = (charge > 6 ? 6 : (charge < 0 ? 0 : charge))

        // Angle correction based on the direction of the Player
        let offset = 0
        if (this.angle === 270 || this.angle === -90) { // Looking Up
          offset = 90.0 * Phaser.Math.DEG_TO_RAD
        } else if (this.angle === 90) { // Looking Down
          offset = -90.0 * Phaser.Math.DEG_TO_RAD
        } else if (Math.abs(this.angle) === 180) { // Looking Left
          offset = 180.0 * Phaser.Math.DEG_TO_RAD
        // } else if (this.angle === 0) { // Looking Right
        }
        _guide.rotation = angle + offset

        // Position correction based on the direction of the Guide
        _guide.anchor.setTo(0, 0.5)
        let x = 0
        let y = 0
        if (_guide.angle === -135) { // Aiming Up-Left
          x = 1
          y = 1
          _guide.angle = 270
          _guide.anchor.setTo(0, 1)
          _guide.frame = 7 + charge
        } else if (_guide.angle === -90) { // Aiming Up
          // x = 0
          y = -1
        } else if (_guide.angle === -45) { // Aiming Up-Right
          _guide.angle = 0
          _guide.anchor.setTo(0, 1)
          _guide.frame = 7 + charge
          // x = 1
          y = 1
        } else if (_guide.angle === 0) { // Aiming Right
          x = 2
        } else if (_guide.angle === 45) { // Aiming Down-Right
          // x = 0
          // y = 0
          _guide.angle = 90
          _guide.anchor.setTo(0, 1)
          _guide.frame = 7 + charge
        } else if (_guide.angle === 90) { // Aiming Down
          x = 1
          y = 2
        } else if (_guide.angle === 135) { // Aiming Down-Left
          x = 1
          // y = 0
          // _guide.angle += 0.5 // fix the half rotation
          _guide.anchor.setTo(0, 1)
          _guide.angle = 180
          _guide.frame = 7 + charge
        } else if (Math.abs(_guide.angle) === 180) { // Aiming Left
          x = -1
          y = 1
        }
        _guide.position.setTo(x, y)
      }

      return _guide.visible ? _guide.angle : undefined
    }

    this.update = function Update () {
      if (this.warping) { return }

      const velocity = this.body.velocity

      if (this.angle === 270 || this.angle === -90) { // Looking Up
        this.body.offset.setTo(1, 0)
        if (velocity.x < 0) { // Going Left
          _sprite.frameName = frames.left + _damage.toString()
          this.body.offset.setTo(1, 0)
          _thruster.y = -1
          _thruster.animations.play('left')
          _glow.animations.play('left')
        } else if (velocity.x > 0) { // Going Right
          _sprite.frameName = frames.right + _damage.toString()
          this.body.offset.setTo(1, 0)
          _thruster.y = 1
          _thruster.animations.play('right')
          _glow.animations.play('right')
        } else {
          _sprite.frameName = frames.center + _damage.toString()
          _thruster.y = 0
          _thruster.animations.play('center')
          _glow.animations.play('center')
        }
      } else if (this.angle === 90) { // Looking Down
        this.body.offset.setTo(0, 1)
        if (velocity.x < 0) { // Going Left
          _sprite.frameName = frames.right + _damage.toString()
          this.body.offset.setTo(0, 1)
          _thruster.y = 1
          _thruster.animations.play('right')
          _glow.animations.play('right')
        } else if (velocity.x > 0) { // Going Right
          _sprite.frameName = frames.left + _damage.toString()
          this.body.offset.setTo(0, 1)
          _thruster.y = -1
          _thruster.animations.play('left')
          _glow.animations.play('left')
        } else {
          _sprite.frameName = frames.center + _damage.toString()
          _thruster.y = 0
          _thruster.animations.play('center')
          _glow.animations.play('center')
        }
      } else if (Math.abs(this.angle) === 180) { // Looking Left
        this.body.offset.setTo(0, 0)
        if (velocity.y < 0) { // Going Up
          _sprite.frameName = frames.right + _damage.toString()
          this.body.offset.setTo(0, 0)
          _thruster.y = 1
          _thruster.animations.play('right')
          _glow.animations.play('right')
        } else if (velocity.y > 0) { // Going Down
          _sprite.frameName = frames.left + _damage.toString()
          this.body.offset.setTo(0, 0)
          _thruster.y = -1
          _thruster.animations.play('left')
          _glow.animations.play('left')
        } else {
          _sprite.frameName = frames.center + _damage.toString()
          _thruster.y = 0
          _thruster.animations.play('center')
          _glow.animations.play('center')
        }
      } else if (this.angle === 0) { // Looking Right
        this.body.offset.setTo(1, 1)
        if (velocity.y < 0) { // Going Up
          _sprite.frameName = frames.left + _damage.toString()
          this.body.offset.setTo(1, 0)
          _thruster.y = -1
          _thruster.animations.play('left')
          _glow.animations.play('left')
        } else if (velocity.y > 0) { // Going Down
          _sprite.frameName = frames.right + _damage.toString()
          this.body.offset.setTo(1, 2)
          _thruster.y = 1
          _thruster.animations.play('right')
          _glow.animations.play('right')
        } else {
          _sprite.frameName = frames.center + _damage.toString()
          _thruster.y = 0
          _thruster.animations.play('center')
          _glow.animations.play('center')
        }
      }
    }

    this.direction = direction
    this.speed = speed
    this.game.add.existing(this)
  }
}
