// Copyright (c) 2016-2021, VIMinO
//
// This source code file is under the GPLv3 license, which can be found at:
// https://www.gnu.org/licenses/gpl-3.0.en.html
// Or the attached LICENSE file

import PIXI from 'expose-loader?PIXI!phaser-ce/build/custom/pixi.js' // eslint-disable-line
import p2 from 'expose-loader?p2!phaser-ce/build/custom/p2.js' // eslint-disable-line
import Phaser from 'expose-loader?Phaser!phaser-ce/build/custom/phaser-split.js' // eslint-disable-line

export default class Enemy extends Phaser.Sprite {
  constructor (game, x, y, type, level, direction = Phaser.ANGLE_DOWN) {
    super(game, x, y, 'ships')

    this.direction = direction
    this.angle = direction
    // let spriteDir = 'D'
    // // if (direction === Phaser.ANGLE_DOWN) {
    // if (direction === Phaser.ANGLE_RIGHT) {
    //   spriteDir = 'R'
    // } else if (direction === Phaser.ANGLE_LEFT) {
    //   spriteDir = 'L'
    // } else if (direction === Phaser.ANGLE_UP) {
    //   spriteDir = 'U'
    // }
    // console.log(direction, spriteDir)

    if (type < 6) {
      this.frameName = 'e' + type.toString() + level.toString()
    } else if (type === 6) {
      if (level === 1) {
        this.frameName = 'juggernaut'
      } else if (level === 2) {
        this.frameName = 'cyclops'
      } else if (level === 3) {
        this.frameName = 'titan'
      }
    }

    if (type === 0) {
      this.angle = this.game.math.degToRad(90 * this.game.rnd.integerInRange(0, 3))
    // } else {
    //   this.angle = direction - 90
    }

    this.anchor.setTo(0.5, 0.5)

    this.type = type
    this.level = level
    this.health = 1
    if (type > 0 && type < 6) {
      this.health = level
    } else if (type > 5) {
      this.health = level * 4
    }

    this.weapons = []

    // Physics
    this.game.physics.arcade.enable(this)
    this.body.collideWorldBounds = false
    this.body.allowGravity = false
    this.body.immovable = true
    // this.body.moves = false

    this.moveTo = (x, y, speed) => {
      const cx = this.position.x
      const cy = this.position.y
      let vx = 0
      let vy = 0
      if (!speed) speed = 1

      if (cx < x) {
        vx = speed
      } else if (cx > x) {
        vx = -speed
      }

      if (cy < y) {
        vy = speed
      } else if (cy > y) {
        vy = -speed
      }

      this.body.velocity.setTo(vx, vy)
    }

    this.stop = () => {
      this.body.stop()
    }

    this.getType = () => {
      return this.type
    }

    this.getLevel = () => {
      return this.level
    }

    this.getPosition = () => {
      return this.position
    }

    this.getHealth = () => {
      return this.health
    }

    this.damage = (value) => {
      if (value !== undefined && typeof (value) === 'number') {
        this.health -= Math.abs(parseInt(value))
      } else {
        this.health = 0.0
      }

      if (this.health <= 0.0) {
        // for (let i in this.weapons) {
        //   this.weapons[i].autofire = false
        //   // this.weapons[i].destroyAll()
        //   // this.weapons[i].destroy()
        //   // delete this.weapons[i]
        // }

        const explosion = this.game.add.sprite(this.x, this.y, 'explosion')
        explosion.anchor.setTo(0.5, 0.5)
        if (this.type === 6) {
          explosion.scale.setTo(2, 2)
        }
        explosion.animations.add('boom', [0, 1, 2, 3, 4], 5, false).onComplete.add(function () {
          explosion.kill()
        }, this)
        explosion.animations.play('boom')

        this.kill()
        return true
      }

      return false
    }

    this.changeTo = (type, level, direction = Phaser.ANGLE_DOWN) => {
      this.type = type
      this.level = level

      this.frameName = 'e' + type.toString() + (level + 1).toString()
      this.body.reset()
      if (type === 0) {
        this.angle = this.game.math.degToRad(90 * this.game.rnd.integerInRange(0, 3))
      } else {
        this.angle = direction - 90
      }

      for (const i in this.weapons) {
        this.weapons[i].destroy()
      }
    }

    this.addDirectionalWeapon = (direction, type = 0) => {
      // console.log('adw', this.angle, Phaser.ANGLE_DOWN, direction)
      // let type = this.type
      // let dir = 'D'
      // // let angleRight = this.angle + Phaser.ANGLE_DOWN // 0 is Right in Phaser
      // // if (direction === Phaser.ANGLE_DOWN) {
      // if (direction === Phaser.ANGLE_UP) {
      //   dir = 'U'
      // } else if (direction === Phaser.ANGLE_LEFT) {
      //   dir = 'L'
      // } else if (direction === Phaser.ANGLE_RIGHT) {
      //   dir = 'R'
      // }
      // let relevel = this.level
      let retype = this.type
      if (type) {
        retype = type
      }
      // console.log('dir', dir)
      let frame = ''
      if (retype === 0) { // Asteroid (no weapon)
        return
      } else if (retype === 1) { // Blaster
        if (this.level === 1) {
          frame = 'b2'
        } else if (this.level === 2) {
          frame = 'b4'
        } else if (this.level === 3) {
          frame = 'b5'
        }
      } else if (retype === 2) { // Bomber
        frame = 'r' + this.level.toString()
      } else if (retype === 3) { // Slasher
        frame = 'w' + this.level.toString()
      } else if (retype === 4) { // Razor
        frame = 'p' + this.level.toString()
      } else if (retype === 5) { // Sweeper
        frame = 'b' + this.level.toString()
      } else if (retype === 6) { // Overlords
        frame = 'l' + this.level.toString()
      }

      const weapon = this.game.add.weapon(4 - this.level, 'projectiles', frame)
      // weapon.body.reset()
      // weapon.bulletAngleOffset = -direction
      weapon.bulletSpeed = (40 * window.game.diff) / this.level
      weapon.fireRate = (600 * (4 - window.game.diff)) * this.level
      weapon.fireAngle = direction
      weapon.bulletKillType = Phaser.Weapon.KILL_WORLD_BOUNDS
      // weapon.energy = this.getLevel() / 2 // Math.round() no round since 1 = 0.5
      weapon.trackSprite(this, 0, 0)
      weapon.level = this.level
      const self = this
      weapon.onFire.add(() => {
        // window.console.log('self', self.x, self.y)
        if (self.x > 0 && self.x < 64 && self.y > 0 && self.y < 64) {
          this.onFireCallback()
        }
      })

      // weapon.fireAngle = direction
      // weapon.angle = direction

      this.weapons.push(weapon)
    }

    this.addWeapon = (target, ot, ox, oy) => {
      let retype = this.type
      if (ot) {
        retype = ot
      }
      if (ot !== undefined) {
        type = ot
      }
      // let direction = 'D'
      // let angleRight = this.angle // + 90 // 0 is Right in Phaser
      // if (angleRight - 90 === Phaser.ANGLE_DOWN) {
      //   direction = 'D'
      // } else if (angleRight - 90 === Phaser.ANGLE_UP) {
      //   direction = 'U'
      // } else if (angleRight - 90 === Phaser.ANGLE_LEFT) {
      //   direction = 'L'
      // } else if (angleRight - 90 === Phaser.ANGLE_RIGHT) {
      //   direction = 'R'
      // }
      // console.log(angleRight, direction)
      let frame = ''
      if (retype === 0) { // Asteroid (no weapon)
        return
      } else if (retype === 1) { // Blaster
        if (this.level === 1) {
          frame = 'b2'
        } else if (this.level === 2) {
          frame = 'b4'
        } else if (this.level === 3) {
          frame = 'b5'
        }
      } else if (retype === 2) { // Bomber
        frame = 'r' + this.level.toString()
      } else if (retype === 3) { // Slasher
        frame = 'w' + this.level.toString()
      } else if (retype === 4) { // Razor
        frame = 'p' + this.level.toString()
      } else if (retype === 5) { // Sweeper
        frame = 'b' + this.level.toString()
      } else if (retype === 6) { // Overlords
        frame = 'l' + this.level.toString()
      }

      // let weapon = this.game.add.weapon(1, 'weapons')
      const weapon = this.game.add.weapon(4 - this.level, 'projectiles', frame)
      // weapon.bulletAngleOffset = angleRight // 270
      // weapon.setBulletFrames(frame, frame)
      // weapon.fireRate = 1000 * this.level
      if (retype === 6) { // Lasers are a lot faster
        weapon.bulletSpeed = (80 * window.game.diff) / this.level
        // weapon.bulletSpeed = 50 / this.level
        weapon.fireRate = (600 * (4 - window.game.diff)) / this.level
      } else {
        weapon.bulletSpeed = (40 * window.game.diff) / this.level
        weapon.fireRate = (600 * (4 - window.game.diff)) * this.level
      }
      // weapon.fireAngle = angleRight // - Phaser.ANGLE_DOWN
      weapon.bulletKillType = Phaser.Weapon.KILL_WORLD_BOUNDS
      weapon.trackSprite(this, ox === undefined ? 0 : ox, oy === undefined ? 0 : oy, this)
      weapon.target = target
      weapon.health = this.level

      const self = this
      weapon.onFire.add(() => {
        // console.log('self', self.x, self.y)
        if (self.x > 0 && self.x < 64 && self.y > 0 && self.y < 64) {
          this.onFireCallback()
        }
      })

      this.weapons.push(weapon)
    }

    this.onFire = (callback) => {
      this.onFireCallback = callback
    }

    this.fire = (id) => {
      // console.log('Enemy.fire', this.weapons)

      if (id === undefined) { // Fire All Weapons
        for (const id in this.weapons) {
          const weapon = this.weapons[id]
          if (weapon.target === undefined) {
            weapon.fire()
          } else {
            weapon.fireAtSprite(weapon.target)
          }
        }
      } else if (id in this.weapons) { // Fire Specific Weapon
        const weapon = this.weapons[id]
        if (weapon.target === undefined) {
          weapon.fire()
        } else {
          weapon.fireAtSprite(weapon.target)
        }
      }
    }
  }
}
