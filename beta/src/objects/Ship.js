// Copyright (c) 2016-2021, VIMinO
//
// This source code file is under the GPLv3 license, which can be found at:
// https://www.gnu.org/licenses/gpl-3.0.en.html
// Or the attached LICENSE file

// PhaserJS CE Libraries
import PIXI from 'expose-loader?PIXI!phaser-ce/build/custom/pixi.js' // eslint-disable-line
import p2 from 'expose-loader?p2!phaser-ce/build/custom/p2.js' // eslint-disable-line
import Phaser from 'expose-loader?Phaser!phaser-ce/build/custom/phaser-split.js' // eslint-disable-line

// Game Modules
// import ProjectileManager from '../objects/Projectile'

class Ship extends Phaser.Sprite {
  constructor (game, ship, level = 1, direction = Phaser.ANGLE_RIGHT, speed = 1.0, x = 0, y = 0) {
    const thrusterFrames = window.index.thruster

    const index = level - 1
    super(game, x, y, 'ships', ship.sprite[index])
    this.anchor.setTo(0.5, 0.5)

    // Private letiables
    let _ship = ship
    let _direction = 0 // 0 = Right, 90 = Down, 270 = -90 = Up, 180 = -180 = Left
    let _circle = false // Use a Circular Body
    let _speed = 0

    let _level = level
    let _structure = level
    // let _weapon = [] // undefined // TODO

    let _onDeath

    // Thruster sprite and animations
    const _thruster = this.game.add.sprite(-4, 0, 'ships', thrusterFrames[0] + '_0')
    _thruster.anchor.setTo(1, 0.5)
    this.addChild(_thruster)
    // _thruster.alpha = 0.3

    // Create animations for all Thruster sprite groups
    for (const animId in window.index.thruster) {
      const animName = window.index.thruster[animId]
      _thruster.animations.add(animName, Phaser.Animation.generateFrameNames(animName + '_', 0, 1), 5, true, false)
    }

    const _updateThruster = function _UpdateThruster (ship, index) {
      if (
        index !== undefined &&
        ship !== undefined &&
        ship !== null &&
        ship.thruster !== undefined &&
        ship.thruster !== null
      ) {
        index = Phaser.Math.clamp(index, 0, ship.thruster.length)
        if (ship.thruster[index] !== undefined && ship.thruster[index] !== null) {
          _thruster.x = !isNaN(ship.thruster[index].x) ? ship.thruster[index].x : 0
          _thruster.y = !isNaN(ship.thruster[index].y) ? ship.thruster[index].y : 0
          _thruster.animations.play(thrusterFrames[ship.thruster[index].sprite])
          _thruster.visible = true
        } else {
          _thruster.visible = false
        }
      } else {
        _thruster.visible = false
      }
    }

    _updateThruster(_ship, index)

    // Physics
    // this.physics.startSystem(Phaser.Physics.ARCADE)
    this.game.physics.arcade.enable(this)
    // this.body.setSize(5, 7, 2, 1)
    this.body.collideWorldBounds = false
    this.body.allowGravity = false
    this.body.immovable = true
    // this.body.offset.setTo(0, 0)
    this.body.syncBounds = true

    // Attribute Editors

    const self = this

    this.__defineGetter__('direction', function _GetDirection () { return _direction })
    this.__defineSetter__('direction', function _SetDirection (value) {
      if (value !== undefined) { _direction = value }
      self.adapt()
    })

    this.__defineGetter__('circle', function _GetCircle () { return _circle })
    this.__defineSetter__('circle', function _SetCircle (value) {
      if (value !== undefined) { _circle = value }
      self.adapt()
    })

    const _updateDetails = function _updateDetails (direction, speed, circle) {
      if (direction !== undefined) { _direction = direction }
      if (speed !== undefined) { _speed = speed }
      if (circle !== undefined) { _circle = circle }
      if (direction !== undefined || speed !== undefined || circle !== undefined) {
        self.adapt()
      }
    }

    this.__defineGetter__('speed', function _GetSpeed () { return _speed })
    this.__defineSetter__('speed', function _SetSpeed (value) {
      if (value !== undefined) { _speed = value }
    })

    this.__defineGetter__('type', function _GetType () { return ship.type })

    this.__defineGetter__('level', function _GetLevel () { return _level })
    this.__defineSetter__('level', function _SetLevel (value) {
      if (level !== undefined && level > 0) { _level = level }
    })

    this.__defineGetter__('structure', function _GetStructure () { return _structure })
    this.__defineSetter__('structure', function _SetStructure (value) {
      if (value !== undefined && value > 0) {
        _structure = value
      }
    })

    // Methods

    this.adapt = function adapt () {
      const right = _direction === 0
      const left = Math.abs(_direction) === 180
      const up = _direction === -90 || _direction === 270
      const down = _direction === 90 || _direction === -270

      self.angle = _direction

      if (right) {
        self.body.offset.setTo(0, 0)
      } else if (left) {
        self.body.offset.setTo(-1, -1)
      } else if (up) {
        // this.body.offset.setTo(Math.floor(this.body.width / 3), 1 - Math.floor(this.body.height / 3))
        self.body.offset.setTo(0, -1)
      } else if (down) {
        // this.body.offset.setTo(Math.floor(this.body.width / 3) - 1, 1 - Math.floor(this.body.height / 3))
        // this.body.offset.setTo(Math.round(this.x - this.body.x), 0)
        self.body.offset.setTo(-1, 0)
      }

      // Set it to a circle if requested
      if (_circle) {
        self.body.setCircle(Math.floor(Math.max(self.width, self.height) / 2))
      }
    }

    this.damage = function damage (value) {
      if (value !== undefined && typeof (value) === 'number') {
        _structure -= parseInt(value)
      } else {
        _structure = 0.0
      }

      if (_structure <= 0.0) {
        this.die()
        return true
      }

      return false
    }

    this.stop = function stop () {
      this.body.stop()
    }

    this.place = function place (x = 0.0, y = 0.0) {
      this.position.setTo(x, y)
    }

    this.move = function move (x = 0.0, y = 0.0) {
      this.body.velocity.setTo(_speed * x, _speed * y)
    }

    this.touches = function touches (object, callback) {
      // return this.game.physics.arcade.collide(this, object)
      this.game.physics.arcade.overlap(this, object, callback)
    }

    this.onDeath = function onDeath (callback) {
      if (typeof (callback) === 'function') {
        _onDeath = callback
      }
    }

    this.die = function die () {
      const x = this.x
      const y = this.y
      const type = this.type
      const level = this.level
      // console.log('Ship.die:',' ',x,' ',y,' ',type,' ',_level)
      this.kill()
      // if (_onDeath) { _onDeath(ship.type, _level) }
      if (_onDeath) { _onDeath(x, y, type, level) }
    }

    this.reset = function reset (newShip = undefined, level = undefined, direction = undefined, speed = undefined, circle = undefined) {
      let index = level === undefined ? _level - 1 : level - 1
      let switchedShip = false
      let switchedLevel = false

      if (newShip !== undefined && newShip !== _ship) {
        _ship = newShip
        switchedShip = true
      }

      if (level !== undefined) {
        _level = level
        index = level - 1
        if (_ship.type === 1) { // asteroid has a random angle
          this.angle = this.game.rnd.integerInRange(0, 3) * 90
        } else {
          this.angle = _direction
        }
        switchedLevel = true
      }

      if (switchedShip || switchedLevel) {
        // Update the several enemy details
        self.frameName = _ship.sprite[index]
        _updateThruster(_ship, index)
        _structure = level
      }

      if (direction !== undefined || speed !== undefined || circle !== undefined) {
        _updateDetails(direction, speed, circle)
      }

      // self.revive()

      return self.frameName
    }

    this.update = function Update () {
      // Kill the Ship if it exists the screen in a direction it didn't come from
      if (
        (this.x > this.game.width + this.width && _direction !== Phaser.ANGLE_LEFT) ||
        (this.x < -this.width && _direction !== Phaser.ANGLE_RIGHT) ||
        (this.y > this.game.height + this.height && _direction !== Phaser.ANGLE_UP) ||
        (this.y < -this.height && _direction !== Phaser.ANGLE_DOWN)
      ) {
        this.kill()
      }

      if (
        (this.body.gravity.x !== 0 &&
          (this.x > this.game.width + this.width * 2 ||
          this.x < -this.width * 2)) ||
        (this.body.gravity.y !== 0 &&
          (this.y > this.game.height + this.height * 2 ||
           this.y < -this.height * 2))
      ) {
        this.kill()
      }
    }

    this.direction = direction
    this.speed = speed
    this.reset()
    this.game.add.existing(this)
  }
}

export default class ShipManager extends Phaser.Group {
  constructor (game, direction = Phaser.ANGLE_RIGHT, size = 0) {
    const ships = Object.keys(window.index.ship)
    super(game)

    let _direction = direction
    let _onDeath

    this.__defineGetter__('direction', function _GetDirection () { return _direction })
    this.__defineSetter__('direction', function _SetDirection (value) {
      if (value !== undefined) {
        _direction = value
      }
    })

    this.__defineGetter__('total', function _GetTotal () { return this.children.length })
    this.__defineSetter__('total', function _SetTotal (value) {
      if (value !== undefined) {
        const difference = value - this.children.length
        if (difference > 0) {
          for (let i = 0; i < difference; ++i) {
            const ship = new Ship(this.game, window.index.ship[ships[0]], -64, -64)
            if (_onDeath) { ship.onDeath(_onDeath) }
            ship.kill()
            this.add(ship)
          }
        } else if (difference < 0) {
          const count = this.children.length - 1
          for (let i = 0; i < -difference; ++i) {
            this.remove(this.children[count - i])
          }
        }
      }
    })

    if (size > 0) {
      this.total = size
    }

    // Get the number of Ships that are still alive
    this.__defineGetter__('count', function _GetCount () {
      let alive = 0
      this.forEach((ship) => {
        if (ship.alive) { ++alive }
      })
      return alive
    })

    // The sum of all (active) Ship levels
    this.__defineGetter__('totalLevel', function GetTotalLevel () {
      let total = 0
      this.forEach((ship) => {
        total += ship.level() // + 1
      })
      return total
    })

    this.onDeath = function onDeath (callback) {
      if (typeof (callback) === 'function' && callback !== _onDeath) {
        _onDeath = callback
        this.forEach((ship) => {
          ship.onDeath(_onDeath)
        })
      }
    }

    // Kill all ships without destroying them
    this.clear = function Clear () {
      this.forEach((ship) => {
        if (ship.alive) { ship.kill() }
      })
    }

    this.send = function Send (
      type = 0,
      level = 1,
      direction = undefined,
      range = 60,
      gravity = { x: 0, y: 0 },
      speed = undefined,
      center = false) {
      // let newShip = Ob ject.keys(window.index['ship'])[type]
      // let shipIndex = level - 1 // Sprite Index (0 to 5)

      if (direction === undefined) { direction = _direction }
      // else { _direction = direction }

      let shipSpeed = 0.0
      if (speed !== undefined) {
        shipSpeed = speed
      } else {
        if (type === 0) {
          shipSpeed = this.game.rnd.integerInRange(10 - level, 12 - level)
        } else if (type < 6) {
          shipSpeed = this.game.rnd.integerInRange(8 - level, 10 - level)
        } else if (type === 6) {
          shipSpeed = this.game.rnd.integerInRange(6 - level, 8 - level)
        }
      }
      shipSpeed *= window.game.diff

      let ship = this.getFirstDead(false)
      if (!ship) { // There are no Ships left, so we must make a new one
        ship = new Ship(this.game, window.index.ship[ships[type]], level, direction, shipSpeed, -64, -64)
        if (_onDeath) { ship.onDeath(_onDeath) }
        ship.kill()
        this.add(ship)
      } else {
        ship.reset(window.index.ship[ships[type]], level, direction, shipSpeed, -64, -64)
      }

      let x = 0
      let y = 0
      let vx = 0
      let vy = 0

      // Range limit if the Ships starts close to a corner
      let limit = 0

      // Adapt the position and movement according to the direction of the Ship
      if (Math.abs(direction) === 180) { // Left
        x = ship.width + this.game.width
        y = Math.round(this.game.rnd.integerInRange(4, this.game.height - 4))

        const corner = this.game.height / 3
        if (y > this.game.height - corner) {
          limit = -1
        } else if (y < corner) {
          limit = 1
        }
        vx = -1
      } else if (direction === -90 || direction === 270) { // Up
        x = Math.round(this.game.rnd.integerInRange(4, this.game.width - 4))
        y = ship.height + this.game.height

        const corner = this.game.width / 4
        if (x > this.game.width - corner) {
          limit = -1
        } else if (x < corner) {
          limit = 1
        }
        vy = -1
      } else if (direction === 0 || direction === 360) { // Right
        x = -ship.width
        y = Math.round(this.game.rnd.integerInRange(4, this.game.height - 4))

        const corner = this.game.height / 4
        if (y > this.game.height - corner) {
          limit = -1
        } else if (y < corner) {
          limit = 1
        }
        vx = 1
      } else if (direction === 90) { // Down
        x = this.game.rnd.integerInRange(4, this.game.width - 4)
        y = -ship.height

        const corner = this.game.width / 4
        if (x > this.game.width - corner) {
          limit = -1
        } else if (x < corner) {
          limit = 1
        }
        vy = 1
      }

      const xAxis = (vy === 0) // we need to know the axis of the direction
      const trajectory = this.game.rnd.realInRange(
        limit === -1 ? 0 : -range,
        limit === 1 ? 0 : range
      )

      const moveDir = Phaser.Math.degToRad(direction - trajectory)
      vx = Math.cos(moveDir)
      vy = Math.sin(moveDir)

      ship.place(x, y)
      ship.move(vx, vy)

      if (gravity !== undefined && (gravity.x !== 0 || gravity.y !== 0)) {
        ship.body.gravity.setTo(gravity.x, gravity.y)
        ship.body.allowGravity = true
      } else {
        ship.body.allowGravity = false
      }

      if (center) {
        if (xAxis) {
          x = 32
        } else {
          y = 32
        }
      }

      ship.position.setTo(x, y)
      ship.revive() // this gives it 100 health, thus the use of 'structure'
    }

    this.sendWave = function sendWave (limit, center = false) {
      // let waveCount = 0
      let waveLevel = limit

      let type = 0
      let level = 0

      while (waveLevel > 0) {
        // Depending on Difficulty and the Level Limit // TODO
        const directions = this.game.rnd.integerInRange(0, 3)
        let redirection = this.game.rnd.integerInRange(0, directions)

        // while (waveLevel > 0)
        type = this.game.rnd.integerInRange(0, 5)
        level = this.game.rnd.integerInRange(1, 6)

        if (level + redirection > waveLevel) {
          if (waveLevel - level > 0) {
            redirection = Phaser.Math.clamp(waveLevel - level, 0, 3)
          } else {
            level = waveLevel
            redirection = 0
          }
        }

        let shipDir = _direction
        if (redirection === 0) { // Opposite to Player direction
          shipDir = shipDir + 180
        } else if (redirection === 1) { // Perpendicular to Player direction
          const side = this.game.rnd.integerInRange(0, 1)
          if (side === 0) {
            shipDir = shipDir - 90 // Phaser.ANGLE_RIGHT
          } else {
            shipDir = shipDir + 90 // Phaser.ANGLE_LEFT
          }
          // gravity = 1
        // } else if (redirection === 2) { // Behind the Player
          // gravity = 2
        }

        const gravity = { x: 0, y: 0 }

        if (_direction === 0) { // Right
          gravity.x = -redirection * 2
        } else if (_direction === 90) { // Down
          gravity.y = -redirection * 2
        } else if (_direction === -90 || shipDir === 270) { // Up
          gravity.y = redirection * 2
        } else if (Math.abs(_direction) === 180) { // Left
          gravity.x = redirection * 2
        }

        this.send(type, level, shipDir, undefined, gravity, undefined, center)
        waveLevel -= level
      }
    }
  }
}

export { Ship, ShipManager }
