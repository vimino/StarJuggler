// Copyright (c) 2016-2021, VIMinO
//
// This source code file is under the GPLv3 license, which can be found at:
// https://www.gnu.org/licenses/gpl-3.0.en.html
// Or the attached LICENSE file

// PhaserJS CE Libraries
import PIXI from 'expose-loader?PIXI!phaser-ce/build/custom/pixi.js' // eslint-disable-line
import p2 from 'expose-loader?p2!phaser-ce/build/custom/p2.js' // eslint-disable-line
import Phaser from 'expose-loader?Phaser!phaser-ce/build/custom/phaser-split.js' // eslint-disable-line

// Game Modules
import Label from '../objects/Label'

export default function AboutSlider (game) {
  const slider = game.add.group(game, null, 'ShopSlider')
  slider.visible = false

  slider.active = false
  slider.preactive = false

  // Advance to Next Level

  slider.advance = false

  // Callbacks (they need to be defined earlier than the shpw amd Toggle methods)

  let onShow
  let onHide

  slider.onShow = function OnShow (callback) {
    if (typeof (callback) === 'function') {
      onShow = callback
    }
  }

  slider.onHide = function OnHide (callback) {
    if (typeof (callback) === 'function') {
      onHide = callback
    }
  }

  // Price modifier based on difficulty

  const modifier = parseInt(500 * window.game.diff)

  // Upgrades (must never go below the base ones)

  const baseUpgrades = {
    power: 0,
    structure: 0,
    skill: 0
  }

  const upgrades = {
    power: 0,
    structure: 0,
    skill: 0,
    score: 0,
    cost: 0,
    selected: undefined
  }

  // Upgrade methods

  const calculateCost = function CalculateCost (currentLevel) {
    if (window.game.toggle[6]) {
      return 0
    } else {
      return (parseInt(currentLevel) * modifier)
    }
  }

  const switchUpgrade = function SwitchUpgrade (object) {
    powerOutline.visible = false
    structureOutline.visible = false
    skillOutline.visible = false

    if (object) {
      upgrades.selected = object.frameName
    }

    if (upgrades.selected) {
      if (upgrades.selected === 'power') {
        buyButton.visible = (upgrades.power !== 5 &&
          (upgrades.cost + calculateCost(upgrades.power + 1 + upgrades.structure + upgrades.skill)) <= upgrades.score)
        sellButton.visible = (baseUpgrades.power !== upgrades.power)
        powerOutline.visible = true
        which.set('Power')
        if (upgrades.power < 5) {
          cost.set('Cost: ' + (calculateCost(upgrades.power + 1 + upgrades.structure + upgrades.skill).toString()))
        } else {
          cost.set('')
        }
      } else if (upgrades.selected === 'structure') {
        buyButton.visible = (upgrades.structure !== 5 &&
          (upgrades.cost + calculateCost(upgrades.structure + 1 + upgrades.power + upgrades.skill)) <= upgrades.score)
        sellButton.visible = (baseUpgrades.structure !== upgrades.structure)
        structureOutline.visible = true
        which.set('Structure')
        if (upgrades.structure < 5) {
          cost.set('Cost: ' + (calculateCost(upgrades.structure + 1 + upgrades.power + upgrades.skill).toString()))
        } else {
          cost.set('')
        }
      } else if (upgrades.selected === 'skill') {
        buyButton.visible = (upgrades.skill !== 5 &&
          (upgrades.cost + calculateCost(upgrades.skill + 1 + upgrades.power + upgrades.structure)) <= upgrades.score)
        sellButton.visible = (baseUpgrades.skill !== upgrades.skill)
        skillOutline.visible = true
        which.set('Skill')
        if (upgrades.skill < 5) {
          cost.set('Cost: ' + (calculateCost(upgrades.skill + 1 + upgrades.power + upgrades.structure).toString()))
        } else {
          cost.set('')
        }
      }
    } else {
      cost.set('')
      buyButton.visible = false
      sellButton.visible = false
    }

    totalCost.set((upgrades.cost >= 0 ? '-' : '+') + upgrades.cost.toString())
  }

  const updateValues = function UpdateValues () {
    let power = Math.floor(upgrades.power * 3)
    if (power < 0) { power = 0 }
    if (power > 15) { power = 15 }
    powerFront.crop(new Phaser.Rectangle(0, 16 - power, 17, 17))
    if ((baseUpgrades.power !== upgrades.power) || (upgrades.power !== 5 &&
      (upgrades.cost + calculateCost(upgrades.power + 1 + upgrades.skill + upgrades.structure)) <= upgrades.score)) {
      powerBack.alpha = 1.0
      powerFront.alpha = 1.0
    } else {
      powerBack.alpha = 0.5
      powerFront.alpha = 0.5
    }

    let structure = Math.floor(upgrades.structure * 3)
    if (structure < 0) { structure = 0 }
    if (structure > 15) { structure = 15 }
    structureFront.crop(new Phaser.Rectangle(0, 16 - structure, 17, 17))

    // If we can't buy or sell make the Upgrade semi-transparent

    if ((baseUpgrades.structure !== upgrades.structure) || (upgrades.structure !== 5 &&
      (upgrades.cost + calculateCost(upgrades.structure + 1 + upgrades.power + upgrades.skill)) <= upgrades.score)) {
      structureBack.alpha = 1.0
      structureFront.alpha = 1.0
    } else {
      structureBack.alpha = 0.5
      structureFront.alpha = 0.5
    }

    let skill = Math.floor(upgrades.skill * 3)
    if (skill < 0) { skill = 0 }
    if (skill > 15) { skill = 15 }
    skillFront.crop(new Phaser.Rectangle(0, 16 - skill, 18, 17))
    if ((baseUpgrades.skill !== upgrades.skill) || (upgrades.skill !== 5 &&
      (upgrades.cost + calculateCost(upgrades.skill + 1 + upgrades.power + upgrades.structure)) <= upgrades.score)) {
      skillBack.alpha = 1.0
      skillFront.alpha = 1.0
    } else {
      skillBack.alpha = 0.5
      skillFront.alpha = 0.5
    }

    score.set(upgrades.score.toString())
  }

  // Frame sprites

  const frame = game.add.group()

  const frameSprite = game.add.sprite(0, 0, 'panel', 'OrangePanel')

  const framePattern = game.add.sprite(1, 1, 'panel', 'OrangePanelPat1')

  frame.add(frameSprite)
  frame.add(framePattern)
  slider.add(frame)

  // Label sprites

  const which = new Label(game, 32, 3, 'Upgrades:')
  which.anchor.setTo(0.5, 0)
  slider.add(which)

  const cost = new Label(game, 32, 28, '')
  cost.anchor.setTo(0.5, 0)
  slider.add(cost)

  const scoreLabel = new Label(game, 32, 39, 'Score:')
  scoreLabel.anchor.setTo(0.5, 0)
  slider.add(scoreLabel)

  const score = new Label(game, 32, 47, '0')
  score.anchor.setTo(0.5, 0)
  slider.add(score)

  const totalCost = new Label(game, 32, 54, '-0')
  totalCost.anchor.setTo(0.5, 0)
  slider.add(totalCost)

  // Power sprites and events

  const power = game.add.group()

  const powerBack = game.add.sprite(4, 10, 'ui', 'power')

  const powerFront = game.add.sprite(4, 27, 'ui', 'power+')
  powerFront.anchor.setTo(0.0, 1.0)
  powerFront.crop(new Phaser.Rectangle(0, 0, 17, 0))

  const powerOutline = game.add.sprite(4, 10, 'ui', 'powerOutline')
  powerOutline.visible = false

  power.add(powerBack)
  power.add(powerFront)
  power.add(powerOutline)
  slider.add(power)

  powerBack.inputEnabled = true
  powerBack.events.onInputDown.add(switchUpgrade, this)

  // Structure sprites and events

  const structure = game.add.group()

  const structureBack = game.add.sprite(23, 10, 'ui', 'structure')

  const structureFront = game.add.sprite(23, 27, 'ui', 'structure+')
  structureFront.anchor.setTo(0.0, 1.0)
  structureFront.crop(new Phaser.Rectangle(0, 0, 17, 0))

  const structureOutline = game.add.sprite(23, 10, 'ui', 'structureOutline')
  structureOutline.visible = false

  structure.add(structureBack)
  structure.add(structureFront)
  structure.add(structureOutline)
  slider.add(structure)

  structureBack.inputEnabled = true
  structureBack.events.onInputDown.add(switchUpgrade, this)

  // Skill sprites and eve3nts

  const skill = game.add.group()

  const skillBack = game.add.sprite(42, 10, 'ui', 'skill')

  const skillFront = game.add.sprite(42, 27, 'ui', 'skill+')
  skillFront.anchor.setTo(0.0, 1.0)
  skillFront.crop(new Phaser.Rectangle(0, 0, 18, 0))

  const skillOutline = game.add.sprite(42, 10, 'ui', 'skillOutline')
  skillOutline.visible = false

  skill.add(skillBack)
  skill.add(skillFront)
  skill.add(skillOutline)
  slider.add(skill)

  skillBack.inputEnabled = true
  skillBack.events.onInputDown.add(switchUpgrade, this)

  // Continue button sprite and events

  const continueButton = game.add.sprite(50, 39, 'ui', 'OrangeGoBut')
  slider.add(continueButton)

  const clickContinue = function ClickContinue (element, event) {
    // Handle the click event and cancel if the user moves the mouse away

    if (event != null && event.isDown) {
      continueButton.frameName = 'OrangeGoButP'
      continueButton.cancel = false
    } else {
      if (event == null) {
        continueButton.cancel = true
      }

      if (continueButton.cancel) {
        continueButton.frameName = 'OrangeGoBut'
      } else {
        slider.hide()
      }
    }
  }

  continueButton.inputEnabled = true
  continueButton.events.onInputDown.add(clickContinue, this)
  continueButton.events.onInputUp.add(clickContinue, this)
  continueButton.events.onInputOut.add(
    () => { clickContinue(null) }
    , this)

  // Buy button sprite and events

  const buyButton = game.add.sprite(0, 34, 'ui', 'OrangeBuyBut')
  slider.add(buyButton)

  const clickBuy = function ClickBuy (element, event) {
    // Handle the click event and cancel if the user moves the mouse away

    if (event != null && event.isDown) {
      buyButton.frameName = 'OrangeBuyButP'
      buyButton.cancel = false
    } else {
      if (event == null) {
        buyButton.cancel = true
      }

      if (!buyButton.cancel) {
        if (upgrades.selected) {
          if (upgrades.selected === 'power' &&
            upgrades.power < 5) {
            const cost = calculateCost(upgrades.power + 1 + upgrades.skill + upgrades.structure)
            if ((upgrades.cost + cost) <= upgrades.score) {
              upgrades.cost += cost
              ++upgrades.power
            }
          } else if (upgrades.selected === 'structure' &&
            upgrades.structure < 5) {
            const cost = calculateCost(upgrades.structure + 1 + upgrades.power + upgrades.skill)
            if ((upgrades.cost + cost) <= upgrades.score) {
              upgrades.cost += cost
              ++upgrades.structure
            }
          } else if (upgrades.selected === 'skill' &&
            upgrades.skill < 5) {
            const cost = calculateCost(upgrades.skill + 1 + upgrades.power + upgrades.structure)
            if ((upgrades.cost + cost) <= upgrades.score) {
              upgrades.cost += cost
              ++upgrades.skill
            }
          }
        }

        switchUpgrade()
        updateValues()
      }

      buyButton.frameName = 'OrangeBuyBut'
    }
  }

  buyButton.inputEnabled = true
  buyButton.events.onInputDown.add(clickBuy, this)
  buyButton.events.onInputUp.add(clickBuy, this)
  buyButton.events.onInputOut.add(
    () => { clickBuy(null) },
    this)

  // Sell button sprites and events

  const sellButton = game.add.sprite(0, 49, 'ui', 'OrangeSellBut')
  slider.add(sellButton)

  const clickSell = function ClickSell (element, event) {
    // Handle the click event and cancel if the user moves the mouse away

    if (event != null && event.isDown) {
      sellButton.frameName = 'OrangeSellButP'
      sellButton.cancel = false
    } else {
      if (event == null) {
        sellButton.cancel = true
      }

      if (!sellButton.cancel) {
        if (upgrades.selected) {
          if (upgrades.selected === 'power' &&
            upgrades.power > baseUpgrades.power) {
            const cost = calculateCost(upgrades.power + upgrades.skill + upgrades.structure)
            upgrades.cost -= cost
            --upgrades.power
          } else if (upgrades.selected === 'structure' &&
            upgrades.structure > baseUpgrades.structure) {
            upgrades.cost -= calculateCost(upgrades.structure + upgrades.power + upgrades.skill)
            --upgrades.structure
          } else if (upgrades.selected === 'skill' &&
            upgrades.skill > baseUpgrades.skill) {
            upgrades.cost -= calculateCost(upgrades.skill + upgrades.power + upgrades.structure)
            --upgrades.skill
          }

          switchUpgrade()
          updateValues()
        }
      }

      sellButton.frameName = 'OrangeSellBut'
    }
  }

  sellButton.inputEnabled = true
  sellButton.events.onInputDown.add(clickSell, this)
  sellButton.events.onInputUp.add(clickSell, this)
  sellButton.events.onInputOut.add(
    () => { clickSell(null) },
    this)

  // Current frames

  let patternFrame = 0

  const animate = function Animate () {
    // Cycle through the frames

    if (!slider.active) { return }

    ++patternFrame
    if (patternFrame > 6) patternFrame = 1
    framePattern.frameName = 'OrangePanelPat' + patternFrame.toString()
  }

  // Show/Hide methods, callbacks and tweens

  const showTween = game.add.tween(slider).to({ x: 0, y: 0 }, 500, Phaser.Easing.Cubic.In, false)
  showTween.onComplete.add(() => {
    slider.active = true
  })

  slider.show = function Show (upgs, advance = true) {
    // console.log('#1: ', advance)

    if (slider.preactive) { return }

    if (onShow) { onShow() }

    slider.advance = advance

    baseUpgrades.power = upgs.power
    upgrades.power = upgs.power
    baseUpgrades.structure = upgs.structure
    upgrades.structure = upgs.structure
    baseUpgrades.skill = upgs.skill
    upgrades.skill = upgs.skill

    upgrades.score = upgs.score
    upgrades.selected = 'power' // so we see the cost instantly
    upgrades.cost = 0

    switchUpgrade()
    updateValues()

    slider.position.x = -64
    slider.visible = true
    slider.preactive = true
    timer.resume()
    showTween.start()
  }

  const hideTween = game.add.tween(slider).to({ x: 64, y: 0 }, 500, Phaser.Easing.Cubic.Out, false)
  hideTween.onComplete.add(() => {
    timer.pause()
    slider.preactive = false
    slider.visible = false
  }, this)

  slider.hide = function Hide () {
    // console.log('#2: ', slider.advance)

    if (!slider.active) { return }

    if (onHide) {
      const upgs = {
        power: upgrades.power,
        structure: upgrades.structure,
        skill: upgrades.skill,
        score: upgrades.score - upgrades.cost
      }

      slider.active = false
      onHide(upgs, slider.advance)
    }
    hideTween.start()
  }

  // Animation timer

  let timer = null

  slider.reset = function Reset () {
    timer = game.time.create()
    timer.loop(160, animate, this)
    timer.start()
    timer.pause()
  }

  slider.reset()

  return slider
}
