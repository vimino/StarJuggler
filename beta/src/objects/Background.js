// Copyright (c) 2016-2021, VIMinO
//
// This source code file is under the GPLv3 license, which can be found at:
// https://www.gnu.org/licenses/gpl-3.0.en.html
// Or the attached LICENSE file

// PhaserJS CE Libraries
import PIXI from 'expose-loader?PIXI!phaser-ce/build/custom/pixi.js' // eslint-disable-line
import p2 from 'expose-loader?p2!phaser-ce/build/custom/p2.js' // eslint-disable-line
import Phaser from 'expose-loader?Phaser!phaser-ce/build/custom/phaser-split.js' // eslint-disable-line

export default class Background extends Phaser.Group {
  constructor (game, sprite = window.index.background.space[0], newdirection = Phaser.ANGLE_RIGHT, newspeed = 10) {
    super(game)

    const back = this.game.add.tileSprite(-1, -1, 66, 66, 'background', sprite)
    back.anchor.setTo(0, 0)
    this.add(back)

    let direction = 0 // 0 = Right, 90 = Down, 270 = -90 = Up, 180 = -180 = Left
    let speed = 0
    let movement = { x: 0, y: 0 }

    const clone = this.game.add.tileSprite(0, 0, 66, 66, 'background', 'world5') // sprite)
    clone.visible = false
    this.add(clone)

    let beforeSet
    let afterSet

    let beforeWarp
    let afterWarp
    // const count = 0

    const transition = this.game.add.tween(clone).to({ alpha: 0.0 }, 2000, Phaser.Easing.Linear.None, false)
    transition.onStart.add(function () {
      if (beforeSet !== undefined) { beforeSet() }
    })
    transition.onComplete.add(function () {
      if (afterSet !== undefined) { afterSet() }
      clone.visible = false
    }, this)

    this.__defineGetter__('direction', function _GetDirection () { return direction })
    this.__defineSetter__('direction', function _SetDirection (value) {
      if (value === undefined) { return }

      if (value !== direction) {
        direction = value
      }

      back.angle = direction
      if (direction === 0) {
        back.x = -1
        back.y = -1
      } else if (direction === 90) {
        back.x = 65
        back.y = -1
      } else if (Math.abs(direction) === 180) {
        back.x = 65
        back.y = 65
      } else if (direction === -90 || direction === 270) {
        back.x = -1
        back.y = 65
      }

      movement = { x: speed, y: 0 }
      this.scroll()
    })

    this.__defineGetter__('speed', function _GetSpeed () { return speed })
    this.__defineSetter__('speed', function _SetSpeed (value) {
      if (value === undefined) { return }

      speed = value < 0 ? 0 : (value < 250 ? value : 250)
      movement = { x: speed, y: 0 }
      if (speed > 0.0) {
        this.scroll()
      } else {
        this.stop()
      }
    })

    let minspeed = 4
    // const maxspeed = 500
    const warpspeed = 100
    // const TenthOfSecond = Phaser.Timer.SECOND / 10

    this.beforeWarp = function BeforeWarp (callback) {
      if (typeof (callback) === 'function') {
        beforeWarp = callback
      }
    }

    this.afterWarp = function AfterWarp (callback) {
      if (typeof (callback) === 'function') {
        afterWarp = callback
      }
    }
    /*
    const warpOut = function WarpOut () {
      speed += warpspeed
      if (speed > maxspeed) { speed = maxspeed }
      this.speed = Math.round(speed)

      if (afterWarp) {
        ++count
        if (count === 9) { afterWarp() }
      }
    }

    const warpIn = function WarpIn () {
      speed -= warpspeed
      if (speed < minspeed) { speed = minspeed }
      this.speed = Math.round(speed)

      if (afterWarp) {
        ++count
        if (count === 9) { afterWarp() }
      }
    }
*/
    this.warp = function warp (out = true) {
      if (beforeWarp !== undefined) { beforeWarp() }
      // count = 0

      if (out) {
        // minspeed = speed
        // warpspeed = (maxspeed - minspeed) / 10.0

        // this.game.time.events.repeat(TenthOfSecond, 10, warpOut, this)
        this.speed = warpspeed
      } else {
        // this.game.time.events.repeat(TenthOfSecond, 10, warpIn, this)

        this.speed = minspeed
      }

      if (afterWarp !== undefined) { afterWarp() }
    }

    this.scroll = function scroll () {
      back.autoScroll(movement.x, movement.y)
    }

    this.stop = function stop () {
      clone.stopScroll()
      back.stopScroll()
    }

    this.beforeSet = function (callback) {
      beforeSet = callback
    }

    this.afterSet = function (callback) {
      afterSet = callback
    }

    this.set = function set (sprite = undefined, animate = false, newdirection = undefined, newspeed = undefined) {
      if (transition.isRunning) { return }

      if (animate) { // Copy the Background so we can transition during the switch
        clone.angle = back.angle
        clone.x = back.x
        clone.y = back.y
        clone.frameName = back.frameName
        clone.tilePosition = back.tilePosition
        clone.autoScroll(movement.x, movement.y)
        clone.alpha = 1.0
        clone.visible = true
      }

      if (sprite !== undefined) { back.frameName = sprite }

      if (newspeed !== undefined) {
        minspeed = newspeed
        speed = newspeed
      } else {
        speed = minspeed
      }

      if (newdirection !== undefined) { this.direction = newdirection }

      if (animate) { transition.start() }
    }

    this.set(undefined, false, newdirection, newspeed)
    this.scroll()
    this.game.add.existing(this)
  }
}
