// Copyright (c) 2016-2021, VIMinO
//
// This source code file is under the GPLv3 license, which can be found at:
// https://www.gnu.org/licenses/gpl-3.0.en.html
// Or the attached LICENSE file

import PIXI from 'expose-loader?PIXI!phaser-ce/build/custom/pixi.js' // eslint-disable-line
import p2 from 'expose-loader?p2!phaser-ce/build/custom/p2.js' // eslint-disable-line
import Phaser from 'expose-loader?Phaser!phaser-ce/build/custom/phaser-split.js' // eslint-disable-line

export default class Counter extends Phaser.Group {
  constructor (game, x, y, font, size) {
    super(game)

    this.value = 0
    // console.log('wut', this.value)

    this.label = game.add.bitmapText(x, y, font, this.value.toString(), size)
    this.label.alpha = 0.6
    this.add(this.label)
  }

  update () {
    this.label.text = this.value.toString()
  }

  plus (value) {
    // console.log('Counter.plus', value)

    if (!isNaN(value)) {
      this.value += parseInt(value)
      if (this.value < 0) {
        this.value = 0
      }
      this.update()
    }
  }

  set (value) {
    console.log('Counter.set', value)

    if (!isNaN(value)) {
      this.value = parseInt(value)
      this.update()
    }
  }

  get () {
    console.log('Counter.get', this.value)

    return this.value
  }

  setAlpha (value) {
    console.log('Counter.setAlpha')

    this.label.alpha = value
  }

  // alpha (value) {
  getAlpha () {
    console.log('Counter.getAlpha', this.label.alpha) // value)

    // let alpha = this.label.alpha
    // if (!isNaN(value)) {
    //   alpha = parseFloat(value)
    // }
    return this.label.alpha // alpha
  }
}
