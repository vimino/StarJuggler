// Copyright (c) 2016-2021, VIMinO
//
// This source code file is under the GPLv3 license, which can be found at:
// https://www.gnu.org/licenses/gpl-3.0.en.html
// Or the attached LICENSE file

import PIXI from 'expose-loader?PIXI!phaser-ce/build/custom/pixi.js' // eslint-disable-line
import p2 from 'expose-loader?p2!phaser-ce/build/custom/p2.js' // eslint-disable-line
import Phaser from 'expose-loader?Phaser!phaser-ce/build/custom/phaser-split.js' // eslint-disable-line

export default class Gauge extends Phaser.Group {
  constructor (game, x, y, size = 10) {
    super(game)
    this.position.setTo(x, y)

    // Gauge sprites

    let _size = 0

    const _gaugeSprite = this.create(0, 0, 'ui')
    const _energySprite = this.create(0, 1, 'ui')
    _energySprite.anchor.setTo(1, 0)
    const _armorSprite = this.create(1, 3, 'ui')
    const _shieldSprite = this.create(1, 3, 'ui')

    this.add(_gaugeSprite)
    this.add(_energySprite)
    this.add(_armorSprite)
    this.add(_shieldSprite)

    // Rectangles used to crop each bar

    const _energyCrop = new Phaser.Rectangle(0, 0, 10, 3)
    _energySprite.crop(_energyCrop)

    const _armorCrop = new Phaser.Rectangle(0, 0, 10, 3)
    _armorSprite.crop(_armorCrop)

    const _shieldCrop = new Phaser.Rectangle(0, 0, 10, 3)
    _shieldSprite.crop(_shieldCrop)

    // Lock the Gauge, stopping all modifications

    this.lock = false

    // Size variables and methods

    this.__defineGetter__('size', function GetSize () { return _size })
    this.__defineSetter__('size', function SetSize (value) {
      if (this.lock) { return }

      if (value % 2 !== 0) {
        console.warn('Gauge.updateSize: Invalid size', value)
        value = 10
      }

      if (value < 10) {
        console.warn('Gauge.updateSize: Size is too small', value)
        value = 10
      }
      if (value > 24) {
        console.warn('Gauge.updateSize: Size is too big', value)
        value = 24
      }

      // Update the internal size

      _size = parseInt(value)

      // Update the sprites and their positions

      _gaugeSprite.frameName = 'gauge' + value.toString()
      const sfn = 'g' + value.toString()
      _energySprite.frameName = sfn + '-energy'
      _armorSprite.frameName = sfn + '-armor'
      _shieldSprite.frameName = sfn + '-shield'

      this.x = Math.floor(game.width / 2 - _gaugeSprite.width / 2)
      _energySprite.x = _gaugeSprite.x + _size + 1

      // Fill values

      this.fill()
    })

    // Energy variables and methods

    let _energy = _size

    const _updateEnergy = function _UpdateEnergy () {
      if (_energy > _size) {
        _energy = _size
      } else if (_energy < 0) {
        _energy = 0
      }

      // Phaser.Rectangle(_size - _energy, 0, _energy, 3)
      const enr = Math.floor(_energy)
      _energyCrop.x = _size - enr
      _energyCrop.width = enr
      _energySprite.updateCrop()
    }

    this.__defineGetter__('energy', function GetEnergy () { return _energy })
    this.__defineSetter__('energy', function SetEnergy (value) {
      if (this.lock) { return }

      _energy = value
      _updateEnergy()
    })

    const _energyRegen = []
    _energyRegen[0] = 0 // value
    _energyRegen[1] = 0 // delay
    _energyRegen[2] = 1 // modifier
    _energyRegen[3] = game.time.create(false)

    const _regenEnergy = function _RegenEnergy () {
      if (this.lock) { return }

      if (_energyRegen[0] <= 0) { return }
      _energy += _energyRegen[0]
      _updateEnergy()
    }

    _energyRegen[3].loop(Phaser.Timer.SECOND, _regenEnergy, this)
    _energyRegen[3].start()

    this.__defineGetter__('energyRegenValue', function GetEnergyRegen () { return _energyRegen[0] })
    this.__defineGetter__('energyRegenDelay', function GetEnergyRegenDelay () { return _energyRegen[1] })
    this.__defineGetter__('energyRegenModifier', function GetEnergyRegeModifier () { return _energyRegen[2] })
    this.setEnergyRegen = function SetEnergyRegeneration (value, delay, modifier) {
      if (value !== undefined) { _energyRegen[0] = value }
      if (delay <= 0) {
        _energyRegen[1] = 0
        _energyRegen[3].pause()
      } else if (delay > 0) {
        _energyRegen[1] = delay
        _energyRegen[3].events[0].delay = Phaser.Timer.SECOND * delay
        if (_energyRegen[3].paused) { _energyRegen[3].resume() }
      }
      if (modifier === undefined) {
        if (_energyRegen[2] !== 1) {
          _energyRegen[2] = 1
          _energyRegen[3].events[0].delay = Phaser.Timer.SECOND *
          _energyRegen[1]
        }
      } else {
        _energyRegen[2] = modifier
        _energyRegen[3].events[0].delay = Phaser.Timer.SECOND *
        _energyRegen[1] / _energyRegen[2]
      }
    }

    // Armor variables and methods

    let _armor = _size

    const _updateArmor = function _UpdateArmor () {
      if (_armor > _size) {
        _armor = _size
      } else if (_armor < 0) {
        _armor = 0
      }

      // Phaser.Rectangle(0, 0, Math.floor(_armor), 3)
      _armorCrop.width = Math.floor(_armor)
      _armorSprite.updateCrop()
    }

    this.__defineGetter__('armor', function GetArmor () { return _armor })
    this.__defineSetter__('armor', function SetArmor (value) {
      if (this.lock) { return }

      _armor = value
      _updateArmor()
    })

    // Shield variables and methods

    let _shield = _size

    const _updateShield = function _UpdateShield () {
      if (_shield > _size) {
        _shield = _size
      } else if (_shield < 0) {
        _shield = 0
      }

      // Phaser.Rectangle(0, 0, Math.floor(_shield), 3)
      _shieldCrop.width = Math.floor(_shield)
      _shieldSprite.updateCrop()
    }

    this.__defineGetter__('shield', function GetShield () { return _shield })
    this.__defineSetter__('shield', function SetShield (value) {
      if (this.lock) { return }

      _shield = value
      _updateShield()
    })

    const _shieldRegen = []
    _shieldRegen[0] = 0 // value
    _shieldRegen[1] = 0 // delay
    _shieldRegen[2] = 1 // modifier
    _shieldRegen[3] = game.time.create(false)

    const _regenShield = function _RegenShield () {
      if (this.lock) { return }

      if (_shieldRegen[0] <= 0) { return }
      _shield += _shieldRegen[0]
      _updateShield()
    }

    _shieldRegen[3].loop(Phaser.Timer.SECOND, _regenShield, this)
    _shieldRegen[3].start()

    this.__defineGetter__('shieldRegenValue', function GetShieldRegen () { return _shieldRegen[0] })
    this.__defineGetter__('shieldRegenDelay', function GetShieldRegenDelay () { return _shieldRegen[1] })
    this.__defineGetter__('shieldRegenModifier', function GetShieldRegenModifier () { return _shieldRegen[2] })
    this.setShieldRegen = function SetShieldRegeneration (value, delay, modifier) {
      if (value !== undefined) { _shieldRegen[0] = value }
      if (delay < 0) {
        _shieldRegen[1] = 0
        _shieldRegen[3].pause()
      } else if (delay >= 0) {
        _shieldRegen[1] = delay
        _shieldRegen[3].events[0].delay = Phaser.Timer.SECOND * delay
        if (_shieldRegen[3].paused) { _shieldRegen[3].resume() }
      }
      if (modifier === undefined) {
        if (_shieldRegen[2] !== 1) {
          _shieldRegen[2] = 1
          _shieldRegen[3].events[0].delay = Phaser.Timer.SECOND *
          _shieldRegen[1] / modifier
        }
      } else {
        _shieldRegen[2] = modifier < 0 ? 0 : modifier
        _shieldRegen[3].events[0].delay = Phaser.Timer.SECOND *
        _shieldRegen[1] / modifier
      }
    }

    // Overcharge

    let _overcharge = 0.0
    this.__defineGetter__('overcharge', function _GetOvercharge () { return _overcharge })
    this.__defineSetter__('overcharge', function _SetOvercharge (value = 0.0) {
      if (value < 0.0) { value = 0.0 }
      if (value > 1.0) { value = 1.0 }
      _overcharge = value
      this.setEnergyRegen(undefined, undefined, value > 0.0 ? 10 * value : 1)
      this.setShieldRegen(undefined, undefined, value > 0.0 ? 5 * value : 1)
    })

    // Main methods

    this.clear = function Clear () {
      this.energy = 0
      this.armor = 0
      this.shield = 0
    }

    this.fill = function Fill () {
      this.energy = _size
      this.armor = _size
      this.shield = _size
    }

    this.__defineGetter__('defense', function _GetDefense () { return (_shield + _armor) })

    this.damage = function Damage (value) {
      if (value === undefined) {
        value = 0
      }

      let remaining = value - _shield

      if (remaining < 0) {
        _shield = -remaining
        remaining = 0
        _updateShield()
      } else {
        if (_shield !== 0) {
          _shield = 0
          _updateShield()
        }
        _armor -= remaining // deduct the remaining damage
        _updateArmor()
      }

      // If Player died, lock the Gauge

      const died = _shield <= 0 && _armor <= 0
      if (died) {
        this.lock = true
      }

      return died
    }

    // Use the setter to update the frame names and positions
    this.size = size
  }
}
