// Copyright (c) 2016-2021, VIMinO
//
// This source code file is under the GPLv3 license, which can be found at:
// https://www.gnu.org/licenses/gpl-3.0.en.html
// Or the attached LICENSE file

// PhaserJS CE Libraries
import PIXI from 'expose-loader?PIXI!phaser-ce/build/custom/pixi.js' // eslint-disable-line
import p2 from 'expose-loader?p2!phaser-ce/build/custom/p2.js' // eslint-disable-line
import Phaser from 'expose-loader?Phaser!phaser-ce/build/custom/phaser-split.js' // eslint-disable-line

// Game Modules
import Label from '../objects/Label'

export default function OptionSlider (game) {
  const slider = game.add.group(game, null, 'OptionSlider')
  slider.visible = false

  slider.active = false
  slider.preactive = false

  // Callbacks (they need to be defined earlier than the shpw amd Toggle methods)

  let onShow
  let onHide

  slider.onShow = function OnShow (callback) {
    if (typeof (callback) === 'function') {
      onShow = callback
    }
  }

  slider.onHide = function OnHide (callback) {
    if (typeof (callback) === 'function') {
      onHide = callback
    }
  }

  // Frame sprites

  const frame = game.add.group()

  const frameSprite = game.add.sprite(0, 0, 'panel', 'BluePanel')
  const framePattern = game.add.sprite(1, 1, 'panel', 'BluePanelPat1')

  frame.add(frameSprite)
  frame.add(framePattern)
  slider.add(frame)

  // Close button sprite and events

  const closeButton = game.add.sprite(50, 50, 'ui', 'BlueBackBut')
  slider.add(closeButton)

  const clickClose = function ClickClose (element, event) {
    // Handle the click event and cancel if the user moves the mouse away

    if (event != null && event.isDown) {
      closeButton.frameName = 'BlueBackButP'
      closeButton.cancel = false
    } else {
      if (event == null) {
        closeButton.cancel = true
      }

      if (closeButton.cancel) {
        closeButton.frameName = 'BlueBackBut'
      } else {
        slider.hide()
      }
    }
  }

  closeButton.inputEnabled = true
  closeButton.events.onInputDown.add(clickClose, this)
  closeButton.events.onInputUp.add(clickClose, this)
  closeButton.events.onInputOut.add(
    () => { clickClose(null) }
    , this)

  // Volume sprites, events and setting

  const volume = game.add.group()
  volume.position.setTo(4, 8)
  slider.add(volume)
  volume.active = false

  const volumeBar = game.add.sprite(0, 0, 'ui', 'volumeBar')
  const volumeBarPattern = game.add.sprite(1, 1, 'ui', 'barPat1')
  volumeBarPattern.alpha = 0.2

  const volumeButton = game.add.sprite(41, -4, 'ui', 'volumeButton')
  const volumeButPattern = game.add.sprite(volumeButton.x + 1, -3, 'ui', 'volumePat1')
  volumeButPattern.alpha = 0.4

  volume.add(volumeBar)
  volume.add(volumeBarPattern)
  volume.add(volumeButton)
  volume.add(volumeButPattern)

  if (game.sound.mute) {
    volumeButton.x = 3
  } else {
    volumeButton.x = (game.sound.volume * 38.0) + 3.5
  }
  volumeButPattern.x = volumeButton.x + 1

  const clickVolume = function ClickVolume (element, event) {
    // Handle the click event

    if (event != null && event.isDown) {
      volumeButton.frameName = 'volumeButtonP'
      updateLabel(1, volumeButton.x)
      volumeButPattern.alpha = 0.8
      volume.active = true
    } else {
      volumeButton.frameName = 'volumeButton'
      volumeButPattern.alpha = 0.4
      updateLabel()
      volume.active = false

      // Value between 0 (Mute) and 38 (100%)
      const vol = Math.round(((volumeButton.x - 3.0) / 38.0) * 100.0) / 100.0
      if (vol < 0.01) {
        game.sound.mute = true
      } else {
        game.sound.mute = false
        game.sound.volume = vol
      }
    }
  }

  volumeButton.inputEnabled = true
  volumeButton.events.onInputDown.add(clickVolume, this)
  volumeButton.events.onInputUp.add(clickVolume, this)

  // Difficulty sprites, events and setting

  const difficulty = game.add.group()
  difficulty.position.setTo(4, 26)
  difficulty.active = false

  const diffBar = game.add.sprite(0, 0, 'ui', 'diffBar')

  const diffBarPattern = game.add.sprite(1, 1, 'ui', 'barPat1')
  diffBarPattern.alpha = 0.4

  const diffButton = game.add.sprite(22, -4, 'ui', 'diffButton')
  diffButton.inputEnabled = true

  const diffButPattern = game.add.sprite(23, -3, 'ui', 'diffPat1')
  diffButPattern.alpha = 0.4

  difficulty.add(diffBar)
  difficulty.add(diffBarPattern)
  difficulty.add(diffButton)
  difficulty.add(diffButPattern)
  slider.add(difficulty)

  if (window.game.diff === 1) {
    diffButton.x = 3
    diffButPattern.x = 4
  } else if (window.game.diff === 2) {
    diffButton.x = 22
    diffButPattern.x = 23
  } else if (window.game.diff === 3) {
    diffButton.x = 41
    diffButPattern.x = 42
  }

  const clickDifficulty = function ClickDifficulty (element, event) {
    // Handle the click event

    if (event != null && event.isDown) {
      diffButton.frameName = 'diffButtonP'
      diffButPattern.alpha = 0.8
      updateLabel(2, diffButton.x)
      difficulty.active = true
    } else {
      diffButton.frameName = 'diffButton'
      diffButPattern.alpha = 0.4
      updateLabel()
      difficulty.active = false

      // Value between 1 and 3
      if (diffButton.x === 3) {
        window.game.diff = 1
      } else if (diffButton.x === 22) {
        window.game.diff = 2
      } else if (diffButton.x === 41) {
        window.game.diff = 3
      }
    }
  }

  diffButton.events.onInputDown.add(clickDifficulty, this)
  diffButton.events.onInputUp.add(clickDifficulty, this)

  // Sliders need realtime feedback

  slider.update = function Update () {
    if (volume.active) {
      const shift = game.input.x - (volumeButton.x + 6)

      if (Math.abs(shift) > 2) {
        volumeButton.x += shift - 4
      }

      if (volumeButton.x < 3) {
        volumeButton.x = 3
      } else if (volumeButton.x > 41) {
        volumeButton.x = 41
      }

      updateLabel(1, volumeButton.x)
      volumeButPattern.x = volumeButton.x + 1
    }

    if (difficulty.active) {
      if (game.input.x < diffButton.x) {
        if (game.input.x < 26) {
          diffButton.x = 3
        } else if (game.input.x < 44) {
          diffButton.x = 22
        }
      } else {
        if (game.input.x > 40) {
          diffButton.x = 41
        } else if (game.input.x > 22) {
          diffButton.x = 22
        }
      }

      updateLabel(2, diffButton.x)
      diffButPattern.x = diffButton.x + 1
    }
  }

  // Fullscreen sprites and events

  const full = game.add.group()
  full.position.setTo(4, 40)

  const fullButton = game.add.sprite(0, 0, 'ui', 'fullOff')

  const fullPattern = game.add.sprite(1, 1, 'ui', 'fullOffPat1')
  fullPattern.alpha = 0.4

  full.add(fullButton)
  full.add(fullPattern)
  slider.add(full)

  const toggleFullscreen = function ToggleFullscreen (element, event) {
    // Handle the click event and cancel if the user moves the mouse away

    if (event != null && event.isDown) {
      updateLabel(3)

      fullPattern.alpha = 0.6
      fullButton.cancel = false
    } else {
      updateLabel()

      fullPattern.alpha = 0.4

      if (event != null && !fullButton.cancel) {
        const newState = !game.scale.isFullScreen

        if (newState) {
          game.scale.startFullScreen(false)
        } else {
          game.scale.stopFullScreen()
        }
      } else {
        fullButton.cancel = true
      }
    }
  }

  fullButton.inputEnabled = true
  fullButton.events.onInputDown.add(toggleFullscreen, this)
  fullButton.events.onInputUp.add(toggleFullscreen, this)
  fullButton.events.onInputOut.add(
    () => { toggleFullscreen(null) }
    , this)

  // Invert Throw Direction sprites and events

  const invert = game.add.group()
  invert.position.setTo(27, 40)

  const invertButton = game.add.sprite(0, 0, 'ui', 'invertOff')

  const invertPattern = game.add.sprite(1, 1, 'ui', 'invertOffPat1')
  invertPattern.alpha = 0.2

  invert.add(invertButton)
  invert.add(invertPattern)
  slider.add(invert)

  const toggleInvert = function ToggleInvert (element, event) {
    // Handle the click event and cancel if the user moves the mouse away

    if (event != null && event.isDown) {
      updateLabel(4)
      invertPattern.alpha = 0.5
      invertButton.cancel = false
    } else {
      updateLabel()
      invertPattern.alpha = 0.2

      if (event != null && !invertButton.cancel) {
        window.game.invert = !window.game.invert
      } else {
        invertButton.cancel = true
      }
    }
  }

  invertButton.inputEnabled = true
  invertButton.events.onInputDown.add(toggleInvert, this)
  invertButton.events.onInputUp.add(toggleInvert, this)
  invertButton.events.onInputOut.add(
    () => { toggleInvert(null) }
    , this)

  // Upper and Lower labels to show details

  const title = new Label(game, 32, 0)
  title.alpha = 0.8
  title.visible = false
  slider.add(title)

  const label = new Label(game, 32, 0)
  label.alpha = 0.8
  label.visible = false
  slider.add(label)

  const updateLabel = function UpdateLabel (button = 0, setting) {
    // Update the Title and Label with details on the current control

    if (button < 1) {
      title.visible = false
      label.visible = false
    } else if (button === 1) {
      title.set('Volume')
      title.place(18.5, 0)

      if (setting === 3) {
        label.set('Mute')
      } else {
        const volume = Math.round(((setting - 3.0) / 38.0) * 100.0)
        label.set(volume.toString() + '%')
      }
      label.place(32 - (label.width / 2), 19)

      title.visible = true
      label.visible = true
    } else if (button === 2) {
      title.set('Difficulty')
      title.place(14, 18)

      if (setting === 3) {
        label.set('Easy')
      } else if (setting === 22) {
        label.set('Normal')
      } else if (setting === 41) {
        label.set('Hard')
      }
      label.place(32 - (label.width / 2), 37)

      title.visible = true
      label.visible = true
    } else if (button === 3) {
      title.set('Fullscreen')
      title.place(11, 32)

      title.visible = true
      label.visible = false
    } else { // if (button === 4)
      title.set('Invert Throw')
      title.place(6, 32)

      title.visible = true
      label.visible = false
    }
  }

  // Animation variables and method

  let controlFrame = 0
  let patternFrame = 0

  const animate = function Animate () {
    // Cycle through the frames

    if (!slider.active) { return }

    ++controlFrame
    if (controlFrame > 4) controlFrame = 1
    volumeBarPattern.frameName = ('barPat' + controlFrame.toString())
    diffBarPattern.frameName = ('barPat' + controlFrame.toString())
    volumeButPattern.frameName = ('volumePat' + controlFrame.toString())
    diffButPattern.frameName = ('diffPat' + controlFrame.toString())

    ++patternFrame
    if (patternFrame > 6) patternFrame = 1
    framePattern.frameName = 'BluePanelPat' + patternFrame.toString()

    // Update the Fullscreen and Invert button state

    let fullState = 'Off'
    if (game.scale.isFullScreen) { fullState = 'On' }
    // Note: This change can happen at any time
    fullButton.frameName = 'full' + fullState
    fullPattern.frameName = ('full' + fullState + 'Pat' + controlFrame.toString())

    let invertState = 'Off'
    if (window.game.invert) { invertState = 'On' }
    invertButton.frameName = 'invert' + invertState
    invertPattern.frameName = ('invert' + invertState + 'Pat' + controlFrame.toString())
  }

  // Show/Hide methods, callbacks and tweens

  const showTween = game.add.tween(slider).to({ x: 0, y: 0 }, 500, Phaser.Easing.Cubic.In, false)
  showTween.onComplete.add(() => {
    slider.active = true
  })

  slider.show = function Show () {
    if (slider.preactive) { return }

    if (onShow) { onShow() }

    slider.position.setTo(64, 64)
    slider.visible = true
    slider.preactive = true
    timer.resume()
    showTween.start()
  }

  const hideTween = game.add.tween(slider).to({ x: 64, y: 64 }, 500, Phaser.Easing.Cubic.Out, false)
  hideTween.onComplete.add(() => {
    timer.pause()
    slider.preactive = false
    slider.visible = false
  }, this)

  slider.hide = function Hide () {
    if (!slider.active) { return }

    if (onHide) { onHide() }
    slider.active = false
    hideTween.start()
  }

  // Animation timer

  let timer = null

  slider.reset = function Reset () {
    timer = game.time.create()
    timer.loop(160, animate, this)
    timer.start()
    timer.pause()
  }

  slider.reset()

  return slider
}
