// Copyright (c) 2016-2021, VIMinO
//
// This source code file is under the GPLv3 license, which can be found at:
// https://www.gnu.org/licenses/gpl-3.0.en.html
// Or the attached LICENSE file

// Basic Label (BitmapText with predefined values)
export default function Label (game, x = 0, y = 0, text = '', font = 'MicroFont', size = 5) {
  const label = game.add.bitmapText(x, y, font, text, size)
  label.alpha = 0.5

  label.set = function set (text) {
    label.text = text
  }

  label.get = function get () {
    return label.text
  }

  label.add = function add (value) {
    const sum = parseInt(label.text) + parseInt(value)
    label.text = sum.toString()
  }

  label.place = function place (x, y) {
    label.x = x
    label.y = y
  }

  return label
}
