// Copyright (c) 2016-2021, VIMinO
//
// This source code file is under the GPLv3 license, which can be found at:
// https://www.gnu.org/licenses/gpl-3.0.en.html
// Or the attached LICENSE file

// PhaserJS CE Libraries
import PIXI from 'expose-loader?PIXI!phaser-ce/build/custom/pixi.js' // eslint-disable-line
import p2 from 'expose-loader?p2!phaser-ce/build/custom/p2.js' // eslint-disable-line
import Phaser from 'expose-loader?Phaser!phaser-ce/build/custom/phaser-split.js' // eslint-disable-line

// Game Modules
import Label from '../objects/Label'

export default function AboutSlider (game) {
  const slider = game.add.group(game, null, 'AboutSlider')
  slider.visible = false

  slider.active = false
  slider.preactive = false

  // Callbacks (they need to be defined earlier than the show amd Toggle methods)

  let onShow
  let onHide

  slider.onShow = function OnShow (callback) {
    if (typeof (callback) === 'function') {
      onShow = callback
    }
  }

  slider.onHide = function OnHide (callback) {
    if (typeof (callback) === 'function') {
      onHide = callback
    }
  }

  // Frame sprites

  const frame = game.add.group()

  const frameSprite = game.add.sprite(0, 0, 'panel', 'GreenPanel')
  const framePattern = game.add.sprite(1, 1, 'panel', 'GreenPanelPat1')

  frame.add(frameSprite)
  frame.add(framePattern)
  slider.add(frame)

  // Close button sprite and events

  const closeButton = game.add.sprite(0, 0, 'ui', 'GreenBackBut')
  slider.add(closeButton)

  const clickClose = function ClickClose (element, event) {
    // Handle the click event and cancel if the user moves the mouse away

    if (event != null && event.isDown) {
      closeButton.frameName = 'GreenBackButP'
      closeButton.cancel = false
    } else {
      if (event == null) {
        closeButton.cancel = true
      }

      if (closeButton.cancel) {
        closeButton.frameName = 'GreenBackBut'
      } else {
        slider.hide()
      }
    }
  }

  closeButton.inputEnabled = true
  closeButton.events.onInputDown.add(clickClose, this)
  closeButton.events.onInputUp.add(clickClose, this)
  closeButton.events.onInputOut.add(
    () => { clickClose(null) }
    , this)

  // Labels and Spritesheet

  const madeBy = new Label(game, 18, 8, 'Made By')
  madeBy.alpha = 0.8
  slider.add(madeBy)

  const vimino = game.add.sprite(2, 16, 'vimino')
  slider.add(vimino)

  const website = new Label(game, 4, 47, 'www.vimino.net')
  website.alpha = 0.6
  slider.add(website)

  // Toggle sprites and events

  const toggles = game.add.group()
  slider.add(toggles)

  for (let i = 0; i < 8; ++i) {
    toggles.create(8 + (i * 6) + (i > 3 ? 1 : 0), 56,
      'ui', window.game.toggle[i] ? 'toggleOn' : 'toggleOff', true, i)
  }

  const toggleColor = [
    0xffffff,
    0xffaaaa,
    0xffffaa,
    0xaaffaa,
    0xaaffff,
    0xaaaaff,
    0xffaaff,
    0xaaaaaa
  ]

  const clickToggle = function ClickToggle (element, event) {
    const index = parseInt(element.z)
    window.game.toggle[index] = !window.game.toggle[index]
    if (window.game.toggle[index]) {
      element.alpha = 0.6
      element.frameName = 'toggleOn'
    } else {
      element.alpha = 0.2
      element.frameName = 'toggleOff'
    }
  }

  toggles.forEach(function (toggle) {
    toggle.tint = toggleColor[toggle.z]
    toggle.alpha = window.game.toggle[toggle.z] ? 0.6 : 0.2
    toggle.inputEnabled = true
    toggle.events.onInputDown.add(clickToggle, this)
  }, this)

  // Animation variables and method

  let viminoFrame = 0
  let patternFrame = 0

  const animate = function Animate () {
    // Cycle through the frames

    if (!slider.active) { return }

    ++viminoFrame
    if (viminoFrame > 6) viminoFrame = 0
    vimino.frame = viminoFrame

    ++patternFrame
    if (patternFrame > 6) patternFrame = 1
    framePattern.frameName = 'GreenPanelPat' + patternFrame.toString()
  }

  // Show/Hide methods, callbacks and tweens

  const showTween = game.add.tween(slider).to({ x: 0, y: 0 }, 500, Phaser.Easing.Cubic.In, false)
  showTween.onComplete.add(() => {
    slider.active = true
  })

  slider.show = function Show () {
    if (slider.preactive) { return }

    if (onShow) { onShow() }

    slider.position.setTo(-64, -64)
    slider.visible = true
    slider.preactive = true
    timer.resume()
    showTween.start()
  }

  const hideTween = game.add.tween(slider).to({ x: -64, y: -64 }, 500, Phaser.Easing.Cubic.Out, false)
  hideTween.onComplete.add(() => {
    timer.pause()
    slider.preactive = false
    slider.visible = false
  }, this)

  slider.hide = function Hide () {
    if (!slider.active) { return }

    if (onHide) { onHide() }
    slider.active = false
    hideTween.start()
  }

  // Animation timer

  let timer = null

  slider.reset = function Reset () {
    timer = game.time.create()
    timer.loop(160, animate, this)
    timer.start()
    timer.pause()
  }

  slider.reset()

  return slider
}
