// Copyright (c) 2016-2021, VIMinO
//
// This source code file is under the GPLv3 license, which can be found at:
// https://www.gnu.org/licenses/gpl-3.0.en.html
// Or the attached LICENSE file

import PIXI from 'expose-loader?PIXI!phaser-ce/build/custom/pixi.js' // eslint-disable-line
import p2 from 'expose-loader?p2!phaser-ce/build/custom/p2.js' // eslint-disable-line
import Phaser from 'expose-loader?Phaser!phaser-ce/build/custom/phaser-split.js' // eslint-disable-line

export default class Pickup extends Phaser.Sprite {
  constructor (game, x, y, type) {
    super(game, x, y, 'pickups')
    this.anchor.setTo(0.5, 0.5)

    // Physics
    this.game.physics.arcade.enable(this)
    this.body.collideWorldBounds = true
    this.body.allowGravity = false
    this.body.immovable = true

    // All the timing events Phaser.Timer.SECOND * this.game.rnd.integerInRange(2, 8)

    // this.eventWait = this.game.add.tween(this).from(
    //   {}, 500,
    //   Phaser.Easing.Linear.None, false)
    // this.eventWait.onComplete.add(() => {
    //   console.log('Pickup.eventWait')
    // }, this)

    // this.cooldown = 2
    this.type = undefined
    this.alpha = 0.0
    this.alive = false

    // this.tween = this.makeTween()
    // this.tween.start()

    // this.pop()
  }

  makeTween () {
    const tween = this.game.add.tween(this)

    tween.to({ alpha: 1 }, 500, Phaser.Easing.Linear.None, false).onStart.add(() => {
      // console.log('Pickup.tweenShow')
      this.alive = true
    }, this)

    tween.to({}, Phaser.Timer.SECOND * this.game.rnd.integerInRange(2, 12) * (4 - window.option.diff), Phaser.Easing.Linear.None, false)

    // .onStart.add(() => {
    //   console.log('Pickup.tweenIdle') // , tweenIdle.delay)
    // }, this)

    tween.to({ alpha: 0 }, 500, Phaser.Easing.Linear.None, false).onComplete.add(() => {
      // console.log('Pickup.tweenHide')
      // this.alive = false
      this.stop()
      // this.eventWait.delay = Phaser.Timer.SECOND * this.game.rnd.integerInRange(10, 50)
    }, this)

    return tween

    // this.eventWait.chain(this.tweenShow)
    // this.tweenShow.chain(this.tweenIdle)
    // this.tweenIdle.chain(this.tweenHide)
    // this.eventHide.chain(this.eventWait)
  }

  moveTo (x, y, speed) {
    const cx = this.position.x
    const cy = this.position.y
    let vx = 0
    let vy = 0
    if (!speed) speed = 1

    if (cx < x) {
      vx = speed
    } else if (cx > x) {
      vx = -speed
    }

    if (cy < y) {
      vy = speed
    } else if (cy > y) {
      vy = -speed
    }

    this.body.velocity.setTo(vx, vy)
  }

  stop () {
    // console.log('Pickup.stop')
    this.body.stop()
    // this.tweenHide.pause()
    this.tween.stop()
    // this.tweenIdle.pause()
    // this.tweenHide.pause()
    // this.tweenShow.stop()
    // this.tweenIdle.stop()
    // this.tweenHide.stop()
    this.alpha = 0.0
    this.kill()
  }

  getType () {
    return this.frame
  }

  setType (type) {
    // console.log('Pickup.setType', type, this.type)

    if (type !== undefined && this.type === type) {
      return
    }

    if (this.type === undefined || type === undefined) {
      this.type = this.game.rnd.integerInRange(0, 5)
    } else {
      this.type = type
    }

    switch (this.type) {
      case 0:
        type = 'shield'
        break
      case 1:
        type = 'shield+'
        break
      case 2:
        type = 'shield++'
        break
      case 3:
        type = 'energy'
        break
      case 4:
        type = 'energy+'
        break
      case 5:
        type = 'energy++'
        break
    }

    this.frameName = type
    this.body.reset(this.position.x, this.position.y)
    this.body.setSize(this.width, this.height, 0, 0)
    this.game.physics.arcade.enable(this)
  }

  pop (type) {
    // console.log('Pickup.reset', this.alive)

    if (this.alive) {
      return false
    }

    this.revive()

    // if (this.lastKill < this.game.time.totalElapsedSeconds() + this.cooldown) {
    //   return false
    // }

    // this.startTime = this.game.time.totalElapsedSeconds()
    // this.active = false
    // this.delay = this.game.rnd.integerInRange(200, 1200)

    this.reset(this.game.rnd.integerInRange(10, 54),
      this.game.rnd.integerInRange(10, 54))
    // this.position.setTo(this.game.rnd.integerInRange(10, 54),
    //                     this.game.rnd.integerInRange(10, 54))
    this.setType()

    this.alpha = 0.0

    // // this.tweenOff.stop()
    // this.tweenOn.start()
    // if (!(this.tweenShow.isRunning || this.tweenIdle.isRunning || this.tweenHide.isRunning)) {
    //   this.tweenShow.resume()
    //   this.tweenIdle.resume()
    //   this.tweenHide.resume()
    //
    //   // console.log(this.tweenShow.chainedTween)
    //   // console.log(this.tweenIdle.chainedTween)
    //
    //   // this.tweenShow.chain(this.tweenIdle)
    //   // this.tweenIdle.chain(this.tweenHide)
    //
    //   // this.tweenShow.stop()
    // }

    // if (this.tween.isRunning) {
    // this.tween.resume()
    this.tween = this.makeTween()
    this.tween.start()
    // }

    return true
  }

  // pick () {
  //   // this.cooldown = this.game.rnd.integerInRange(1, 10)
  //   // this.lastKill = this.game.time.totalElapsedSeconds()
  //   this.kill()
  // }

  // update () {
  //   if (this.game.time.totalElapsedSeconds() > this.startTime + this.delay) {
  //     // if (this.alive) {
  //     //   this.phaseOut()
  //     // } else {
  //     this.phaseIn()
  //     // }
  //
  //     this.startTime = this.game.time.totalElapsedSeconds()
  //   }
  // }
  //
  // phaseIn () {
  //   this.game.add.tween(this).to({alpha: 1}, 1000, Phaser.Easing.Linear.None, true)
  //   // phase.onComplete(() => {
  //   //   this.active = true
  //   // }, this)
  //
  //   // let phase = this.game.add.tween(this).to({alpha: 1}, Phaser.Timer.SECOND, Phaser.Easing.Linear.None, true)
  //   //
  //   // phase.onComplete.add(function () {
  //   //   this.active = true
  //   // }, this)
  // }
  //
  // phaseOut () {
  //   // let phaseOut = this.game.add.tween(this).to({alpha: 0}, 10000, Phaser.Easing.Linear.None, true)
  //   // phaseOut.onComplete.add(() => {
  //   //   // this.active = false
  //   //   this.kill()
  //   // }, this)
  //
  //   // let phase = this.game.add.tween(this).to({alpha: 0}, Phaser.Timer.SECOND, Phaser.Easing.Linear.None, true)
  //   //
  //   // phase.onComplete.add(function () {
  //   //   this.reset()
  //   // }, this)
  // }
  //
  // isActive () {
  //   return this.active
  // }
}
