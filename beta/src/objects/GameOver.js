// Copyright (c) 2016-2021, VIMinO
//
// This source code file is under the GPLv3 license, which can be found at:
// https://www.gnu.org/licenses/gpl-3.0.en.html
// Or the attached LICENSE file

// PhaserJS CE Libraries
import PIXI from 'expose-loader?PIXI!phaser-ce/build/custom/pixi.js' // eslint-disable-line
import p2 from 'expose-loader?p2!phaser-ce/build/custom/p2.js' // eslint-disable-line
import Phaser from 'expose-loader?Phaser!phaser-ce/build/custom/phaser-split.js' // eslint-disable-line

// GameOver slider. It shows/hides once and has no background
export default function GameOver (game) {
  const slider = game.add.group(game, null, 'GameOver')
  slider.visible = false

  // Callbacks (they need to be defined earlier than the shpw amd Toggle methods)

  let onShow
  let onHide

  slider.onShow = function OnShow (callback) {
    if (typeof (callback) === 'function') {
      onShow = callback
    }
  }

  slider.onHide = function OnHide (callback) {
    if (typeof (callback) === 'function') {
      onHide = callback
    }
  }

  // GameOver sprite

  const gameoverSprite = game.add.sprite(game.world.centerX, 12, 'ui', 'gameover')
  gameoverSprite.anchor.setTo(0.5, 0)
  slider.add(gameoverSprite)

  // Replay button, pattern and events

  const replay = game.add.group()
  replay.position.setTo(5, 30)

  const replayButton = game.add.sprite(0, 0, 'ui', 'replay')

  const replayPattern = game.add.sprite(1, 1, 'ui', 'replayPat1')
  replayPattern.alpha = 0.4

  const toggleReplay = function ToggleReplay (element, event) {
    if (event != null && event.isDown) {
      replayButton.frameName = 'replayP'
      replayPattern.alpha = 0.8
      replayButton.cancel = false
    } else {
      replayButton.frameName = 'replay'
      replayPattern.alpha = 0.4

      if (event != null && !replayButton.cancel) {
        if (onHide) { onHide() }
        timer.pause()
        slider.visible = false
        game.state.start('Game')
      } else {
        replayButton.cancel = true
      }
    }
  }

  replayButton.cancel = false
  replayButton.inputEnabled = true
  replayButton.events.onInputDown.add(toggleReplay, this)
  replayButton.events.onInputUp.add(toggleReplay, this)
  replayButton.events.onInputOut.add(
    () => { toggleReplay(null) },
    this
  )

  replay.add(replayButton)
  replay.add(replayPattern)
  slider.add(replay)

  // Quit button, pattern and events

  const quit = game.add.group()
  quit.position.setTo(35, 30)

  const quitButton = game.add.sprite(0, 0, 'ui', 'quit')

  const quitPattern = game.add.sprite(1, 1, 'ui', 'quitPat1')
  quitPattern.alpha = 0.4

  const toggleQuit = function ToggleQuit (element, event) {
    if (event != null && event.isDown) {
      quitButton.frameName = 'quitP'
      quitPattern.alpha = 0.8
      quitButton.cancel = false
    } else {
      quitButton.frameName = 'quit'
      quitPattern.alpha = 0.4

      if (event != null && !quitButton.cancel) {
        if (onHide) { onHide() }
        timer.pause()
        slider.visible = false
        game.state.start('Menu')
      } else {
        quitButton.cancel = true
      }
    }
  }

  quitButton.cancel = false
  quitButton.inputEnabled = true
  quitButton.events.onInputDown.add(toggleQuit, this)
  quitButton.events.onInputUp.add(toggleQuit, this)
  quitButton.events.onInputOut.add(
    () => { toggleQuit(null) },
    this
  )

  quit.add(quitButton)
  quit.add(quitPattern)
  slider.add(quit)

  // Show/Hide methods, callbacks and tweens

  slider.show = function Show () {
    if (slider.visible) { return }

    if (onShow) { onShow() }
    timer.resume()
    slider.visible = true
    slider.y = -64
    game.add.tween(this).to({ y: 0 }, 1000, Phaser.Easing.Bounce.Out, true)
  }

  // Animation variables and method

  let pattern = 0

  const animate = function Animate () {
    // Cycle through the frames

    if (!slider.visible) { return }

    pattern++
    if (pattern > 4) pattern = 1

    replayPattern.frameName = ('replayPat' + pattern.toString())
    quitPattern.frameName = ('quitPat' + pattern.toString())
  }

  // Note that this timer needs its loop to keep being re-added

  let timer = null

  // Reset the Timer any time this object instead of redeclaring it
  slider.reset = function Reset () {
    timer = game.time.create(false)
    timer.loop(160, animate, this)
    timer.start()
    timer.pause()
  }

  slider.reset()

  return slider
}
