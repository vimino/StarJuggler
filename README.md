# Star Juggler (Beta, ongoing development)

### [Prototype](https://vimino.gitlab.io/StarJuggler/proto)

The prototype (versions 0.1.X) were built during the [**#LOWREZ** in 2017](https://itch.io/jam/lowrezjam2017/rate/167910).  
I had to cut several things, namely sound, to make it in time for the deadline.  
This version was in place 132 (Overall) of that Jam.

### [Alpha](https://vimino.gitlab.io/StarJuggler/alpha)

Since then, I worked on creating the Alpha (versions 0.2.X to 0.6.X). Later entering the [**FFSjam** in 2018](https://itch.io/jam/finally-finish-something-2018/rate/167910) to give myself a deadline.  
I implemented many of the ideas I had, despite rushing some details. This gave a clear idea of where it could go.  
This version was in place 25 (Overall) of that Jam.

### [Beta](https://vimino.gitlab.io/StarJuggler/beta)

After adding several upgrades after the **FFS Jam** (versions 0.7.X to 0.8.X) I added them and fixed the new Warping feature for the [**Rekindle Jam**](https://itch.io/jam/rekindle-jam/rate/1121150) in 2021.
It's been quite a while but it was nice to see how far it has come.

# [Play the Alpha on Itch.io](https://vimino.itch.io/star-juggler)
# [Play the Beta on Itch.io](https://vimino.itch.io/star-juggler-next)

### Commands
This is a npm package and supports the following commands:
- **npm run lint** : Checks the source files (in the 'src' folder) for errors
- **npm run build** : Builds the game.js file and places it in the *public* folder
- **npm test** : Runs webpack-dev-server which serves the *public* folder in **localhost:8080**

### License

Copyright &copy; 2016-2021, VIMinO

Source Code files (html,js) are licensed under the [GPLv3 License ](https://www.gnu.org/licenses/gpl-3.0.en.html)<br/>

Media (png,wav,ogg) files are licensed under the [Creative Commons Attribution-ShareAlike 4.0 International (CC BY-SA 4.0) ](https://creativecommons.org/licenses/by-sa/4.0/)

All Rights for the remaining files are Reserved

[![gplv3 button](https://www.gnu.org/graphics/gplv3-88x31.png)](https://www.gnu.org/licenses/gpl.html)
[![licensebuttons by-sa](https://licensebuttons.net/l/by-sa/3.0/88x31.png)](https://creativecommons.org/licenses/by-sa/4.0)

### Missing

While the build file for the Prototype is version 1.4 the project files are actually for version 1.3.
Let's assume I didn't save them since there was a very minor update between the two.
