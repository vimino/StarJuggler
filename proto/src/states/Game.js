// Copyright (c) 2016-2021, VIMinO
//
// This source code file is under the GPLv3 license, which can be found at:
// https://www.gnu.org/licenses/gpl-3.0.en.html
// Or the attached LICENSE file

import Player from '../objects/Player'
import Star from '../objects/Star'
import Enemy from '../objects/Enemy'
import Pickup from '../objects/Pickup'
import Gauge from '../objects/Gauge'
import Counter from '../objects/Counter'
import GameOver from '../objects/GameOver'

import PIXI from 'expose-loader?PIXI!phaser-ce/build/custom/pixi.js'
import p2 from 'expose-loader?p2!phaser-ce/build/custom/p2.js'
import Phaser from 'expose-loader?Phaser!phaser-ce/build/custom/phaser-split.js'

export default class Game extends Phaser.State {
  constructor () {
    super()

    // Initialize Variables

    this.ui = undefined

    this.player = undefined
    this.speed = 20

    this.star = undefined
    this.cooldown = 0.5
    this.lockStar = false
    this.armed = 0
    this.armStart = undefined

    this.pickup = undefined

    this.gauge = undefined
    this.shiGenDelay = 60
    this.eneGenDelay = 10

    this.score = undefined

    this.enemies = undefined

    this.waveCount = 0 // 200
    this.waveLevelLimit = 50
    // this.waveStart = 0
    // this.waveDuration = 5

    this.gameOver = undefined
  }

  create () {
    // Set Objects Up

    this.waveCount = 0

    this.ui = this.game.add.group()
    this.add.existing(this.ui)

    this.pickup = new Pickup(this.game, this.game.rnd.integerInRange(5, 59), this.game.rnd.integerInRange(5, 59), 'shield')
    this.add.existing(this.pickup)

    // this.star = this.game.add.sprite(this.player.x, this.player.y - 4, 'projectiles')
    // this.star.frameName = 's1'

    // this.star = this.game.add.weapon(1, 'projectiles', 's1')
    // // this.star.setBulletFrames(1, 1)
    // // this.star.anchor.setTo(0.5, 0.5)
    // this.star.bulletAngleOffset = 90
    // this.star.bulletSpeed = 32
    // // this.star.fireRate = 1000
    // // this.star.setBulletFrames(20, 20)
    // this.star.bulletKillType = Phaser.Weapon.KILL_WORLD_BOUNDS

    this.player = new Player(this.game, 34, 43)
    this.add.existing(this.player)

    // this.star.trackSprite(this.player, 0, -4)

    this.star = new Star(this.game, this.player)
    this.add.existing(this.star)

    this.enemies = this.game.add.group()
    this.sendWave()

    this.gauge = new Gauge(this.game, 26, 56)
    this.ui.add(this.gauge)

    this.score = new Counter(this.game, 4, 4, 'MicroFont', 5)
    this.ui.add(this.score)

    this.gauge.setShield(11)
    this.gauge.setEnergy(11)

    this.gameOver = new GameOver(this)

    // Initialize Physics
    this.physics.startSystem(Phaser.Physics.ARCADE)

    // Start Timers
    this.generate('shield')
    this.generate('energy')
  }

  update () {
    this.star.update()

    // Player center
    let px = this.player.position.x
    let py = this.player.position.y

    // Mouse position
    let mouse = this.game.input.mousePointer
    let mx = mouse.position.x
    let my = mouse.position.y

    // If the Player is Alive
    if (this.player.isAlive()) {
      // If the mouse is active
      if (mx !== 0 && my !== 0) {
        // Move the Player to the Mouse
        if (Math.abs(px - mx) > 1 || Math.abs(py - my) > 1) {
          this.player.moveToPointer(this.speed)
        } else {
          this.player.stop()
        }
      }

      // Charge/Fire Star on Mouse down and up
      if (!this.lockStar && mouse.isDown && this.gauge.getEnergy() > 0) {
        this.chargeStar()
      }

      if (this.armed > 0 && mouse.isUp) {
        this.fireStar()
      }
    }

    let self = this

    // Check for Player-Star collisions
    this.game.physics.arcade.collide(self.player, self.star, function (player, star) {
      if (!star.isActive()) {
        self.gauge.addEnergy(star.drain())
      }
    })

    // Check for Star-Enemy collisions
    if (this.star.isActive()) {
      this.game.physics.arcade.collide(self.enemies, self.star, function (star, enemy) {
        let enemyHealth = enemy.getHealth()
        let starEnergy = star.getEnergy()

        star.drain(enemyHealth)

        if (enemy.damage(starEnergy)) {
          let prize = enemy.getLevel() + enemy.getType()
          if (enemy.getType() === 6) {
            prize += 50 * enemy.getLevel()
          }
          self.score.addValue(prize)
        }
      })
    }

    // Check for Enemy-Player collisions
    this.game.physics.arcade.collide(self.enemies, self.player, function (player, enemy) {
      if (self.gauge.addShield(-enemy.getHealth())) {
        self.player.die()
        self.gauge.visible = false
        self.score.setAlpha(1)
        self.gameOver.show()
      }
      if (enemy.damage(self.gauge.getShield())) {
        let prize = enemy.getLevel() + enemy.getType()
        if (enemy.getType() === 6) {
          prize += 10 * enemy.getLevel()
        }
        self.score.addValue(prize)
      }
    })

    for (let e in this.enemies.children) {
      let enemy = this.enemies.children[e]

      // if (e.y > 70) {
      //   self.enemies.remove(e)
      //   e.kill()
      // }

      // if (this.player.touches(e)) {
      //   this.gauge.addShield(-e.getHealth() + e.type === 6 ? -e.getHealth : 0) // getLevel() + 1))
      //     e.kill()
      //   // this.makeEnemy()
      // }

      // if (this.game.physics.arcade.overlap(this.star.bullets, e)) {
      //   this.star.bullets.killAll()
      //
      //   if (e.damage(this.lastCharge)) {
      //     this.score.addValue((e.getLevel() + 1) * 2)
      //     // e.kill()
      //     this.makeEnemy()
      //   }
      // }

      let isAlive = enemy.getHealth() > 0

      if (isAlive && enemy.y > 84) {
        enemy.damage()
      }

      for (let j in enemy.weapons) {
        let self = this
        let weapon = enemy.weapons[j]

        // If the Enemy is dead, destroy all Weapons and respective Bullets
        if (!isAlive) {
          weapon.destroy()
          continue
        }

        // Check for EnemyBullet-Star collisions
        this.game.physics.arcade.collide(weapon.bullets, this.star, function (star, bullet) {
          let bulletEnergy = enemy.getLevel()

          bullet.kill()
          self.star.drain(bulletEnergy)
        })

        // Check for EnemyBullet-Player collisions
        this.game.physics.arcade.collide(weapon.bullets, this.player, function (target, bullet) {
          bullet.kill()
          if (self.gauge.addShield(-enemy.getLevel())) {
            self.player.die()
            self.gauge.visible = false
            self.score.setAlpha(1)
            self.gameOver.show()
          }
        })
      }

      if (!isAlive) {
        // Clean Enemies up
        this.enemies.remove(enemy)
        enemy.destroy()
      } else {
        // Fire all Enemy weapons
        enemy.fire()
      }
    }

    if (this.pickup.isActive() && this.player.touches(this.pickup)) {
      switch (this.pickup.getType()) {
        case 0:
          this.gauge.addShield(2)
          this.score.addValue(1)
          break
        case 1:
          this.gauge.addShield(4)
          this.score.addValue(2)
          break
        case 2:
          this.gauge.addShield(6)
          this.score.addValue(3)
          break
        case 3:
          this.gauge.addEnergy(2)
          this.score.addValue(1)
          break
        case 4:
          this.gauge.addEnergy(2)
          this.score.addValue(2)
          break
        case 5:
          this.gauge.addEnergy(6)
          this.score.addValue(3)
          break
      }

      this.pickup.reset()
    }

    // Send a new Wave if there are no Enemies or after some Time
    if (this.enemies.children.length === 0 && this.player.isAlive()) {
      this.sendWave()
    }

    this.game.world.bringToTop(this.ui)
    this.game.world.bringToTop(this.gameOver)
  }

  generate (variable) {
    let delay = 0
    if (variable === 'shield') {
      this.gauge.addShield(1)
      delay = this.shiGenDelay
    } else if (variable === 'energy') {
      this.gauge.addEnergy(1)
      delay = this.eneGenDelay
    }

    this.game.time.events.add(Phaser.Timer.SECOND * delay, this.generate, this, variable)
  }

  sendWave () { // skip) {
    // if (!skip && this.game.time.totalElapsedSeconds() < this.waveStart + this.waveDuration) {
    //   return
    // }

    // this.waveStart = this.game.time.totalElapsedSeconds()
    this.waveCount++
    // this.waveDuration = this.waveCount * 5

    let waveLevel = 1 + Math.floor(this.waveCount / 2)
    if (waveLevel > this.waveLevelLimit) {
      waveLevel = this.waveLevelLimit
    }

    // Every 10 waves, send a Boss
    // if (this.waveCount % 2 === 0) {
      // let bossLevel = this.game.rnd.integerInRange(1, 3)
      // if (bossLevel * 2 > waveLevel) {
      //   bossLevel = waveLevel
      // }
      // waveLevel -= bossLevel * 2
      //
      // this.makeEnemy(6, bossLevel)
    // }

    while (waveLevel > 0) {
      let type = this.game.rnd.weightedPick([1, 2, 3, 4, 5, 0, 6]) // this.game.rnd.integerInRange(0, 6)
      let level = this.game.rnd.integerInRange(1, 3)
      if (level > waveLevel) {
        level = waveLevel
      }
      if (type === 6) {
        waveLevel -= level * 2
      } else {
        waveLevel -= level
      }
      this.makeEnemy(type, level)
    }
  }

  makeEnemy (type, level) {
    if (level === undefined) {
      level = this.game.rnd.integerInRange(1, 3)
    }

    if (type === undefined) {
      type = this.game.rnd.integerInRange(0, 6)
    }

    let newX = this.game.rnd.integerInRange(5, 59)

    // if (e) {
    // //   e.y = -10
    // //   e.changeTo(type, level)
    // //   e.x = newX
    // //   e.addWeapon(type, 4)
    //   e.damage()
    // }
    // } else {
    let enemy = new Enemy(this.game, newX, -10, type, level)
    this.add.existing(enemy)

    if (type < 5) {
      enemy.addWeapon()
    } else if (type === 5) {
      enemy.addDirectionalWeapon(Phaser.ANGLE_DOWN)
      enemy.addDirectionalWeapon(Phaser.ANGLE_LEFT)
      enemy.addDirectionalWeapon(Phaser.ANGLE_RIGHT)
      enemy.addDirectionalWeapon(Phaser.ANGLE_UP)
    } else if (type === 6) {
      let sideWeapons = this.game.rnd.integerInRange(1, 5)
      let sideWeapons2 = this.game.rnd.integerInRange(1, 5)
      if (level === 1) {
        enemy.addWeapon()
        enemy.addWeapon(undefined, sideWeapons, 3, 0)
        enemy.addWeapon(undefined, sideWeapons, -3, 0)
      } else if (level === 2) {
        enemy.addWeapon(this.player)
        enemy.addWeapon(undefined, sideWeapons, 3, 0)
        enemy.addWeapon(undefined, sideWeapons, -3, 0)
      } else if (level === 3) {
        enemy.addWeapon(this.player)
        enemy.addWeapon(this.player, sideWeapons, 3, 0)
        enemy.addWeapon(this.player, sideWeapons, -3, 0)
        enemy.addWeapon(undefined, sideWeapons2, 6, 0)
        enemy.addWeapon(undefined, sideWeapons2, -6, 0)
      }
    }
    // }

    let speed = 2
    if (type === 0) {
      speed = this.rnd.integerInRange(8, 12)
    } else if (type < 6) {
      speed = this.rnd.integerInRange(6, 10)
    } else if (type > 5) {
      speed = this.rnd.integerInRange(4, 8)
    }
    enemy.moveTo(enemy.getPosition().x, 100, speed)

    this.enemies.add(enemy)
  }

  chargeStar () {
    // if (this.star.getEnergy() > 0) {
    //   return
    // }

    if (this.armed === 0 && this.gauge.getEnergy() >= 1) {
      this.armed = 1
      this.gauge.addEnergy(-1)
      this.armStart = this.game.time.totalElapsedSeconds()
    } else if (this.armed < 5 && this.gauge.getEnergy() >= 1 && this.game.time.totalElapsedSeconds() > (this.armStart + this.cooldown)) {
      this.armStart = this.game.time.totalElapsedSeconds()
      this.armed += 1
      this.gauge.addEnergy(-1)
    // } else if (this.armed < 5 && this.gauge.getEnergy() >= 2 && this.game.time.totalElapsedSeconds() > (this.armStart + this.cooldown * this.armed)) {
    //   this.armed += 1
    //   this.gauge.addEnergy(-2)
    } else if (this.armed === 5) {
      this.player.showGlow(true)
    }
  }

  fireStar () {
    this.player.showGlow(false)
    this.star.shoot(this.armed, this.player.x, this.player.y)

    this.lastCharge = this.armed
    this.armed = 0
    this.lockStar = true
    this.game.time.events.add(Phaser.Timer.SECOND * this.cooldown, function () {
      this.lockStar = false
    }, this)
  }

  render () {
    // this.game.debug.body(this.pickup)
    // this.game.debug.body(this.player)
    // this.game.debug.body(this.star)
    // this.game.debug.spriteBounds(this.player, 'pink')
    // this.game.debug.body(this.enemy)
    // this.game.debug.spriteBounds(this.enemy)
  }
}
