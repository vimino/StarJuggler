// Copyright (c) 2016-2021, VIMinO
//
// This source code file is under the GPLv3 license, which can be found at:
// https://www.gnu.org/licenses/gpl-3.0.en.html
// Or the attached LICENSE file

import assets from '../assets'

import PIXI from 'expose-loader?PIXI!phaser-ce/build/custom/pixi.js'
import p2 from 'expose-loader?p2!phaser-ce/build/custom/p2.js'
import Phaser from 'expose-loader?Phaser!phaser-ce/build/custom/phaser-split.js'

export default class Boot extends Phaser.State {
  preload () {
    this.load.path = 'assets/'

    // Align the Game screen
    this.scale.pageAlignHorizontally = true
    this.scale.pageAlignVertically = true

    // Apply a custom Scale (64*?)
    this.scale.scaleMode = Phaser.ScaleManager.USER_SCALE // .NO_SCALE
    this.scale.fullScreenScaleMode = Phaser.ScaleManager.USER_SCALE
    this.scale.setResizeCallback(function (context) {
      // Calculate how many times the Game size fits in the Window
      // PS: context.width and context.height change based on the scale
      let widthScale = window.innerWidth / context.game.width
      let heightScale = window.innerHeight / context.game.height
      let newScale = Math.floor(Math.min(widthScale, heightScale))
      // If the size is different from the current one, update it!
      if (!context.scale || context.scale !== newScale) {
        context.scale = newScale
        context.game.scale.setUserScale(newScale, newScale)
      }
    }, this)

    // Stop Phaser/Canvas from rounding/smoothing pixels
    // https://belenalbeza.com/retro-crisp-pixel-art-in-phaser/
    this.game.renderer.renderSession.roundPixels = true
    this.stage.smoothed = false
    Phaser.Canvas.setImageRenderingCrisp(this.game.canvas)

    // If the game canvas loses focus, keep the game running
    // this.stage.disableVisibilityChange = false

    // Set the background color
    this.stage.backgroundColor = '#444444'

    // this.mockup = window.game.load.image('mockup', './assets/mockup.png')

    this.load.pack('boot', null, assets)
  }

  create () {
    // let mockup = window.game.add.sprite(window.game.world.centerX, window.game.world.centerY, 'mockup')
    // mockup.anchor.setTo(0.5, 0.5)

    this.state.start('Menu')
  }
}
