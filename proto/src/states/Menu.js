// Copyright (c) 2016-2021, VIMinO
//
// This source code file is under the GPLv3 license, which can be found at:
// https://www.gnu.org/licenses/gpl-3.0.en.html
// Or the attached LICENSE file

import assets from '../assets'

import PIXI from 'expose-loader?PIXI!phaser-ce/build/custom/pixi.js'
import p2 from 'expose-loader?p2!phaser-ce/build/custom/p2.js'
import Phaser from 'expose-loader?Phaser!phaser-ce/build/custom/phaser-split.js'

export default class Boot extends Phaser.State {
  create () {
    this.stars = []
    for (let i = 0; i < 60; ++i) {
      let size = this.game.rnd.weightedPick([3, 3, 3, 3, 2, 2, 1]) // this.game.rnd.integerInRange(2, 3)

      let star = this.game.add.sprite(0, 0, 'title')
      star.frameName = 'star' + size.toString()
      star.anchor.setTo(0.5, 0.5)

      star.alpha = 0.5
      star.speed = this.game.rnd.realInRange(0.2, 1.2)
      this.shootStar(star)
      this.stars.push(star)
    }

    let title = this.game.add.sprite(0, 4, 'title')
    title.frameName = 'title'

    let play = this.game.add.sprite(window.game.world.centerX, 33, 'title') // 38
    play.frameName = 'play'
    play.anchor.setTo(0.5, 0)
    play.inputEnabled = true
    play.events.onInputDown.add(this.play, this)

    // let opts = this.game.add.sprite(window.game.world.centerX, 50, 'title')
    // opts.frameName = 'options'
    // opts.anchor.setTo(0.5, 0)
    // opts.inputEnabled = true
    // opts.events.onInputDown.add(this.options, this)

    let expand = this.game.add.sprite(window.game.world.centerX, 48, 'title')
    expand.frameName = 'expand'
    expand.anchor.setTo(0.5, 0)
    expand.inputEnabled = true
    expand.events.onInputDown.add(this.expand, this)
  }

  update () {
    for (let i in this.stars) {
      // let x = this.game.rnd.integerInRange(0, 5) - 10
      // if (x < 0) {
      //   x = 0
      // } else if (x > 64) {
      //   x = 64
      // }
      // this.stars[i].x += x
      this.stars[i].y += this.stars[i].speed

      if (this.stars[i].y > 80) {
        this.shootStar(this.stars[i])
      }
    }

    // let mouse = this.game.input.mousePointer
    //
    // if (mouse.isDown) {
    // }
  }

  shootStar (star) {
    star.x = this.game.rnd.integerInRange(0, 64)
    star.y = -this.game.rnd.integerInRange(0, 120)
    // star.rotation = this.game.math.degToRad(30 * this.game.rnd.integerInRange(0, 6))
  }

  play () {
    this.state.start('Game')
  }

  expand () {
    if (!this.scale.isFullScreen && this.scale.scaleMode === Phaser.ScaleManager.USER_SCALE) {
      this.scale.startFullScreen(true)
    } else if (this.scale.scaleMode === Phaser.ScaleManager.NO_SCALE) {
      this.scale.scaleMode = Phaser.ScaleManager.USER_SCALE
    } else if (this.scale.isFullScreen) {
      this.scale.stopFullScreen()
      this.scale.scaleMode = Phaser.ScaleManager.NO_SCALE
    }
  }

  // options () {
  //   this.state.start('Game')
  // }
}
