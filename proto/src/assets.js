// Copyright (c) 2016-2021, VIMinO
//
// This source code file is under the GPLv3 license, which can be found at:
// https://www.gnu.org/licenses/gpl-3.0.en.html
// Or the attached LICENSE file

export default {
  boot: [{
    key: 'title',
    type: 'atlasXML',
    atlasURL: 'title.xml',
    textureURL: 'title.png'
  }, {
    key: 'gauge',
    type: 'atlasXML',
    atlasURL: 'gauge.xml',
    textureURL: 'gauge.png'
  }, {
    key: 'ships',
    type: 'atlasXML',
    atlasURL: 'ships.xml',
    textureURL: 'ships.png'
  }, {
    key: 'pickups',
    type: 'atlasXML',
    atlasURL: 'pickups.xml',
    textureURL: 'pickups.png'
  }, {
    key: 'projectiles',
    type: 'atlasXML',
    atlasURL: 'projectiles.xml',
    textureURL: 'projectiles.png'
  }, {
    key: 'explosion',
    type: 'spritesheet',
    frameMax: 5,
    frameWidth: 10,
    frameHeight: 10
  }, {
    key: 'MicroFont',
    type: 'bitmapFont',
    atlasURL: 'font/MicroFont.xml',
    textureURL: 'font/MicroFont.png'
  }]
}
