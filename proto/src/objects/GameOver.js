// Copyright (c) 2016-2021, VIMinO
//
// This source code file is under the GPLv3 license, which can be found at:
// https://www.gnu.org/licenses/gpl-3.0.en.html
// Or the attached LICENSE file

import PIXI from 'expose-loader?PIXI!phaser-ce/build/custom/pixi.js'
import p2 from 'expose-loader?p2!phaser-ce/build/custom/p2.js'
import Phaser from 'expose-loader?Phaser!phaser-ce/build/custom/phaser-split.js'

export default class Counter extends Phaser.Group {
  constructor (game) {
    super(game)

    let gameOver = this.game.add.sprite(this.game.world.centerX, 12, 'title', 'gameover')
    gameOver.anchor.setTo(0.5, 0)
    this.add(gameOver)

    let replayButton = this.game.add.sprite(this.game.world.centerX, 25, 'title', 'replay')
    replayButton.anchor.setTo(0.5, 0)
    replayButton.inputEnabled = true
    replayButton.events.onInputDown.add(this.replay, this)
    this.add(replayButton)

    let quitButton = this.game.add.sprite(this.game.world.centerX, 40, 'title', 'quit')
    quitButton.anchor.setTo(0.5, 0)
    quitButton.inputEnabled = true
    quitButton.events.onInputDown.add(this.quit, this)
    this.add(quitButton)

    this.visible = false
    this.alpha = 0.6
    this.y = -64
  }

  show () {
    this.game.add.tween(this).to({y: 0}, 1000, Phaser.Easing.Bounce.Out, true)
    this.visible = true
  }

  replay () {
    this.game.state.start('Game')
  }

  quit () {
    this.game.state.start('Menu')
  }
}
