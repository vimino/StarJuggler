// Copyright (c) 2016-2021, VIMinO
//
// This source code file is under the GPLv3 license, which can be found at:
// https://www.gnu.org/licenses/gpl-3.0.en.html
// Or the attached LICENSE file

import PIXI from 'expose-loader?PIXI!phaser-ce/build/custom/pixi.js'
import p2 from 'expose-loader?p2!phaser-ce/build/custom/p2.js'
import Phaser from 'expose-loader?Phaser!phaser-ce/build/custom/phaser-split.js'

export default class Counter extends Phaser.Group {
  constructor (game, x, y, font, size) {
    super(game)

    this.value = 0

    // this.label = this.game.add.text(4, 3, this.value)
    // this.label.font = 'Monospace'
    // this.label.fontSize = 8
    // // this.label.fontWeight = 'bold'
    // this.label.fill = '#ffffff'

    this.label = game.add.bitmapText(x, y, font, this.value.toString(), size)
    this.label.alpha = 0.6
    this.add(this.label)
  }

  update () {
    this.label.text = this.value.toString()
  }

  addValue (value) {
    this.value += parseInt(value)
    if (this.value < 0) {
      this.value = 0
    }
    this.update()
  }

  setValue (value) {
    this.value = parseInt(value)
    this.update()
  }

  getValue () {
    return this.value
  }

  setAlpha (value) {
    this.label.alpha = value
  }
}
