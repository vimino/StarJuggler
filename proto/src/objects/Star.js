// Copyright (c) 2016-2021, VIMinO
//
// This source code file is under the GPLv3 license, which can be found at:
// https://www.gnu.org/licenses/gpl-3.0.en.html
// Or the attached LICENSE file

import PIXI from 'expose-loader?PIXI!phaser-ce/build/custom/pixi.js'
import p2 from 'expose-loader?p2!phaser-ce/build/custom/p2.js'
import Phaser from 'expose-loader?Phaser!phaser-ce/build/custom/phaser-split.js'

export default class Star extends Phaser.Sprite {
  constructor (game, parent) {
    super(game, 0, 0, 'projectiles', 's1')
    this.anchor.setTo(0.5, 0.5)
    this.parent = parent

    this.visible = false
    this.shotAngle = 0
    this.active = true
    this.r = 6
    this.energy = 0

    game.time.events.loop(Phaser.Timer.SECOND / 60, function () {
      this.rotation += this.game.math.degToRad(30)
      if (this.rotation > 2 * Math.PI) {
        this.rotation = 0
      }
    }, this)

    this.game.physics.arcade.enable(this)
    // Physics
    this.body.collideWorldBounds = false
    this.body.allowGravity = false
    this.body.immovable = true

    this.body.onWorldBounds = new Phaser.Signal()
    this.body.onWorldBounds.add(this.hitWorldBounds, this)
  }

  update () {
    if (this.active) {
      if (this.shotAngle < Math.PI * 2.5) {
        this.shotAngle += Math.PI / 60
        this.x = this.cx + Math.cos(this.shotAngle) * this.r
        this.y = this.cy + Math.sin(this.shotAngle) * this.r
      } else {
        this.active = false
        this.alpha = 0.5
        this.body.moves = true
        let escapeAngle = this.game.rnd.realInRange(0, Math.PI * 2)
        let escapeSpeed = this.game.rnd.realInRange(5, 20)
        this.body.velocity.x = Math.cos(escapeAngle) * escapeSpeed
        this.body.velocity.y = Math.sin(escapeAngle) * escapeSpeed
        this.body.collideWorldBounds = true
      }
    }
  }

  shoot (level, x, y) {
    this.r = 4 + level * 2
    this.cx = this.x = x
    this.cy = this.y = y - this.r

    this.body.collideWorldBounds = false
    this.alpha = 1
    this.active = true
    this.body.moves = false

    this.energy = level * 2
    this.frameName = 's' + level.toString()
    this.body.setCircle(Math.max(this.width, this.height) / 2)
    this.visible = true

    this.shotAngle = Math.PI * 0.5
  }

  isActive () {
    return this.active
  }

  getEnergy () {
    return this.energy
  }

  drain (value) {
    if (value === undefined) {
      value = this.energy
    }
    let energy = Math.floor(this.energy) // / 2)
    this.energy -= value

    if (this.energy <= 0) {
      this.active = true
      this.visible = false
      this.x = -30
      this.y = -30
    }

    return energy
  }

  hitWorldBounds () {
    this.drain()
  }
}
