// Copyright (c) 2016-2021, VIMinO
//
// This source code file is under the GPLv3 license, which can be found at:
// https://www.gnu.org/licenses/gpl-3.0.en.html
// Or the attached LICENSE file

import PIXI from 'expose-loader?PIXI!phaser-ce/build/custom/pixi.js'
import p2 from 'expose-loader?p2!phaser-ce/build/custom/p2.js'
import Phaser from 'expose-loader?Phaser!phaser-ce/build/custom/phaser-split.js'

export default class Gauge extends Phaser.Group {
  constructor (game, x, y) {
    super(game)

    this.gauge = game.make.sprite(x, y, 'gauge')
    this.gauge.frameName = 'gauge'

    this.danger = game.make.sprite(x + 1, y + 3, 'gauge')
    this.danger.frameName = 'danger'
    this.danger.visible = false

    this.energy = game.make.sprite(x + 1, y + 1, 'gauge')
    this.energy.frameName = 'energy'

    this.shield = game.make.sprite(x + 1, y + 3, 'gauge')
    this.shield.frameName = 'shield'

    this.add(this.gauge)
    this.add(this.danger)
    this.add(this.energy)
    this.add(this.shield)

    this.setShield(0)
    this.setEnergy(0)
  }

  updateShield () {
    if (this.shieldValue > 11) {
      this.shieldValue = 11
    } else if (this.shieldValue < 0) {
      this.shieldValue = 0
    }

    if (this.shieldValue < 5) {
      this.danger.visible = true
    } else {
      this.danger.visible = false
    }

    this.shieldCrop = new Phaser.Rectangle(1, 3, Math.floor(this.shieldValue), 3)
    this.shield.crop(this.shieldCrop)
  }

  addShield (value) {
    this.shieldValue += value
    this.updateShield()
    return this.shieldValue < 1
  }

  setShield (value) {
    this.shieldValue = value
    this.updateShield()
  }

  getShield (value) {
    return this.shieldValue
  }

  updateEnergy () {
    if (this.energyValue > 11) {
      this.energyValue = 11
    } else if (this.energyValue < 0) {
      this.energyValue = 0
    }

    this.energyCrop = new Phaser.Rectangle(12 - this.energyValue, 1, this.energyValue, 4)
    this.energy.crop(this.energyCrop)
    this.energy.x = this.gauge.x + 12 - this.energyValue

    // this.energy.width = this.energyValue
  }

  addEnergy (value) {
    this.energyValue += value
    this.updateEnergy()
  }

  setEnergy (value) {
    this.energyValue = value
    this.updateEnergy()
  }

  getEnergy (value) {
    return this.energyValue
  }
}
