// Copyright (c) 2016-2021, VIMinO
//
// This source code file is under the GPLv3 license, which can be found at:
// https://www.gnu.org/licenses/gpl-3.0.en.html
// Or the attached LICENSE file

import PIXI from 'expose-loader?PIXI!phaser-ce/build/custom/pixi.js'
import p2 from 'expose-loader?p2!phaser-ce/build/custom/p2.js'
import Phaser from 'expose-loader?Phaser!phaser-ce/build/custom/phaser-split.js'

export default class Pickup extends Phaser.Sprite {
  constructor (game, x, y, type) {
    super(game, x, y, 'pickups')

    this.delay = 4 // seconds
    this.startTime = 0
    this.reset()

    this.active = false

    // Physics
    this.game.physics.arcade.enable(this)
    this.body.collideWorldBounds = false
    this.body.allowGravity = false
    this.body.immovable = true
  }

  moveTo (x, y, speed) {
    let cx = this.position.x
    let cy = this.position.y
    let vx = 0
    let vy = 0
    if (!speed) speed = 1

    if (cx < x) {
      vx = speed
    } else if (cx > x) {
      vx = -speed
    }

    if (cy < y) {
      vy = speed
    } else if (cy > y) {
      vy = -speed
    }

    this.body.velocity.setTo(vx, vy)
  }

  stop () {
    this.body.stop()
  }

  getType () {
    return this.frame
  }

  reset (type) {
    this.startTime = this.game.time.totalElapsedSeconds()
    this.active = false
    this.alpha = 0

    this.position.setTo(this.game.rnd.integerInRange(10, 54), this.game.rnd.integerInRange(10, 54))

    if (type === undefined) {
      let randomType = this.game.rnd.integerInRange(0, 5)
      switch (randomType) {
        case 0:
          type = 'shield'
          break
        case 1:
          type = 'shield+'
          break
        case 2:
          type = 'shield++'
          break
        case 3:
          type = 'energy'
          break
        case 4:
          type = 'energy+'
          break
        case 5:
          type = 'energy++'
          break
      }
    }

    this.frameName = type
    this.anchor.setTo(0.5, 0.5)
    this.game.physics.arcade.enable(this)
    this.body.setSize(this.width, this.height, 0, 0)
  }

  update () {
    if (this.game.time.totalElapsedSeconds() > this.startTime + this.delay) {
      if (this.active) {
        this.phaseOut()
      } else {
        this.phaseIn()
      }

      this.startTime = this.game.time.totalElapsedSeconds()
    }
  }

  phaseIn () {
    let phase = this.game.add.tween(this).to({alpha: 1}, Phaser.Timer.SECOND, Phaser.Easing.Linear.None, true)

    phase.onComplete.add(function () {
      this.active = true
    }, this)
  }

  phaseOut () {
    let phase = this.game.add.tween(this).to({alpha: 0}, Phaser.Timer.SECOND, Phaser.Easing.Linear.None, true)

    phase.onComplete.add(function () {
      this.reset()
    }, this)
  }

  isActive () {
    return this.active
  }
}
