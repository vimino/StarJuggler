// Copyright (c) 2016-2021, VIMinO
//
// This source code file is under the GPLv3 license, which can be found at:
// https://www.gnu.org/licenses/gpl-3.0.en.html
// Or the attached LICENSE file

import PIXI from 'expose-loader?PIXI!phaser-ce/build/custom/pixi.js'
import p2 from 'expose-loader?p2!phaser-ce/build/custom/p2.js'
import Phaser from 'expose-loader?Phaser!phaser-ce/build/custom/phaser-split.js'

export default class Enemy extends Phaser.Sprite {
  constructor (game, x, y, type, level) {
    super(game, x, y, 'ships')
    // this.frame = type * 3 + level
    if (type < 6) {
      this.frameName = 'e' + type.toString() + level.toString()
    } else if (type === 6) {
      if (level === 1) {
        this.frameName = 'juggernaut'
      } else if (level === 2) {
        this.frameName = 'cyclops'
      } else if (level === 3) {
        this.frameName = 'titan'
      }
    }
    // this.boundsSprite = this
    if (type === 0) {
      this.rotation = this.game.math.degToRad(90 * this.game.rnd.integerInRange(0, 3))
    } else {
      this.rotation = 0
    }
    this.anchor.setTo(0.5, 0.5)

    this.type = type
    this.level = level
    this.health = 1
    if (type > 0 && type < 6) {
      this.health = level
    } else if (type > 5) {
      this.health = level * 4
    }

    this.weapons = []

    // Physics
    this.game.physics.arcade.enable(this)
    this.body.collideWorldBounds = false
    this.body.allowGravity = false
    this.body.immovable = true
  }

  moveTo (x, y, speed) {
    let cx = this.position.x
    let cy = this.position.y
    let vx = 0
    let vy = 0
    if (!speed) speed = 1

    if (cx < x) {
      vx = speed
    } else if (cx > x) {
      vx = -speed
    }

    if (cy < y) {
      vy = speed
    } else if (cy > y) {
      vy = -speed
    }

    this.body.velocity.setTo(vx, vy)
  }

  stop () {
    this.body.stop()
  }
  getType () {
    return this.type
  }

  getLevel () {
    return this.level
  }

  getPosition () {
    return this.position
  }

  getHealth () {
    return this.health
  }

  damage (value) {
    if (value === undefined) {
      value = this.health
    }

    this.health -= parseInt(value)

    if (this.health <= 0) {
      // for (let i in this.weapons) {
      //   this.weapons[i].autofire = false
      //   // this.weapons[i].destroyAll()
      //   // this.weapons[i].destroy()
      //   // delete this.weapons[i]
      // }

      // this.x = -100
      // this.destroy()

      this.kill()

      let explosion = this.game.add.sprite(this.x, this.y, 'explosion')
      explosion.anchor.setTo(0.5, 0.5)
      if (this.type === 6) {
        explosion.scale.setTo(2, 2)
      }
      explosion.animations.add('boom', [0, 1, 2, 3, 4], 5, false).onComplete.add(function () {
        explosion.kill()
      }, this)
      explosion.animations.play('boom')

      // this.visible = false

      return true
    }

    return false
  }

  changeTo (type, level) {
    this.type = type
    this.level = level

    // this.frame = type * 3 + level
    this.frameName = 'e' + type.toString() + (level + 1).toString()
    if (type === 0) {
      this.rotation = this.game.math.degToRad(90 * this.game.rnd.integerInRange(0, 3))
    } else {
      this.rotation = 0
    }

    for (let i in this.weapons) {
      // this.weapons[i].killAll()
      this.weapons[i].destroy()
      // delete this.weapons[i]
    }
  }

  addDirectionalWeapon (direction) {
    let type = this.type
    let frame = ''
    if (type === 0) { // Asteroid (no weapon)
      return
    } else if (type === 1) { // Blaster
      if (this.level === 1) {
        frame = 'b2'
      } else if (this.level === 2) {
        frame = 'b4'
      } else if (this.level === 3) {
        frame = 'b5'
      }
    } else if (type === 2) { // Bomber
      frame = 'r' + this.level.toString()
    } else if (type === 3) { // Slasher
      frame = 'w' + this.level.toString()
    } else if (type === 4) { // Razor
      frame = 'p' + this.level.toString()
    } else if (type === 5) { // Sweeper
      frame = 'b' + this.level.toString()
    } else if (type === 6) { // Overlords
      frame = 'l' + this.level.toString()
    }

    let weapon = this.game.add.weapon(4 - this.level, 'projectiles', frame)
    weapon.bulletAngleOffset = Phaser.ANGLE_DOWN
    weapon.bulletSpeed = 50 / this.level
    weapon.fireRate = 1000 * this.level
    weapon.fireAngle = Phaser.ANGLE_DOWN
    weapon.bulletKillType = Phaser.Weapon.KILL_WORLD_BOUNDS
    weapon.trackSprite(this)

    weapon.fireAngle = direction

    this.weapons.push(weapon)
  }

  addWeapon (target, ot, ox, oy) {
    let type = this.type
    if (ot !== undefined) {
      type = ot
    }
    let frame = ''
    if (type === 0) { // Asteroid (no weapon)
      return
    } else if (type === 1) { // Blaster
      if (this.level === 1) {
        frame = 'b2'
      } else if (this.level === 2) {
        frame = 'b4'
      } else if (this.level === 3) {
        frame = 'b5'
      }
    } else if (type === 2) { // Bomber
      frame = 'r' + this.level.toString()
    } else if (type === 3) { // Slasher
      frame = 'w' + this.level.toString()
    } else if (type === 4) { // Razor
      frame = 'p' + this.level.toString()
    } else if (type === 5) { // Sweeper
      frame = 'b' + this.level.toString()
    } else if (type === 6) { // Overlords
      frame = 'l' + this.level.toString()
    }

    // let weapon = this.game.add.weapon(1, 'weapons')
    let weapon = this.game.add.weapon(4 - this.level, 'projectiles', frame)
    weapon.bulletAngleOffset = 270
    weapon.bulletSpeed = 50 / this.level
    // weapon.setBulletFrames(frame, frame)
    weapon.fireRate = 1000 * this.level
    weapon.fireAngle = Phaser.ANGLE_DOWN
    weapon.bulletKillType = Phaser.Weapon.KILL_WORLD_BOUNDS
    weapon.trackSprite(this, ox === undefined ? 0 : ox, oy === undefined ? 0 : oy)
    weapon.target = target

    this.weapons.push(weapon)
  }

  fire (id) {
    if (id === undefined) {
      for (let i in this.weapons) {
        if (this.weapons[i].target !== undefined) {
          this.weapons[i].fireAtSprite(this.weapons[i].target)
        } else {
          this.weapons[i].fire()
        }
      }
    } else if (id in this.weapons) {
      if (this.weapons[id].target !== undefined) {
        this.weapons[id].fireAtSprite(this.weapons[id].target)
      } else {
        this.weapons[id].fire()
      }
    }
  }
}
