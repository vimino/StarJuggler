// Copyright (c) 2016-2021, VIMinO
//
// This source code file is under the GPLv3 license, which can be found at:
// https://www.gnu.org/licenses/gpl-3.0.en.html
// Or the attached LICENSE file

export default {
  loader: [{
    key: 'loader',
    src: 'loader.png',
    type: 'spritesheet',
    'frameWidth': 54,
    'frameHeight': 33
  }],

  main: [{
    key: 'ui',
    type: 'atlasXML',
    atlasURL: 'ui.xml'
  }, {
    key: 'panel',
    type: 'atlasXML',
    atlasURL: 'panel.xml'
  // }, {
  //   key: 'bg_menu',
  //   type: 'image'
  }, {
    key: 'vimino',
    type: 'spritesheet',
    frameMax: 7,
    frameWidth: 60,
    frameHeight: 30
  }, {
    key: 'ships',
    type: 'atlasXML',
    atlasURL: 'ships.xml',
    textureURL: 'ships.png'
  }, {
    key: 'pickups',
    type: 'atlasXML',
    atlasURL: 'pickups.xml',
    textureURL: 'pickups.png'
  }, {
    key: 'projectiles',
    type: 'atlasXML',
    atlasURL: 'projectiles.xml',
    textureURL: 'projectiles.png'
  }, {
    key: 'star',
    type: 'atlasXML',
    atlasURL: 'star.xml',
    textureURL: 'star.png'
  // }, {
  //   key: 'star0',
  //   type: 'atlasXML',
  //   atlasURL: 'star0.xml',
  //   textureURL: 'star0.png'
  // }, {
  //   key: 'star1',
  //   type: 'atlasXML',
  //   atlasURL: 'star1.xml',
  //   textureURL: 'star1.png'
  // }, {
  //   key: 'star2',
  //   type: 'atlasXML',
  //   atlasURL: 'star2.xml',
  //   textureURL: 'star2.png'
  // }, {
  //   key: 'star3',
  //   type: 'atlasXML',
  //   atlasURL: 'star3.xml',
  //   textureURL: 'star3.png'
  // }, {
  //   key: 'star4',
  //   type: 'atlasXML',
  //   atlasURL: 'star4.xml',
  //   textureURL: 'star4.png'
  // }, {
  //   key: 'star5',
  //   type: 'atlasXML',
  //   atlasURL: 'star5.xml',
  //   textureURL: 'star5.png'
  }, {
    key: 'explosion',
    type: 'spritesheet',
    frameMax: 5,
    frameWidth: 10,
    frameHeight: 10
  }, {
    key: 'background',
    type: 'atlasXML',
    atlasURL: 'background.xml',
    textureURL: 'background.png'
  }, {
    key: 'MicroFont',
    type: 'bitmapFont',
    atlasURL: 'font/MicroFont.xml',
    textureURL: 'font/MicroFont.png'
  }, {
    key: 'music_menu',
    type: 'audio',
    urls: ['audio/menu.ogg']
  }, {
    key: 'music_shop',
    type: 'audio',
    urls: ['audio/shop.ogg']
  }, {
    key: 'music_gameover',
    type: 'audio',
    urls: ['audio/gameover.ogg']
  }, {
    key: 'music_space',
    type: 'audio',
    urls: ['audio/space.ogg']
  }, {
    key: 'music_world',
    type: 'audio',
    urls: ['audio/world.ogg']
  }, {
    key: 'throw',
    type: 'audio',
    urls: ['audio/throw.wav']
  }, {
    key: 'catch',
    type: 'audio',
    urls: ['audio/catch.wav']
  }, {
    key: 'blast',
    type: 'audio',
    urls: ['audio/blast.wav']
  }, {
    key: 'pickup1',
    type: 'audio',
    urls: ['audio/pickup1.wav']
  }, {
    key: 'pickup2',
    type: 'audio',
    urls: ['audio/pickup2.wav']
  }, {
    key: 'explosion1',
    type: 'audio',
    urls: ['audio/explosion1.wav']
  }, {
    key: 'explosion2',
    type: 'audio',
    urls: ['audio/explosion2.wav']
  }, {
    key: 'explosion3',
    type: 'audio',
    urls: ['audio/explosion3.wav']
  }, {
    key: 'no',
    type: 'audio',
    urls: ['audio/no.wav']
  }, {
    key: 'oh',
    type: 'audio',
    urls: ['audio/oh.wav']
  }, {
    key: 'slide',
    type: 'audio',
    urls: ['audio/slide.wav']
  }, {
    key: 'unslide',
    type: 'audio',
    urls: ['audio/unslide.wav']
  }]
}
