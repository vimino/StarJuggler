// Copyright (c) 2016-2021, VIMinO
//
// This source code file is under the GPLv3 license, which can be found at:
// https://www.gnu.org/licenses/gpl-3.0.en.html
// Or the attached LICENSE file

import PIXI from 'expose-loader?PIXI!phaser-ce/build/custom/pixi.js' // eslint-disable-line
import p2 from 'expose-loader?p2!phaser-ce/build/custom/p2.js' // eslint-disable-line
import Phaser from 'expose-loader?Phaser!phaser-ce/build/custom/phaser-split.js' // eslint-disable-line

import Player from '../objects/Player'
import Star from '../objects/Star'

let player, starma

let shotGuide = new Phaser.Line(32, 32, 32, 32)
let directionGuide = [
  new Phaser.Line(32, 32, 32, 32),
  new Phaser.Line(32, 32, 32, 32),
  new Phaser.Line(32, 32, 32, 32),
  new Phaser.Line(32, 32, 32, 32),
  new Phaser.Line(32, 32, 32, 32),
  new Phaser.Line(32, 32, 32, 32),
  new Phaser.Line(32, 32, 32, 32),
  new Phaser.Line(32, 32, 32, 32)
]

let getRandomColor = function () {
  let chars = '0123456789abcdef'
  let color = ''
  for (let i = 0; i < 6; ++i) {
    color += chars[Math.floor(Math.random() * chars.length)]
  }
  return color
}

export default class Test extends Phaser.State {
  create () {
    console.log('This is a Javascript ES6 test')

    player = new Player(this.game, this.game.world.centerX, this.game.world.centerY)
    this.add.existing(player)
    player.showGlow(true)
    console.log('Player', player.x, player.y)

    starma = this.game.add.group()
    // this.star = []
    for (let i = 0; i < 5; ++i) {
      let star = new Star(this.game)
      starma.add(star)
    }

    // Throw the next available Star from the Parents' position
    starma.throw = (level, x, y, angle = (270.0 / 180.0 * Math.PI)) => { // Math.PI / 2.0) {
      let newStar = starma.getFirstDead(false, x, y)
      if (!newStar) {
        console.log('no stars to give')
        return false
      }

      newStar.setColor(getRandomColor())
      newStar.throw(level, x, y, angle)
      return true
    }

    let d2r = function (r) {
      return (r / 180.0 * Math.PI)
    }

    // 8 Available Directions
    //
    // 0 -3.141592653589793
    // -2.748893571891069
    // 45 -2.356194490192345
    // -1.9634954084936207
    // 90 -1.5707963267948966
    // -1.1780972450961724
    // 135 -0.7853981633974483
    // -0.39269908169872414
    // 180 0
    // 0.39269908169872414
    // 225 0.7853981633974483
    // 1.1780972450961724
    // 270 1.5707963267948966
    // 1.9634954084936207
    // 315 2.356194490192345
    // 2.748893571891069
    // 360 3.141592653589793

    for (let i = 0; i < 8; ++i) {
      let a = d2r(i * 45.0 - 180.0)
      directionGuide[i].fromSprite({ x: player.x, y: player.y },
        { x: player.x + 8 * Math.cos(a),
          y: player.y + 8 * Math.sin(a) }, false)
    }

    let level = 1
    this.game.time.events.loop(100, () => {
      let angle = 270.0 / 180.0 * Math.PI
      if (this.game.input.activePointer.withinGame) {
        let pointer = this.game.input.mousePointer
        console.log(pointer.x, pointer.y, 'vs', player.x, player.y)
        let deltax = pointer.x - player.x
        let deltay = pointer.y - player.y
        angle = Math.atan2(deltay, deltax)
      }

      shotGuide.fromSprite({ x: player.x, y: player.y },
        { x: player.x + 16 * Math.cos(angle),
          y: player.y + 16 * Math.sin(angle) }, false)

      if (starma.throw(level, player.x, player.y, angle)) {
        ++level
        if (level > 5) { level = 1 }
      }
    }, this)
  }

  render () {
    if (this.game.input.activePointer.isDown) {
      this.game.debug.geom(shotGuide)

      for (let i in directionGuide) {
        this.game.debug.geom(directionGuide[i])
      }
    }
  }
}
