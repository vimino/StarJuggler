// Copyright (c) 2016-2021, VIMinO
//
// This source code file is under the GPLv3 license, which can be found at:
// https://www.gnu.org/licenses/gpl-3.0.en.html
// Or the attached LICENSE file

import PIXI from 'expose-loader?PIXI!phaser-ce/build/custom/pixi.js' // eslint-disable-line
import p2 from 'expose-loader?p2!phaser-ce/build/custom/p2.js' // eslint-disable-line
import Phaser from 'expose-loader?Phaser!phaser-ce/build/custom/phaser-split.js' // eslint-disable-line

import OptionSlider from '../objects/OptionSlider'
import AboutSlider from '../objects/AboutSlider'
import Label from '../objects/Label'

export default class Menu extends Phaser.State {
  create () {
    // this.game.stage.10groundColor = '#ff4444'

    // background

    window.background = this.game.rnd.integerInRange(1, 2)
    // console.log('background', 'menu' + window.background.toString())
    this.background = this.game.add.tileSprite(-1, -1, 66, 132, 'background', 'menu' + window.background.toString())
    this.background.autoScroll(0, 10)

    // Stars

    this.stars = []
    for (let i = 0; i < 60; ++i) {
      let size = this.game.rnd.weightedPick([1, 2, 3, 4, 5, 6, 7])

      let star = this.game.add.sprite(0, 0, 'ui')
      star.frameName = 'star' + size.toString()
      star.anchor.setTo(0.5, 0.5)

      star.alpha = 0.5
      star.speed = this.game.rnd.realInRange(0.2, 0.6)
      this.shootStar(star)
      this.stars.push(star)
    }

    // Version label

    this.version = new Label(this.game, 2, 2, 'MicroFont', 5)
    this.version.set('0.6.0')
    this.version.setAlpha(0.1)

    this.version.label.inputEnabled = true
    this.version.label.events.onInputDown.add(this.clickVersion, this)
    this.version.label.events.onInputUp.add(this.clickVersion, this)
    this.version.label.events.onInputOut.add(() => { this.clickVersion(null) }, this)

    // Title

    this.title = this.game.add.sprite(5, 4, 'ui') // window.game.world.centerX
    this.title.frameName = 'title1'
    // this.title.anchor.setTo(0, 0)
    this.title.wait = 0

    // Play Button

    this.play = this.game.add.group()
    this.play.position.setTo(5, 38)

    this.playButton = this.game.add.sprite(0, 0, 'ui')
    this.playButton.frameName = 'play'
    this.play.add(this.playButton)

    this.playPattern = this.game.add.sprite(1, 1, 'ui')
    this.playPattern.frameName = 'playPat1'
    this.playPattern.alpha = 0.4
    this.play.add(this.playPattern)
    this.play.pattern = 1

    this.playButton.inputEnabled = true
    this.playButton.events.onInputDown.add(this.clickPlay, this)
    this.playButton.events.onInputUp.add(this.clickPlay, this)
    this.playButton.events.onInputOut.add(() => { this.clickPlay(null) }, this)

    // Options Button

    this.options = this.game.add.group()
    this.options.position.setTo(35, 38)

    this.optionsButton = this.game.add.sprite(0, 0, 'ui')
    this.optionsButton.frameName = 'options'
    this.options.add(this.optionsButton)

    this.optionsPattern = this.game.add.sprite(1, 1, 'ui')
    this.optionsPattern.frameName = 'optionsPat1'
    this.optionsPattern.alpha = 0.3
    this.options.add(this.optionsPattern)
    this.options.pattern = 1

    this.optionsButton.inputEnabled = true
    this.optionsButton.events.onInputDown.add(this.clickOptions, this)
    this.optionsButton.events.onInputUp.add(this.clickOptions, this)
    this.optionsButton.events.onInputOut.add(
      () => { this.clickOptions(null) },
      this)

    // if (this.msMenu) {
    //   this.msMenu.stop()
    // } else {
    this.msMenu = this.game.add.audio('music_menu')
    this.msMenu.volume = 0.5
    // }
    this.msMenu.loopFull()
    this.fxSlide = this.game.add.audio('slide')
    this.fxUnslide = this.game.add.audio('unslide')

    // Sliders

    let slideIn = () => {
      this.fxSlide.play()
    }
    let slideOut = () => {
      this.fxUnslide.play()
    }

    this.optionSlider = new OptionSlider(this)
    this.optionSlider.onShow(slideIn)
    this.optionSlider.onHide(slideOut)

    this.aboutSlider = new AboutSlider(this)
    this.aboutSlider.onShow(slideIn)
    this.aboutSlider.onHide(slideOut)

    // this.game.sound.volume = 1.0 // 0.5

    // Timers

    this.game.time.events.loop(Phaser.Timer.SECOND / 30, this.animateStars, this)
    this.game.time.events.loop(Phaser.Timer.SECOND / 6, this.animate, this)
    this.game.time.events.loop(Phaser.Timer.SECOND, this.animateTitle, this)
  }

  animate () {
    if (this.optionSlider.active || this.aboutSlider.active) {
      this.background.stopScroll()
      return
    }

    this.background.autoScroll(0, 10)

    this.play.pattern++
    if (this.play.pattern > 4) this.play.pattern = 1
    this.playPattern.frameName = ('playPat' + this.play.pattern.toString())

    this.options.pattern++
    if (this.options.pattern > 4) this.options.pattern = 1
    this.optionsPattern.frameName = ('optionsPat' + this.options.pattern.toString())
  }

  animateStars () {
    if (this.optionSlider.active || this.aboutSlider.active) {
      return
    }

    for (let i in this.stars) {
      this.stars[i].y += this.stars[i].speed

      if (this.stars[i].y > 80) {
        this.shootStar(this.stars[i])
      }
    }
  }

  animateTitle () {
    if (this.optionSlider.active || this.aboutSlider.active) {
      return
    }

    if (this.title.frameName === 'title1') {
      this.title.frameName = 'title2'
    } else {
      this.title.frameName = 'title1'
    }
  }

  shootStar (star) {
    star.x = this.game.rnd.integerInRange(0, 64)
    star.y = -this.game.rnd.integerInRange(0, 120)

    // star.rotation = this.game.math.degToRad(30 * this.game.rnd.integerInRange(0, 6))
  }

  clickVersion (element, event) {
    if (this.optionSlider.active || this.aboutSlider.active) {
      return
    }

    if (event != null && event.isDown) {
      this.version.setAlpha(0.6)
      this.version.cancel = false
    } else {
      if (event != null && !this.version.cancel) {
        this.aboutSlider.show()
      } else {
        this.version.cancel = true
      }

      this.version.setAlpha(0.1)
    }
  }

  clickPlay (element, event) {
    if (this.optionSlider.active || this.aboutSlider.active) {
      return
    }

    if (event != null && event.isDown) {
      this.playButton.frameName = 'playP'
      this.playPattern.alpha = 0.8
      this.playButton.cancel = false
    } else {
      if (event != null && !this.playButton.cancel) {
        window.background = undefined
        this.msMenu.stop()
        this.state.start('Game')
      } else {
        this.playButton.cancel = true
      }

      this.playButton.frameName = 'play'
      this.playPattern.alpha = 0.4
    }
  }

  clickOptions (element, event) {
    if (this.optionSlider.active || this.aboutSlider.active) {
      return
    }

    if (event != null && event.isDown) {
      this.optionsButton.frameName = 'optionsP'
      this.optionsPattern.alpha = 0.8
      this.optionsButton.cancel = false
    } else {
      if (event != null && !this.optionsButton.cancel) {
        this.optionSlider.show()
      } else {
        this.optionsButton.cancel = true
      }

      this.optionsButton.frameName = 'options'
      this.optionsPattern.alpha = 0.4
    }
  }
}
