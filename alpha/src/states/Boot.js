// Copyright (c) 2016-2021, VIMinO
//
// This source code file is under the GPLv3 license, which can be found at:
// https://www.gnu.org/licenses/gpl-3.0.en.html
// Or the attached LICENSE file

import assets from '../assets'

import PIXI from 'expose-loader?PIXI!phaser-ce/build/custom/pixi.js' // eslint-disable-line
import p2 from 'expose-loader?p2!phaser-ce/build/custom/p2.js' // eslint-disable-line
import Phaser from 'expose-loader?Phaser!phaser-ce/build/custom/phaser-split.js' // eslint-disable-line

export default class Boot extends Phaser.State {
  preload () {
    this.load.path = 'assets/'

    // Align the Game screen
    this.scale.pageAlignHorizontally = true
    this.scale.pageAlignVertically = true

    // Apply a custom Scale (64*?)
    this.scale.scaleMode = Phaser.ScaleManager.USER_SCALE // .NO_SCALE
    this.scale.fullScreenScaleMode = Phaser.ScaleManager.USER_SCALE
    this.scale.setResizeCallback(function (context) {
      // Calculate how many times the Game size fits in the Window
      // PS: context.width and context.height change based on the scale
      let widthScale = window.innerWidth / context.game.width
      let heightScale = window.innerHeight / context.game.height

      if (widthScale < 1) { widthScale = 1 }
      if (heightScale < 1) { heightScale = 1 }

      let newScale = Math.floor(Math.min(widthScale, heightScale))
      // If the size is different from the current one, update it!
      if (!context.scale || context.scale !== newScale) {
        context.scale = newScale
        context.game.scale.setUserScale(newScale, newScale)
      }
    }, this)

    // Stop Phaser/Canvas from rounding/smoothing pixels
    // https://belenalbeza.com/retro-crisp-pixel-art-in-phaser/
    this.game.renderer.renderSession.roundPixels = true
    this.stage.smoothed = false
    Phaser.Canvas.setImageRenderingCrisp(this.game.canvas)

    // If the game canvas loses focus, keep the game running
    // this.stage.disableVisibilityChange = true

    // Set the background color
    this.stage.backgroundColor = '#111111'

    this.load.pack('loader', null, assets)

    this.game.sound.volume = 1.0 // 0.5

    // Set default options: Difficulty = Normal and Not Fullscreen
    // if (!('opts' in this.game)) {
    window.option = {
      'diff': 2,
      'full': false
    }
    // console.log('reset', this.game)
    // }
  }

  create () {
    this.state.start('Loader')
  }
}
