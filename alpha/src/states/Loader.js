// Copyright (c) 2016-2021, VIMinO
//
// This source code file is under the GPLv3 license, which can be found at:
// https://www.gnu.org/licenses/gpl-3.0.en.html
// Or the attached LICENSE file

import assets from '../assets'

import PIXI from 'expose-loader?PIXI!phaser-ce/build/custom/pixi.js' // eslint-disable-line
import p2 from 'expose-loader?p2!phaser-ce/build/custom/p2.js' // eslint-disable-line
import Phaser from 'expose-loader?Phaser!phaser-ce/build/custom/phaser-split.js' // eslint-disable-line

export default class Loader extends Phaser.State {
  preload () {
    this.load.path = 'assets/'
  }

  create () {
    // this.game.stage.backgroundColor = '#44aaff'
    this.back = this.game.add.sprite(5, 15, 'loader', 0)

    this.game.load.onLoadStart.add(this.startLoading, this)
    this.game.load.onFileComplete.add(this.progressLoading, this)
    this.game.load.onLoadComplete.add(this.startGame, this)

    this.load.pack('main', null, assets)
    this.game.load.start()
  }

  startLoading () {
    this.front = this.game.add.sprite(5, 15, 'loader', 1)
    this.front.crop(new Phaser.Rectangle(0, 0, 0, 33))
  }

  progressLoading (progress, cacheKey, success, totalLoaded, totalFiles) {
    this.front.crop(new Phaser.Rectangle(0, 0, ((progress / 100.0) * 54), 33))
  }

  startGame () {
    // hide the grayscale Title (back)
    this.back.visible = false

    // Show the Menu background but keep the colored Title (front) on top
    window.background = this.game.rnd.integerInRange(1, 2)
    let background = this.game.add.sprite(-1, -1, 'background', 'menu' + window.background.toString()) //, 66, 66, 'bg_menu')
    background.alpha = 0.0
    this.world.bringToTop(this.front)
    this.game.add.tween(background).to({alpha: 1.0}, 500, Phaser.Easing.Cubic.In, true)

    // Slide the colored Title (front) up then switch to the Menu
    this.game.add.tween(this.front).to({x: 5, y: 4}, 500, Phaser.Easing.Cubic.In, true).onComplete.add(() => {
      this.state.start('Menu')
    }, this)
  }
}
