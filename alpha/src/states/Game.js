// Copyright (c) 2016-2021, VIMinO
//
// This source code file is under the GPLv3 license, which can be found at:
// https://www.gnu.org/licenses/gpl-3.0.en.html
// Or the attached LICENSE file

import PIXI from 'expose-loader?PIXI!phaser-ce/build/custom/pixi.js' // eslint-disable-line
import p2 from 'expose-loader?p2!phaser-ce/build/custom/p2.js' // eslint-disable-line
import Phaser from 'expose-loader?Phaser!phaser-ce/build/custom/phaser-split.js' // eslint-disable-line

import Player from '../objects/Player'
import Star from '../objects/Star'
import Enemy from '../objects/Enemy'
import Pickup from '../objects/Pickup'
import Gauge from '../objects/Gauge'
import Counter from '../objects/Counter'

import GameOver from '../objects/GameOver'
import ShopSlider from '../objects/ShopSlider'

// Constants
Math.PIx180 = 180.0 * Math.PI

// Gauge, Player, Star Manager, Enemy Manager
let keyboard, gauge, player, starma, enema, upgrades
// The (line) Guides showing the shot angle and available directions
let shotGuide = new Phaser.Line(32, 32, 32, 32)
// let directionGuide = [
//   new Phaser.Line(32, 32, 32, 32),
//   new Phaser.Line(32, 32, 32, 32),
//   new Phaser.Line(32, 32, 32, 32),
//   new Phaser.Line(32, 32, 32, 32),
//   new Phaser.Line(32, 32, 32, 32),
//   new Phaser.Line(32, 32, 32, 32),
//   new Phaser.Line(32, 32, 32, 32),
//   new Phaser.Line(32, 32, 32, 32)
// ]

export default class Game extends Phaser.State {
  constructor () {
    super()

    // Variables

    this.ui = undefined
    this.pickup = undefined
    this.score = undefined
    this.gameOver = undefined
    this.music = undefined

    // Constants

    // this.pickupLimit = 1

    this.killCount = 0
  }

  create () {
    upgrades = {
      'power': 0,
      'structure': 0,
      'skill': 0,
      'score': 0
    }

    // console.log('Game.create', window.option)

    this.killsRequired = 10 * (4 - window.option.diff)

    this.shiGenDelay = 12 * window.option.diff
    this.eneGenDelay = 4 * window.option.diff
    this.waveCount = window.option.diff - 1 // 200
    this.waveLimit = 10 * window.option.diff // number of Waves before a chance to Shop
    this.waveLevelLimit = 100 * window.option.diff

    this.starCooldown = 0.5 * window.option.diff
    this.starLimit = 1 + (3 - window.option.diff)

    // Set Objects Up

    // this.waveCount = 0

    // If it's the first time, we start in space
    let bgName = 'space'
    window.space = true
    // If it's the first time, pick a random background
    window.background = this.game.rnd.integerInRange(1, 5)
    // console.log('background', bgName + window.background.toString())

    this.background = this.game.add.tileSprite(-1, -1, 66, 66, 'background', bgName + window.background.toString())
    this.background.autoScroll(0, 4 * window.option.diff)

    this.ui = this.game.add.group()
    this.add.existing(this.ui)

    this.pickup = new Pickup(this.game, this.game.rnd.integerInRange(5, 59), this.game.rnd.integerInRange(5, 59), 'shield')
    this.add.existing(this.pickup)

    player = new Player(this.game, 34, 43, 15)
    this.add.existing(player)

    starma = this.game.add.group()
    starma.energy = 0.0
    for (let i = 0; i < this.starLimit; ++i) {
      let star = new Star(this.game, 0xddddff)
      starma.add(star)
    }

    this.calculateAngle = function () {
      let pointer = this.game.input.activePointer
      let a = { x: 0, y: 0 }
      let b = { x: 0, y: 0 }

      if (pointer.withinGame) {
        a = pointer.position
        b = player.position
      } else {
        a = player.position
        b = keyboard.previous
      }

      let deltax = a.x - b.x
      let deltay = a.y - b.y

      // let deltax = b.x - a.x
      // let deltay = b.y - a.y

      let angle = Math.atan2(deltay, deltax)

      // Limit the Angle to one of 8 directions
      if (angle <= -2.748893571891069) { // left (-PI)
        angle = -3.141592653589793
      } else if (angle > -2.748893571891069 && angle <= -1.9634954084936207) { // up+left
        angle = -2.356194490192345
      } else if (angle > -1.9634954084936207 && angle <= -1.1780972450961724) { // up
        angle = -1.5707963267948966
      } else if (angle > -1.1780972450961724 && angle <= -0.39269908169872414) { // up+right
        angle = -0.7853981633974483
      } else if (angle > -0.39269908169872414 && angle <= 0.39269908169872414) { // right
        angle = 0.0
      } else if ((angle > 0.39269908169872414 && angle <= 1.1780972450961724)) { // down+right
        angle = 0.7853981633974483
      } else if (angle > 1.1780972450961724 && angle <= 1.9634954084936207) { // down
        angle = 1.5707963267948966
      } else if (angle > 1.9634954084936207 && angle <= 2.748893571891069) { // down+left
        angle = 2.356194490192345
      } else if (angle > 2.748893571891069) { // left (PI)
        angle = 3.141592653589793
      }

      // Update the Line
      if (window.option.diff === 2) {
        shotGuide.fromSprite({ x: player.x, y: player.y },
          { x: player.x + 10 * Math.cos(angle),
            y: player.y + 10 * Math.sin(angle) }, false)
      } else if (window.option.diff === 1) {
        shotGuide.fromSprite({ x: player.x, y: player.y },
          { x: player.x + (1 + starma.energy) * 6 * Math.cos(angle),
            y: player.y + (1 + starma.energy) * 6 * Math.sin(angle) }, false)
      }

      // let d2r = function (r) {
      //   return (r / 180.0 * Math.PI)
      // }
      // for (let i = 0; i < 8; ++i) {
      //   let a = d2r(i * 45.0 - 180.0)
      //   directionGuide[i].fromSprite({ x: player.x, y: player.y },
      //     { x: player.x + 8 * Math.cos(a),
      //       y: player.y + 8 * Math.sin(a) }, false)
      //   // console.log(i * 45, a)
      // }

      // console.log('angle', angle)
      return angle
    }

    this.world.bringToTop(player)

    enema = this.game.add.group()

    gauge = new Gauge(this.game, 26, 56)
    // gauge.resize(10)
    gauge.setEnergyRegeneration(1, this.eneGenDelay)
    gauge.setShieldRegeneration(1, this.shiGenDelay)
    this.ui.add(gauge)

    this.score = new Counter(this.game, 4, 4, 'MicroFont', 5)
    this.ui.add(this.score)

    this.shopSlider = new ShopSlider(this)
    this.gameOver = new GameOver(this)
    this.gameOver.onHide(() => {
      this.msGameOver.stop()
    })

    this.shopSlider.onShow(() => {
      this.fxSlide.play()
      this.msSpace.stop()
      this.msWorld.stop()
      this.msShop.loopFull()

      this.background.stopScroll()
      this.game.add.tween(this.background).to({alpha: 0}, Phaser.Timer.SECOND, Phaser.Easing.Linear.None, true) // , y: 128

      gauge.refill()
      player.stop()
      // gauge.addEnergy(starma.energy)
      starma.energy = 0
    })

    this.shopSlider.onHide((upgs) => {
      this.fxUnslide.play()
      this.msShop.stop()

      upgrades.power = upgs.power
      this.eneGenDelay = Math.round((4 * window.option.diff) - (upgrades.power * 0.5))

      upgrades.structure = upgs.structure
      gauge.resize(10 + 2 * upgrades.structure)
      this.shiGenDelay = Math.round((12 * window.option.diff) - (upgrades.structure * 0.5))

      // gauge.refill()
      upgrades.skill = upgs.skill
      this.starCooldown = (0.5 - (0.05 * upgrades.skill)) * window.option.diff
      this.starLimit = Math.floor(1 + (3 - window.option.diff) + (upgrades.skill / 2))
      for (let i = starma.children.length; i < this.starLimit; ++i) {
        let star = new Star(this.game, 0xddddff)
        starma.add(star)
      }

      this.score.set(upgs.score)

      // Transition from World to Space
      let bgName = window.space ? 'world' : 'space'
      window.space = !window.space

      if (window.space) {
        this.msSpace.loopFull()
      } else {
        this.msWorld.loopFull()
      }

      // If Pick a random backgroound different from the previous
      let newBackground = window.background
      while (newBackground === window.background) {
        newBackground = this.game.rnd.integerInRange(1, 5)
      }
      window.background = newBackground

      // console.log('background', bgName + window.background.toString())
      this.background.frameName = bgName + window.background.toString()
      // this.background.y = -66
      this.game.add.tween(this.background).to({alpha: 1.0}, Phaser.Timer.SECOND, Phaser.Easing.Linear.None, true) // , y: -1

      this.background.autoScroll(0, 4 * window.option.diff)
      this.waveLimit *= 2
    })

    // Initialize Physics
    this.physics.startSystem(Phaser.Physics.ARCADE)

    // Keyboard Events
    keyboard = this.game.input.keyboard.addKeys({
      'up': Phaser.KeyCode.UP,
      'down': Phaser.KeyCode.DOWN,
      'left': Phaser.KeyCode.LEFT,
      'right': Phaser.KeyCode.RIGHT,
      'w': Phaser.KeyCode.W,
      's': Phaser.KeyCode.S,
      'a': Phaser.KeyCode.A,
      'd': Phaser.KeyCode.D,
      'spacebar': Phaser.KeyCode.SPACEBAR
    })
    let keycodes = Object.keys(keyboard).map(key => keyboard[key].keyCode)
    // console.log(keycodes)
    this.game.input.keyboard.addKeyCapture(keycodes)
    keyboard.charge = false
    keyboard.previous = { x: player.x, y: player.y + 0.5 }
    this.updatePreviousPosition = function () {
      keyboard.previous.x = player.position.x
      keyboard.previous.y = player.position.y
    }

    // Last Pointer X and Y
    this.lmx = 0
    this.lmy = 0

    // Start or Restart the Music if it's already playing
    if (this.spacemusic || this.worldmusic) {
      this.msSpace.stop()
      this.msWorld.stop()
      this.msGameOver.stop()
      this.msShop.stop()
    } else {
      this.msSpace = this.game.add.audio('music_space')
      this.msSpace.volume = 0.25
      this.msWorld = this.game.add.audio('music_world')
      this.msWorld.volume = 0.25
      this.msGameOver = this.game.add.audio('music_gameover')
      this.msGameOver.volume = 0.5
      this.msShop = this.game.add.audio('music_shop')
      this.msShop.volume = 0.75
    }
    if (window.space) {
      this.msSpace.loopFull()
    } else {
      this.msWorld.loopFull()
    }

    this.fxThrow = this.game.add.audio('throw')
    this.fxCatch = this.game.add.audio('catch')
    this.fxBlast = this.game.add.audio('blast')
    this.fxPickup1 = this.game.add.audio('pickup1')
    this.fxPickup2 = this.game.add.audio('pickup2')
    this.fxExplosion1 = this.game.add.audio('explosion1')
    this.fxExplosion2 = this.game.add.audio('explosion2')
    this.fxExplosion3 = this.game.add.audio('explosion3')
    this.fxNo = this.game.add.audio('no')
    this.fxOh = this.game.add.audio('oh')
    this.fxSlide = this.game.add.audio('slide')
    this.fxUnslide = this.game.add.audio('unslide')
    // this.fxGameOver = this.game.add.audio('gameover')

    starma.throw = () => {
      if (this.lockStar || starma.energy < 1) {
        return
      }

      let newStar = starma.getFirstDead(false)
      if (!newStar) {
        // console.log('no stars left')
        return false
      }

      player.showGlow(false)
      this.fxThrow.play()
      this.lastCharge = starma.energy

      newStar.throw(starma.energy, player.x, player.y, this.calculateAngle(), upgrades.power, upgrades.skill)
      starma.energy = 0.0
      this.lockStar = true
      this.game.time.events.add(Phaser.Timer.SECOND * this.starCooldown, function () {
        this.lockStar = false
      }, this)
      return true
    }

    starma.charge = () => {
      if (this.lockStar || !starma.getFirstDead(false)) {
        return
      }

      // console.log('starma.charge', starma.energy, gauge.getEnergy())

      if (starma.energy === 0 && gauge.getEnergy() >= 1) {
        starma.energy = 1
        gauge.addEnergy(-1)
        this.armStart = this.game.time.totalElapsedSeconds()
      } else if (starma.energy < (3 + upgrades.skill) && gauge.getEnergy() >= 1 &&
      this.game.time.totalElapsedSeconds() > (this.armStart + this.starCooldown)) {
        this.armStart = this.game.time.totalElapsedSeconds()
        starma.energy += 1
        gauge.addEnergy(-1)
      } else if (starma.energy === (3 + upgrades.skill)) {
        player.showGlow(true)
      }
    }

    this.sendWave()
  }

  update () {
    let self = this

    if (self.shopSlider.active) {
      return
    }

    // enema.forEach((enemy) => {
    //   if (enemy.angle === Phaser.ANGLE_RIGHT) {
    //     enemy.x -= 1
    //   } else if (enemy.angle === Phaser.ANGLE_DOWN) {
    //     enemy.y += 1
    //   } else if (enemy.angle === Phaser.ANGLE_LEFT) {
    //     enemy.x += 1
    //   } else if (enemy.angle === Phaser.ANGLE_UP) {
    //     enemy.y -= 1
    //   }
    // })

    // Player center
    let px = player.position.x
    let py = player.position.y

    // Pointer position
    let pointer = self.game.input.activePointer
    let mx = pointer.position.x
    let my = pointer.position.y

    this.calculateAngle()

    // If the Player is Alive
    if (player.isAlive()) {
      // if (mx !== this.lmx || my !== this.lmy) {
      if (this.game.input.activePointer.withinGame) {
        // Move the Player to the Pointer
        if (Math.abs(px - mx) > 1 || Math.abs(py - my) > 1) {
          player.moveToPointer(player.speed)
        } else {
          player.stop()
          this.lmx = mx
          this.lmy = my
        }
      } else {
        player.body.velocity.y = 0
        if (keyboard['up'].isDown || keyboard['w'].isDown) {
          this.updatePreviousPosition()
          player.body.velocity.y = -player.speed
        } else if (keyboard['down'].isDown || keyboard['s'].isDown) {
          this.updatePreviousPosition()
          player.body.velocity.y = player.speed
        }

        player.body.velocity.x = 0
        if (keyboard['left'].isDown || keyboard['a'].isDown) {
          this.updatePreviousPosition()
          player.body.velocity.x = -player.speed
        } else if (keyboard['right'].isDown || keyboard['d'].isDown) {
          this.updatePreviousPosition()
          player.body.velocity.x = player.speed
        }
      }

      if (pointer.isDown || keyboard['spacebar'].isDown) {
        starma.charge()
      }

      if (pointer.isUp && keyboard['spacebar'].isUp) {
        starma.throw()
      }
    }

    // Check for Player-Star collisions
    self.game.physics.arcade.overlap(player, starma, function (goodguy, star) {
      if (star.alive && star.isCatchable()) {
        // console.log('star-player', gauge.getEnergy(), star.getEnergy())

        self.fxCatch.play()
        let energy = star.catch()
        gauge.addEnergy(energy)
        self.score.plus(Math.floor(energy / 2.0))
      }
    })

    // Check for Star-Enemy collisions
    self.game.physics.arcade.overlap(enema, starma, function (enemy, star) {
      if (!star.alive) {
        return
      }

      // console.log('star-enemy', star.getEnergy(), enemy.getHealth())

      let enemyHealth = enemy.getHealth()
      let starEnergy = star.getEnergy()

      if (starEnergy > 0.0) {
        starEnergy = Math.round(starEnergy / 2)
      }

      star.catch(enemyHealth)

      let prize = enemy.getLevel() + enemy.getType()

      // If it destroys the enemy
      if (enemy.damage(starEnergy)) {
        this.playRandomExplosion(enemy.getType() === 0)
        if (enemy.getType() === 6) {
          prize += 50 * enemy.getLevel()
        }
        // self.score.plus(prize * window.option.diff)

        this.killCount += enemy.level
        // console.log('kill count', this.killCount)
        if (this.killCount > this.killsRequired) {
          this.pickup.pop()
          this.killCount -= this.killsRequired
        }
      } else {
        prize += 5 * enemy.getLevel()
      }

      self.score.plus(prize * window.option.diff)
    }, null, this)

    // Check for Enemy-Player collisions
    self.game.physics.arcade.overlap(enema, player, function (goodguy, badguy) {
      // console.log('enemy-player', badguy.getHealth(), gauge.getArmor() + gauge.getShield())

      if (gauge.damage(badguy.getHealth())) {
        this.playRandomExplosion()
        goodguy.die()
        starma.throw()

        starma.energy = 0
        gauge.visible = false
        self.score.setAlpha(1.0)

        self.msSpace.stop()
        self.msWorld.stop()
        self.msGameOver.loopFull()

        // self.fxGameOver.play()
        self.gameOver.show()
      }

      if (badguy.damage(gauge.getDefense())) {
        this.playRandomExplosion(badguy.getType() === 0)
        let prize = badguy.getLevel() + badguy.getType()
        if (badguy.getType() === 6) {
          prize += 10 * badguy.getLevel()
        }
        self.score.plus(prize)

        this.killCount += badguy.level
        // console.log('kill count', this.killCount)
        if (this.killCount > this.killsRequired) {
          this.pickup.pop()
          this.killCount -= this.killsRequired
        }
      }
    }, null, this)

    for (let e in enema.children) {
      let enemy = enema.children[e]

      let isAlive = enemy.getHealth() > 0.0
      // Kill enemies that went too far beyond the Scrreen, given their direction
      if (isAlive) {
        // console.log(enemy.angle, Phaser.ANGLE_UP, enemy.y, 0 - enemy.height)
        // console.log(enemy.angle, enemy.x, enemy.y)
        if (enemy.direction === Phaser.ANGLE_RIGHT && enemy.x > 74 + enemy.width) {
          enemy.damage()
        } else if (enemy.direction === Phaser.ANGLE_DOWN && enemy.y > 74 + enemy.height) {
          enemy.damage()
        } else if (enemy.direction === Phaser.ANGLE_LEFT && enemy.x < -10 - enemy.width) {
          enemy.damage()
        } else if (enemy.direction === Phaser.ANGLE_UP && enemy.y < -10 - enemy.height) {
          enemy.damage()
        }
      }
      // if (isAlive && enemy.y > 64 + enemy.height) {
      //   enemy.damage()
      // }

      for (let j in enemy.weapons) {
        let weapon = enemy.weapons[j]

        // If the Enemy is dead, destroy all Weapons and respective Bullets
        if (!isAlive) {
          weapon.destroy()
          continue
        }

        // Check for EnemyBullet-Star collisions
        self.game.physics.arcade.overlap(weapon.bullets, starma, function (bullet, star) {
          if (!star.alive) {
            return
          }

          // console.log('bullet-star', bullet.parent.level, star.getEnergy())
          // console.log(bullet)
          // // let bulletEnergy = bullet.level // Math.round(enemy.getLevel() / 2)
          //
          // let starEn = star.getEnergy()
          //
          // x('bullet-star', bullet.energy, starEn)

          // if (starEn > bullet.health) {
          //   bullet.kill()
          //   star.catch(bullet.health)
          // } else {
          //   bullet.energy -= starEn
          // }

          bullet.kill()
          star.gain(1.0)
        })

        // Check for EnemyBullet-Player collisions
        self.game.physics.arcade.overlap(weapon.bullets, player, function (goodguy, bullet) {
          // console.log('bullet-player', gauge.getArmor() + gauge.getShield(), bullet.health)

          if (gauge.damage(1)) { // bullet.health)) {
            self.playRandomExplosion()
            goodguy.die()
            starma.throw()

            starma.energy = 0
            gauge.visible = false
            self.score.setAlpha(1.0)
            self.msSpace.stop()
            self.msWorld.stop()
            self.msGameOver.loopFull()
            // self.fxGameOver.play()
            self.gameOver.show()
          } else {
            bullet.kill()
          }
        })
      }

      if (!isAlive) {
        // Clean Enemies up
        enema.remove(enemy)
        enemy.destroy()
      } else {
        // Fire all Enemy weapons
        enemy.fire()
      }
    }

    if (self.pickup.alive && player.touches(self.pickup)) {
      // console.log(self.pickup.getType())
      switch (self.pickup.getType()) {
        case 0:
          gauge.addShield(2)
          self.fxPickup1.play()
          self.score.plus(4)
          break
        case 1:
          gauge.addShield(4)
          self.fxPickup1.play()
          self.score.plus(8)
          break
        case 2:
          gauge.addShield(6)
          self.fxPickup1.play()
          self.score.plus(12)
          break
        case 3:
          gauge.addEnergy(2)
          self.fxPickup2.play()
          self.score.plus(4)
          break
        case 4:
          gauge.addEnergy(4)
          self.fxPickup2.play()
          self.score.plus(8)
          break
        case 5:
          gauge.addEnergy(6)
          self.fxPickup2.play()
          self.score.plus(12)
          break
      }

      self.pickup.stop()
    }

    // Send a new Wave if there are no Enemies
    if (enema.children.length === 0 && player.isAlive()) {
      self.sendWave()
    }

    self.game.world.bringToTop(self.ui)
    self.game.world.bringToTop(self.shopSlider)
    self.game.world.bringToTop(self.gameOver)
  }

  sendWave () {
    // if (upgrades.score === 0) {
    //   this.score.plus(15000)
    //   upgrades.power = 0
    //   upgrades.structure = 0
    //   upgrades.skill = 0
    //   upgrades.score = this.score.get()
    //   this.shopSlider.show(upgrades)
    //   return
    // }

    this.waveCount += window.option.diff
    if (this.waveCount > this.waveLimit) {
      upgrades.score = this.score.get()
      this.shopSlider.show(upgrades)
      return
    }

    // console.log('sendWave', this.waveCount)

    let waveLevel = 1 + Math.floor(this.waveCount / 2)
    if (waveLevel > this.waveLevelLimit) {
      waveLevel = this.waveLevelLimit
    }

    // Easier modes lock enemies to less directions (Easy = Down + One side)
    let redirection = this.game.rnd.integerInRange(0, 2)
    let side = this.game.rnd.integerInRange(0, 1)

    // console.log('wl', waveLevel)
    while (waveLevel > 0) {
      let type = this.game.rnd.weightedPick([1, 2, 3, 4, 5, 0, 6]) // this.game.rnd.integerInRange(0, 6)
      let level = this.game.rnd.integerInRange(1, 3)
      if (window.option.diff === 3) { // Hard mode: Enemies from all directions
        redirection = this.game.rnd.integerInRange(0, 2)
      }
      if (level + redirection > waveLevel) {
        if (waveLevel - level > 0) {
          redirection = waveLevel - level
        }
      }
      // if (redirection === 0) {
      let direction = Phaser.ANGLE_DOWN
      if (redirection === 1) {
        if (window.option.diff >= 2) { // Normal mode: Enemies from either side
          side = this.game.rnd.integerInRange(0, 1)
        }
        if (side === 0) {
          direction = Phaser.ANGLE_RIGHT
        } else {
          direction = Phaser.ANGLE_LEFT
        }
      } else if (redirection === 2) {
        direction = Phaser.ANGLE_UP
      }
      // console.log(type, level, redirection)
      if (type === 6) {
        waveLevel -= level * 2
      } else {
        waveLevel -= level
      }
      this.makeEnemy(type, level, direction)
      // let x = this.game.rnd.integerInRange(0, 3)
      // this.makeEnemy(6, level, x * 90) // type, level) // , Phaser.ANGLE_UP)
    }
  }

  makeEnemy (type, level, direction = Phaser.ANGLE_DOWN) {
    if (level === undefined) {
      level = this.game.rnd.integerInRange(1, 3)
    }

    if (type === undefined) {
      type = this.game.rnd.integerInRange(0, 6)
    }

    let randomPos = this.game.rnd.integerInRange(6, 58)
    let x = 0
    let y = 0

    if (direction === Phaser.ANGLE_RIGHT) {
      x = -20 // 84
      y = randomPos
    } else if (direction === Phaser.ANGLE_DOWN) {
      x = randomPos
      y = -20
    } else if (direction === Phaser.ANGLE_LEFT) {
      x = 84
      y = randomPos
    } else if (direction === Phaser.ANGLE_UP) {
      x = randomPos
      y = 84
    }

    let enemy = new Enemy(this.game, x, y, type, level, direction)
    this.add.existing(enemy)

    // enemy.angle = direction - 90

    enemy.onFire(() => {
      this.fxBlast.play()
    })

    if (type < 5) {
      enemy.addWeapon()
    } else if (type === 5) {
      enemy.addDirectionalWeapon(Phaser.ANGLE_RIGHT) // 0
      enemy.addDirectionalWeapon(Phaser.ANGLE_DOWN) // 90
      enemy.addDirectionalWeapon(Phaser.ANGLE_LEFT) // 180
      enemy.addDirectionalWeapon(Phaser.ANGLE_UP) // 270
    } else if (type === 6) {
      let sideWeapons = this.game.rnd.integerInRange(1, 5)
      let sideWeapons2 = this.game.rnd.integerInRange(1, 4)
      let sideWeapons3 = this.game.rnd.integerInRange(1, 3)

      // let ox = 0
      // let oy = 0
      // let rox = 0
      // let roy = 0
      // // if (enemy.direction === Phaser.ANGLE_LEFT || enemy.direction === Phaser.ANGLE_RIGHT) {
      // oy = 6
      // roy = -6
      // console.log('foo', oy, roy)
      // } else if (enemy.direction === Phaser.ANGLE_DOWN || enemy.direction === Phaser.ANGLE_UP) {
      //   ox = 6
      //   rox = -6
      //   console.log('bar', ox, rox)
      // }

      if (level === 1) {
        enemy.addWeapon()
        enemy.addWeapon(undefined, sideWeapons2, 0, 6)
        enemy.addWeapon(undefined, sideWeapons2, 0, -6)
      } else if (level === 2) {
        enemy.addWeapon() // undefined) // player)
        enemy.addWeapon(undefined, sideWeapons2, 0, 6)
        enemy.addWeapon(undefined, sideWeapons2, 0, -6)
        enemy.addDirectionalWeapon(enemy.angle, sideWeapons3)
        enemy.addDirectionalWeapon(enemy.angle + 180, sideWeapons3)
        // enemy.addWeapon(undefined, sideWeapons2, 6, 0) // (player
        // enemy.addWeapon(undefined, sideWeapons2, -6, 0) // (player
      } else if (level === 3) {
        enemy.addWeapon() // player)
        // enemy.addWeapon(undefined, sideWeapons, 3, 0) // (player
        // enemy.addWeapon(undefined, sideWeapons, -3, 0) // (player
        enemy.addWeapon(undefined, sideWeapons, 0, 6) // (player
        enemy.addWeapon(undefined, sideWeapons, 0, -6) // (player
        // enemy.addWeapon(undefined, sideWeapons3, 8, 0)
        // enemy.addWeapon(undefined, sideWeapons3, -8, 0)
        enemy.addDirectionalWeapon(enemy.angle, sideWeapons2)
        enemy.addDirectionalWeapon(enemy.angle + 180, sideWeapons2)
        enemy.addDirectionalWeapon(enemy.angle + 90, sideWeapons3)
        enemy.addDirectionalWeapon(enemy.angle + 270, sideWeapons3)
      }
    }
    // }

    let speed = 2
    if (type === 0) {
      speed = this.rnd.integerInRange(8, 12)
    } else if (type < 6) {
      speed = this.rnd.integerInRange(6, 10)
    } else if (type > 5) {
      speed = this.rnd.integerInRange(4, 8)
    }

    if (window.option.diff < 3) {
      if (enemy.direction === Phaser.ANGLE_RIGHT || enemy.direction === Phaser.ANGLE_LEFT) {
        speed *= window.option.diff === 2 ? 0.8 : 0.6
      } else if (enemy.direction === Phaser.ANGLE_UP) {
        speed *= window.option.diff === 2 ? 0.6 : 0.4
      }
    }

    speed *= (0.5 * window.option.diff)

    if (direction === Phaser.ANGLE_RIGHT) {
      enemy.moveTo(128, enemy.getPosition().y, speed)
    } else if (direction === Phaser.ANGLE_DOWN) {
      enemy.moveTo(enemy.getPosition().x, 128, speed)
    } else if (direction === Phaser.ANGLE_LEFT) {
      enemy.moveTo(-128, enemy.getPosition().y, speed)
    } else if (direction === Phaser.ANGLE_UP) {
      enemy.moveTo(enemy.getPosition().x, -128, speed)
    }

    // enemy.angle = 90 // Phaser.ANGLE_RIGHT

    enema.add(enemy)
  }

  render () {
    // this.game.debug.body(player)
    // this.game.debug.body(this.pickup)
    // starma.forEach((s) => {
    //   this.game.debug.body(s)
    // })

    // enema.forEach((e) => {
    //   this.game.debug.body(e)
    //   e.weapons.forEach((w) => {
    //     w.forEach((sw) => {
    //       this.game.debug.body(sw)
    //     })
    //   })
    // })

    if (!this.shopSlider.active &&
        player.alive &&
        (this.game.input.activePointer.isDown ||
        keyboard['spacebar'].isDown)) {
      if (window.option.diff < 3) {
        this.game.debug.geom(shotGuide)
      }

      // for (let a in directionGuide) {
      //   this.game.debug.geom(directionGuide[a])
      // }
    }
  }

  playRandomExplosion (asteroid = false) {
    let sfx = this.game.rnd.integerInRange(1, 3)
    if (!asteroid) {
      let extrafx = this.game.rnd.integerInRange(1, 10)
      if (extrafx === 10) {
        switch (this.game.rnd.integerInRange(1, 2)) {
          case 1:
            this.fxNo.play()
            break
          case 2:
            this.fxOh.play()
            break
        }
      }
    }
    switch (sfx) {
      case 1:
        this.fxExplosion1.play()
        break
      case 2:
        this.fxExplosion2.play()
        break
      case 3:
        this.fxExplosion3.play()
        break
    }
  }

  // equalAngles (a, b) {
  //   // console.log(a, '=', b, a === 360 + b)
  //   return (a === b || a === -b || a === 360 + b || a === 360 - b)
  // }
}
