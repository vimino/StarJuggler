// Copyright (c) 2016-2021, VIMinO
//
// This source code file is under the GPLv3 license, which can be found at:
// https://www.gnu.org/licenses/gpl-3.0.en.html
// Or the attached LICENSE file

import PIXI from 'expose-loader?PIXI!phaser-ce/build/custom/pixi.js' // eslint-disable-line
import p2 from 'expose-loader?p2!phaser-ce/build/custom/p2.js' // eslint-disable-line
import Phaser from 'expose-loader?Phaser!phaser-ce/build/custom/phaser-split.js' // eslint-disable-line

export default class Gauge extends Phaser.Group {
  constructor (game, x, y, size = 10) {
    super(game)

    this.x = x
    this.y = y

    this.gauge = game.make.sprite(0, 0, 'ui')
    this.gauge.frameName = 'gauge' + size.toString()

    this.energy = game.make.sprite(1, 1, 'ui')
    this.armor = game.make.sprite(1, 3, 'ui')
    this.shield = game.make.sprite(1, 3, 'ui')

    this.resize(size)

    this.regenEnergy = 0
    this.timerEnergy = undefined
    this.regenArmor = 0
    this.timerArmor = undefined
    this.regenShield = 0
    this.timerShield = undefined

    this.add(this.gauge)
    this.add(this.energy)
    this.add(this.armor)
    this.add(this.shield)
  }

  // Main methods

  clear () {
    this.setEnergy(0)
    this.setArmor(0)
    this.setShield(0)
  }

  resize (size) {
    if (size < 10 || size > 24 || size % 2 !== 0) {
      console.error('Error@Gauge.updateSize: Invalid size, ' + size.toString())
      return
    } else if (size === this.size) {
      console.warn('Warning@Gauge.updateSize: The new size is the same as the current one')
    }

    this.gauge.frameName = 'gauge' + size.toString()
    let sfn = 'g' + size.toString()
    this.energy.frameName = sfn + '-energy'
    this.armor.frameName = sfn + '-armor'
    this.shield.frameName = sfn + '-shield'

    this.x = Math.floor(this.game.width / 2 - this.gauge.width / 2)
    this.energy.x = this.gauge.x + 1
    this.armor.x = this.gauge.x + 1
    this.shield.x = this.gauge.x + 1

    this.size = size
    this.refill()
    // this.setEnergy(size) // this.valueEnergy
    // this.setArmor(size)  // this.valueArmor
    // this.setShield(size) // this.valueShield
  }

  damage (value) {
    if (!value || value < 0) {
      console.warn('Warning@Gauge.damage: Invalid damage, ', value)
      return
    }

    // console.log('Gauge.damage', value)

    let remainingShield = this.valueShield - value

    if (remainingShield > 0) {
      this.addShield(-value)
    } else {
      if (remainingShield === 0 && this.valueShield !== 0) {
        this.setShield(0)
      }

      this.addArmor(remainingShield)
    }

    // Return wether the Player died
    return (this.valueArmor <= 0)
  }

  getDefense () {
    return (this.valueShield + this.valueArmor)
  }

  // Energy methods

  addEnergy (value) {
    if (value) {
      this.valueEnergy += value
      this.updateEnergy()
    }
  }

  takeEnergy (value) {
    if (value) {
      return this.addEnergy(-value)
    }
  }

  setEnergy (value) {
    if (value) {
      this.valueEnergy = value
      this.updateEnergy()
    }
  }

  getEnergy (value) {
    return this.valueEnergy
  }

  updateEnergy () {
    if (this.valueEnergy > this.size) {
      this.valueEnergy = this.size
    } else if (this.valueEnergy < 0) {
      this.valueEnergy = 0
    }

    // The Energy gauge is reversed (right-to-left)
    let energy = Math.floor(this.valueEnergy)
    this.energy.x = this.gauge.x + this.size + 1 - energy

    let crop = new Phaser.Rectangle(this.size - energy, 0, energy, 3)
    this.energy.crop(crop)
  }

  setEnergyRegeneration (value, delay) {
    let self = this
    if (self.timerEnergy) { self.game.time.events.remove(self.timerEnergy) }
    self.regenEnergy = value
    self.timerEnergy = self.game.time.events.loop(Phaser.Timer.SECOND * delay,
      () => { self.addEnergy(self.regenEnergy) }, self)
  }

  getEnergyRegen () {
    return this.regenEnergy
  }

  // Armor methods

  addArmor (value) {
    this.valueArmor += value
    this.updateArmor()
    return (this.valueArmor < 1)
  }

  takeArmor (value) {
    return this.addArmor(-value)
  }

  setArmor (value) {
    this.valueArmor = value
    this.update()
  }

  getArmor (value) {
    return this.valueArmor
  }

  updateArmor () {
    if (this.valueArmor > this.size) {
      this.valueArmor = this.size
    } else if (this.valueArmor < 0) {
      this.valueArmor = 0
    }

    let crop = new Phaser.Rectangle(0, 0, Math.floor(this.valueArmor), 3)
    this.armor.crop(crop)
  }

  setArmorRegeneration (value, delay) {
    let self = this
    if (self.timerArmor) { self.game.time.events.remove(self.timerArmor) }
    self.regenArmor = value
    self.timerArmor = self.game.time.events.loop(Phaser.Timer.SECOND * delay,
      () => { self.addArmor(self.regenArmor) }, self)
  }

  getArmorRegen () {
    return this.regenShield
  }

  // Shield methods

  addShield (value) {
    this.valueShield += value
    this.updateShield()
    return this.valueShield < 1
  }

  takeShield (value) {
    return this.addShield(-value)
  }

  setShield (value) {
    this.valueShield = value
    this.updateShield()
  }

  getShield (value) {
    return this.valueShield
  }

  updateShield () {
    if (this.valueShield > this.size) {
      this.valueShield = this.size
    } else if (this.valueShield < 0) {
      this.valueShield = 0
    }

    let crop = new Phaser.Rectangle(0, 0, Math.floor(this.valueShield), 3)
    this.shield.crop(crop)
  }

  setShieldRegeneration (value, delay) {
    let self = this
    if (self.timerShield) { self.game.time.events.remove(self.timerShield) }
    self.regenShield = value
    self.timerShield = self.game.time.events.loop(Phaser.Timer.SECOND * delay,
      () => { self.addShield(self.regenShield) }, self)
  }

  getShieldRegen () {
    return this.regenShield
  }

  refill () {
    this.setEnergy(this.size)
    this.setArmor(this.size)
    this.setShield(this.size)
  }
}
