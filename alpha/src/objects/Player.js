// Copyright (c) 2016-2021, VIMinO
//
// This source code file is under the GPLv3 license, which can be found at:
// https://www.gnu.org/licenses/gpl-3.0.en.html
// Or the attached LICENSE file

import PIXI from 'expose-loader?PIXI!phaser-ce/build/custom/pixi.js' // eslint-disable-line
import p2 from 'expose-loader?p2!phaser-ce/build/custom/p2.js' // eslint-disable-line
import Phaser from 'expose-loader?Phaser!phaser-ce/build/custom/phaser-split.js' // eslint-disable-line

export default class Player extends Phaser.Sprite {
  constructor (game, x = 0, y = 0, speed = 10) {
    super(game, x, y, 'ships')
    this.frameName = 'player'
    this.anchor.setTo(0.5, 0.5)

    this.alive = true

    this.speed = speed

    this.chargeFrame = 1
    this.charge = this.game.add.sprite(0, 0, 'ships', 'glow1') // this.x - 1, this.y - 1
    // this.charge.frameName = 'glow1'
    this.charge.anchor.setTo(0.5, 0.5)
    this.charge.alpha = 0.5
    this.charge.visible = false
    this.addChild(this.charge)

    game.time.events.loop(Phaser.Timer.SECOND / 10, function () {
      if (this.chargeFrame === 1) {
        this.chargeFrame = 2
      } else {
        this.chargeFrame = 1
      }
      this.charge.frameName = 'glow' + this.chargeFrame.toString()
    }, this)

    // Physics
    this.game.physics.arcade.enable(this)
    this.body.setSize(5, 7, 2, 1)
    this.body.collideWorldBounds = true
    this.body.allowGravity = false
    this.body.immovable = true
  }

  // update () {
  //   this.charge.position.setTo(this.x, this.y)
  // }

  moveToPointer () {
    this.game.physics.arcade.moveToPointer(this, this.speed)
  }

  stop () {
    this.body.stop() // .velocitysetTo(0, 0)
  }

  show (state) {
    this.visible = state
  }

  touches (object) {
    return this.game.physics.arcade.collide(this, object)
  }

  isAlive () {
    return this.alive
  }

  die () {
    this.showGlow(true)
    this.alive = false
    this.kill()

    let explosion = this.game.add.sprite(this.x, this.y, 'explosion')
    explosion.anchor.setTo(0.5, 0.5)
    explosion.animations.add('boom', [0, 1, 2, 3, 4], 5, false).onComplete.add(function () {
      explosion.kill()
    }, this)
    explosion.animations.play('boom')
  }

  showGlow (enabled) {
    this.charge.visible = enabled
  }

  setSpeed (speed) {
    this.speed = speed
  }
}
