// Copyright (c) 2016-2021, VIMinO
//
// This source code file is under the GPLv3 license, which can be found at:
// https://www.gnu.org/licenses/gpl-3.0.en.html
// Or the attached LICENSE file

import PIXI from 'expose-loader?PIXI!phaser-ce/build/custom/pixi.js' // eslint-disable-line
import p2 from 'expose-loader?p2!phaser-ce/build/custom/p2.js' // eslint-disable-line
import Phaser from 'expose-loader?Phaser!phaser-ce/build/custom/phaser-split.js' // eslint-disable-line

export default class Counter extends Phaser.Group {
  constructor (game) {
    super(game)

    let gameOver = this.game.add.sprite(this.game.world.centerX, 12, 'ui', 'gameover')
    gameOver.anchor.setTo(0.5, 0)
    this.add(gameOver)

    // let replayButton = this.game.add.sprite(this.game.world.centerX, 25, 'ui', 'replay')
    // replayButton.anchor.setTo(0.5, 0)
    // replayButton.inputEnabled = true
    // replayButton.events.onInputDown.add(this.replay, this)
    // this.add(replayButton)

    this.replay = game.add.group()
    this.replay.position.setTo(5, 30)

    this.replayButton = this.game.add.sprite(0, 0, 'ui', 'replay')
    // this.replayButton.anchor.setTo(0, 0)
    this.replayButton.inputEnabled = true
    this.replayButton.events.onInputDown.add(this.toggleReplay, this)
    this.replayButton.events.onInputUp.add(this.toggleReplay, this)
    this.replayButton.events.onInputOut.add(
      () => { this.toggleReplay(null) },
      this)
    this.replayButton.cancel = false
    this.replayPattern = this.game.add.sprite(1, 1, 'ui', 'replayPat1')

    this.replayPattern.alpha = 0.4

    this.replay.add(this.replayButton)
    this.replay.add(this.replayPattern)
    this.add(this.replay)

    // let quitButton = this.game.add.sprite(this.game.world.centerX, 40, 'ui', 'quit')
    // quitButton.anchor.setTo(0.5, 0)
    // quitButton.inputEnabled = true
    // quitButton.events.onInputDown.add(this.quit, this)
    // this.add(quitButton)

    this.quit = game.add.group()
    this.quit.position.setTo(35, 30)

    this.quitButton = game.add.sprite(0, 0, 'ui', 'quit')
    // this.quitButton.anchor.setTo(0, 0)
    this.quitButton.inputEnabled = true
    this.quitButton.events.onInputDown.add(this.toggleQuit, this)
    this.quitButton.events.onInputUp.add(this.toggleQuit, this)
    this.quitButton.events.onInputOut.add(
      () => { this.toggleQuit(null) },
      this)
    this.quitButton.cancel = false

    this.quitPattern = this.game.add.sprite(1, 1, 'ui', 'quitPat1')
    this.quitPattern.alpha = 0.4

    this.quit.add(this.quitButton)
    this.quit.add(this.quitPattern)
    this.add(this.quit)
    // this.quit.visible = false

    this.visible = false
    // this.alpha = 0.6
    this.y = -64

    this.pattern = 0
    this.wait = 0
  }

  show () {
    this.game.add.tween(this).to({y: 0}, 1000, Phaser.Easing.Bounce.Out, true)
    this.visible = true
  }

  onHide (callback) {
    this.onHide = callback
  }

  update () {
    if (!this.visible) {
      return
    }

    this.wait++
    if (this.wait > 10) {
      this.pattern++
      if (this.pattern > 4) this.pattern = 1

      this.replayPattern.frameName = ('replayPat' + this.pattern.toString())
      this.quitPattern.frameName = ('quitPat' + this.pattern.toString())
      this.wait = 0
    }
  }

  // replay () {
  //   this.game.state.start(''')
  // }

  // quit () {
  //   this.game.state.start('Menu')
  // }

  toggleReplay (element, event) {
    if (event != null && event.isDown) {
      this.replayButton.frameName = 'replayP'
      this.replayPattern.alpha = 0.8
      this.replayButton.cancel = false
    } else {
      this.replayButton.frameName = 'replay'
      this.replayPattern.alpha = 0.4

      if (event != null && !this.replayButton.cancel) {
        window.space = true
        this.onHide()
        this.game.state.start('Game')
      } else {
        this.replayButton.cancel = true
      }
    }
  }

  toggleQuit (element, event) {
    if (event != null && event.isDown) {
      this.quitButton.frameName = 'quitP'
      this.quitPattern.alpha = 0.8
      this.quitButton.cancel = false
    } else {
      this.quitButton.frameName = 'quit'
      this.quitPattern.alpha = 0.4

      if (event != null && !this.quitButton.cancel) {
        window.background = undefined
        this.onHide()
        this.game.state.start('Menu') // Test') // Game')
      } else {
        this.quitButton.cancel = true
      }
    }
  }
}
