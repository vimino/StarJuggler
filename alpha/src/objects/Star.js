// Copyright (c) 2016-2021, VIMinO
//
// This source code file is under the GPLv3 license, which can be found at:
// https://www.gnu.org/licenses/gpl-3.0.en.html
// Or the attached LICENSE file

import PIXI from 'expose-loader?PIXI!phaser-ce/build/custom/pixi.js' // eslint-disable-line
import p2 from 'expose-loader?p2!phaser-ce/build/custom/p2.js' // eslint-disable-line
import Phaser from 'expose-loader?Phaser!phaser-ce/build/custom/phaser-split.js' // eslint-disable-line

export default class Star extends Phaser.Sprite {
  constructor (game, color = 0xffffff, speed = 0.025) {
    super(game, -32, -32, 'star', '00')
    this.anchor.setTo(0.5, 0.5)

    this.tint = color
    this.power = 0
    this.speed = speed
    this.level = 0
    this.frameIndex = 0
    this.frameCount = 6

    this.game.physics.arcade.enable(this)
    this.body.collideWorldBounds = false
    this.body.allowGravity = false
    this.body.immovable = true
    this.body.moves = false

    // SECOND / frameCount * 2
    game.time.events.loop(15, this.rotate, this) //  / this.frameCount
    game.time.events.loop(20, this.move, this)

    this.reset()
  }

  rotate () {
    if (this.frameIndex >= this.frameCount) { this.frameIndex = 0 }
    this.frameName = this.power.toString() + this.frameIndex.toString() // 's' + this.frameIndex.toString()
    this.frameIndex += 1
  }

  move () {
    if (!this.alive) {
      return
    }

    if (this.forward) {
      this.shotAngle += Math.PI * this.speed
      this.x = this.cx + this.r * (Math.cos(this.baseAngle) - Math.cos(this.shotAngle))
      this.y = this.cy + this.r * (Math.sin(this.baseAngle) - Math.sin(this.shotAngle))

      if (this.shotAngle > this.startAngle + 2 * Math.PI) {
        this.forward = false
      }
    } else {
      this.alpha -= this.speed / 2.0
      this.shotAngle -= Math.PI * this.speed
      this.x = this.cx - this.r * (Math.cos(this.baseAngle) - Math.cos(this.shotAngle))
      this.y = this.cy - this.r * (Math.sin(this.baseAngle) - Math.sin(this.shotAngle))

      if (this.alpha < 0.1) {
        this.catch()
        this.baseAngle = undefined
        this.shotAngle = undefined
      }
    }
  }

  setColor (color) {
    // Example, Red: 0xff0000
    let hex = parseInt(color, 16)
    // console.log('Star.setColor', hex)
    this.tint = hex
  }

  setSpeed (speed) {
    // console.log('Star.setSpeed', speed)
    this.speed = speed
  }

  reset (x, y) {
    this.visible = false

    let _x = -32
    if (x) { _x = x }
    // this.position.x = _x
    // this.body.position.x = _x

    let _y = -32
    if (y) { _y = y }
    // this.position.y = _y
    // this.body.position.y = _y

    this.position.setTo(_x, _y)
    this.body.reset(_x, _y)

    this.alpha = 1.0
    this.alive = false
    this.forward = true
  }

  // Star.shoot(charged, parent.x, parent.y, angle)
  throw (level, x, y, angle, power, skill) {
    // console.log('Star.throw', level, x, y, angle)

    if (!level) {
      return
    }

    this.power = power

    this.baseAngle = angle
    this.shotAngle = this.baseAngle // reverses baseAngle
    this.startAngle = this.shotAngle

    if (!power) {
      power = 0
    }

    if (level !== this.level) {
      this.loadTexture('star', this.power.toString() + '0')
      // this.frameName = 's1'

      // switch (power) {
      //   case 0:
      //     this.frameCount = 3
      //     break
      //   case 1:
      //     this.frameCount = 5
      //     break
      //   case 2:
      //     this.frameCount = 7
      //     break
      //   case 3:
      //     this.frameCount = 9
      //     break
      //   case 4:
      //     this.frameCount = 11
      //     break
      //   case 5:
      //     this.frameCount = 13
      //     break
      // }

      this.body.setCircle(Math.max(this.width, this.height) / 2.5)
      this.body.reset(x, y)
      let offset = power > 1.0 ? power / 2.0 : power + 0.5
      this.body.offset.setTo(offset, offset)

      this.level = level
    }

    this.r = 4 + level * 2
    if (skill) {
      this.r += Math.round(skill / 2.0)
    }
    this.cx = x
    this.cy = y
    this.reset(x, y)

    this.energy = level * (5 - window.option.diff)
    if (power) {
      this.energy += power
    }
    this.visible = true
    this.alive = true
  }

  getEnergy () {
    return this.energy
  }

  catch (value) {
    // console.log('Star.catch', value)

    let energy = 0.0
    // The Stars' initial energy was multiplied by 2-4 depending on difficulty
    let actualEnergy = this.energy / (5 - window.option.diff)

    // Picking the Star when it's doing circling behind the initial position only gives half the energy
    if (this.alpha < 1.0) {
      actualEnergy /= 2.0
    }

    if (value) {
      energy = Math.abs(actualEnergy - value)
      this.energy -= value
    } else {
      // Catching a Star regains 100% of its energy
      energy = actualEnergy // * 0.9
      this.energy = 0.0
    }

    if (this.energy <= 0.0) {
      this.reset()
    }

    return energy
  }

  isCatchable () {
    return this.alpha < 1.0 ? true : this.shotAngle > this.startAngle + Math.PI
  }

  gain (value) {
    this.energy += value
  }
}
