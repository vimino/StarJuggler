// Copyright (c) 2016-2021, VIMinO
//
// This source code file is under the GPLv3 license, which can be found at:
// https://www.gnu.org/licenses/gpl-3.0.en.html
// Or the attached LICENSE file

import PIXI from 'expose-loader?PIXI!phaser-ce/build/custom/pixi.js' // eslint-disable-line
import p2 from 'expose-loader?p2!phaser-ce/build/custom/p2.js' // eslint-disable-line
import Phaser from 'expose-loader?Phaser!phaser-ce/build/custom/phaser-split.js' // eslint-disable-line

export default class Label extends Phaser.Group {
  constructor (game, x, y, font, size) {
    super(game)

    this.text = ''

    this.label = game.add.bitmapText(x, y, font, this.text, size)
    this.label.alpha = 0.6
    this.add(this.label)
  }

  update () {
    this.label.text = this.text.toString()
  }

  set (value) {
    this.text = value
    this.update()
  }

  get () {
    return this.text
  }

  setAlpha (value) {
    this.label.alpha = value
  }

  moveTo (x = undefined, y = undefined) {
    let newX = x === undefined ? this.label.position.x : x
    let newY = y === undefined ? this.label.position.y : y
    this.label.position.setTo(newX, newY)
  }
}
