// Copyright (c) 2016-2021, VIMinO
//
// This source code file is under the GPLv3 license, which can be found at:
// https://www.gnu.org/licenses/gpl-3.0.en.html
// Or the attached LICENSE file

import PIXI from 'expose-loader?PIXI!phaser-ce/build/custom/pixi.js' // eslint-disable-line
import p2 from 'expose-loader?p2!phaser-ce/build/custom/p2.js' // eslint-disable-line
import Phaser from 'expose-loader?Phaser!phaser-ce/build/custom/phaser-split.js' // eslint-disable-line

import Label from '../objects/Label'

export default class OptionSlider extends Phaser.Group {
  constructor (game) {
    super(game)

    this.position.setTo(64, 64)

    this.panel = game.add.group()

    this.panelFrame = this.game.add.sprite(0, 0, 'panel', 'BluePanel')
    this.panelPattern = this.game.add.sprite(1, 1, 'panel', 'BluePanelPat1')

    this.panel.add(this.panelFrame)
    this.panel.add(this.panelPattern)
    this.add(this.panel)

    this.full = game.add.group()
    this.full.position.setTo(4, 40)
    this.add(this.full)
    // if (!('opts' in this.game)) {
    //   window.option = {
    //     'diff': 2,
    //     'full': false
    //   }
    // }

    this.fullButton = this.game.add.sprite(0, 0, 'ui', 'fullOff')
    this.fullButton.inputEnabled = true
    this.fullButton.events.onInputDown.add(this.toggleFull, this)
    this.fullButton.events.onInputUp.add(this.toggleFull, this)
    this.fullButton.events.onInputOut.add(
      () => { this.toggleFull(null) },
      this)

    this.fullPattern = this.game.add.sprite(1, 1, 'ui', 'fullOffPat1')
    this.fullPattern.alpha = 0.2

    this.full.add(this.fullButton)
    this.full.add(this.fullPattern)

    this.difficulty = game.add.group()
    this.difficulty.position.setTo(4, 26)
    this.add(this.difficulty)
    this.difficulty.active = false

    this.diffBar = this.game.add.sprite(0, 0, 'ui', 'diffBar')

    this.diffBarPattern = this.game.add.sprite(1, 1, 'ui', 'barPat1')
    this.diffBarPattern.alpha = 0.4

    // this.diffButton = this.game.add.sprite(0, -6, 'ui', 'diffButton')
    // this.diffButton = this.game.add.sprite(20, -6, 'ui', 'diffButton')
    this.diffButton = this.game.add.sprite(22, -4, 'ui', 'diffButton')
    // this.diffButton = this.game.add.sprite(47, -6, 'ui', 'diffButton')
    this.diffButton.inputEnabled = true
    this.diffButton.events.onInputDown.add(this.toggleDiff, this)
    this.diffButton.events.onInputUp.add(this.toggleDiff, this)
    this.game.physics.enable(this.diffButton, Phaser.Physics.ARCADE)
    this.diffButPattern = this.game.add.sprite(23, -3, 'ui', 'diffPat1')
    this.diffButPattern.alpha = 0.4

    this.difficulty.add(this.diffBar)
    this.difficulty.add(this.diffBarPattern)
    this.difficulty.add(this.diffButton)
    this.difficulty.add(this.diffButPattern)

    this.volume = game.add.group()
    this.volume.position.setTo(4, 8)
    this.add(this.volume)
    this.volume.active = false

    this.volumeBar = this.game.add.sprite(0, 0, 'ui', 'volumeBar')
    this.volumeBarPattern = this.game.add.sprite(1, 1, 'ui', 'barPat1')
    this.volumeBarPattern.alpha = 0.2

    // 22
    this.volumeButton = this.game.add.sprite(41, -4, 'ui', 'volumeButton')
    this.volumeButton.inputEnabled = true
    this.volumeButton.events.onInputDown.add(this.toggleVolume, this)
    this.volumeButton.events.onInputUp.add(this.toggleVolume, this)

    // 23
    this.volumeButPattern = this.game.add.sprite(this.volumeButton.x + 1, -3, 'ui', 'volumePat1')
    this.volumeButPattern.alpha = 0.4

    this.volume.add(this.volumeBar)
    this.volume.add(this.volumeBarPattern)
    this.volume.add(this.volumeButton)
    this.volume.add(this.volumeButPattern)

    this.closeButton = this.game.add.sprite(50, 50, 'ui', 'BlueBackBut')
    this.add(this.closeButton)

    this.closeButton.inputEnabled = true
    this.closeButton.events.onInputDown.add(this.toggleClose, this)
    this.closeButton.events.onInputUp.add(this.toggleClose, this)
    this.closeButton.events.onInputOut.add(
      () => { this.toggleClose(null) },
      this)

    this.game.physics.enable([this.volumeButton, this.volumeBar, this.diffButton, this.diffBar], Phaser.Physics.ARCADE)
    this.volumeButton.body.setSize(12, 16)
    this.volumeButton.body.immovable = true
    this.diffButton.body.setSize(12, 16)
    this.diffButton.body.immovable = true

    this.title = new Label(this.game, 32, 0, 'MicroFont', 5)
    this.title.setAlpha(0.8)
    this.title.visible = false
    this.add(this.title)

    this.label = new Label(this.game, 32, 0, 'MicroFont', 5)
    this.label.setAlpha(0.8)
    this.label.visible = false
    this.add(this.label)

    if (this.game.sound.mute) {
      this.volumeButton.x = 3
    } else {
      // let volume = Math.round(((this.volumeButton.x - 3.0) / 38.0) * 100.0) / 100.0
      this.volumeButton.x = (this.game.sound.volume * 38.0) + 3.5
    }
    this.volumeButPattern.x = this.volumeButton.x + 1

    if (window.option.diff === 1) {
      this.diffButton.x = 3
    } else if (window.option.diff === 2) {
      this.diffButton.x = 22
    } else if (window.option.diff === 3) {
      this.diffButton.x = 41
    }
    this.diffButPattern.x = this.diffButton.x + 1

    this.pattern = 0
    this.framePattern = 0

    game.time.events.loop(Phaser.Timer.SECOND / 6, this.animate, this)
  }

  update () {
    if (this.volume.active) {
      let shift = this.game.input.x - (this.volumeButton.x + 6)

      // Dragging volume left
      if (Math.abs(shift) > 2) {
        this.volumeButton.x += shift - 4
      }

      if (this.volumeButton.x < 3) {
        this.volumeButton.x = 3
      } else if (this.volumeButton.x > 41) {
        this.volumeButton.x = 41
      }

      this.updateLabel(1, this.volumeButton.x)
      this.volumeButPattern.x = this.volumeButton.x + 1
    }

    if (this.difficulty.active) {
      // Dragging Diff left
      if (this.game.input.x < this.diffButton.x) {
        if (this.game.input.x < 26) {
          this.diffButton.x = 3
        } else if (this.game.input.x < 44) {
          this.diffButton.x = 22
        }
      } else { // Dragging Diff right
        if (this.game.input.x > 40) {
          this.diffButton.x = 41
        } else if (this.game.input.x > 22) {
          this.diffButton.x = 22
        }
      }

      this.updateLabel(2, this.diffButton.x)
      this.diffButPattern.x = this.diffButton.x + 1
    }
  }

  animate () {
    if (!this.active) {
      return
    }

    this.pattern++
    this.framePattern++
    if (this.pattern > 4) this.pattern = 1
    if (this.framePattern > 6) this.framePattern = 1

    let buttonName = 'full'
    if (this.game.scale.isFullScreen) {
      buttonName += 'On'
    } else {
      buttonName += 'Off'
    }
    // Also update the button since this change could be canceled
    this.fullButton.frameName = buttonName

    // let buttonName = 'full' + (this.game.isFullscreen ? 'On' : 'Off') + 'Pat'
    buttonName += 'Pat'
    this.fullPattern.frameName = (buttonName + this.pattern.toString())
    this.volumeBarPattern.frameName = ('barPat' + this.pattern.toString())
    this.diffBarPattern.frameName = ('barPat' + this.pattern.toString())
    this.volumeButPattern.frameName = ('volumePat' + this.pattern.toString())
    this.diffButPattern.frameName = ('diffPat' + this.pattern.toString())

    this.panelPattern.frameName = 'BluePanelPat' + this.framePattern.toString()

    this.wait = 0
  }

  onShow (callback) {
    this.onShow = callback
  }

  show () {
    if (this.onShow) {
      this.onShow()
    }

    this.game.add.tween(this).to({x: 0, y: 0}, 500, Phaser.Easing.Cubic.In, true)
    this.active = true
  }

  onHide (callback) {
    this.onHide = callback
  }

  hide () {
    if (this.onHide) {
      this.onHide()
    }

    let tween = this.game.add.tween(this).to({x: 64, y: 64}, 500, Phaser.Easing.Cubic.Out, true)
    tween.onComplete.add(() => {
      this.active = false
    }, this)
  }

  toggleClose (element, event) {
    if (event != null && event.isDown) {
      this.closeButton.frameName = 'BlueBackButP'
      this.closeButton.cancel = false
    } else {
      if (event == null) {
        this.closeButton.cancel = true
      }

      if (this.closeButton.cancel) {
        this.closeButton.frameName = 'BlueBackBut'
      } else {
        this.hide()
      }
    }
  }

  toggleFull (element, event) {
    if (event != null && event.isDown) {
      // this.fullButton.frameName = buttonName.concat('P')
      this.fullPattern.alpha = this.game.isFullscreen ? 0.6 : 0.3
      this.fullButton.cancel = false
    } else {
      if (event != null && !this.fullButton.cancel) {
        this.game.isFullscreen = !this.game.isFullscreen
        // buttonName = 'full' + (this.game.isFullscreen ? 'On' : 'Off')
        // buttonName.concat('Pat')

        if (this.game.isFullscreen) {
          this.game.scale.startFullScreen(false)
        } else {
          this.game.scale.stopFullScreen()
        }
      } else {
        this.fullButton.cancel = true
      }

      this.fullPattern.alpha = this.game.isFullscreen ? 0.4 : 0.15
    }

    // this.fullPattern.frameName = buttonName.concat(
    //   'Pat', this.pattern)
  }

  updateLabel (button, setting) {
    let y = 0

    if (button === 1) {
      this.title.set('Volume')
      this.title.moveTo(18.5, 0)

      if (setting === 3) {
        this.label.set('Mute')
      } else {
        let volume = Math.round(((setting - 3.0) / 38.0) * 100.0)
        this.label.set(volume.toString() + '%')
      }

      y = 19
    } else if (button === 2) {
      this.title.set('Difficulty')
      this.title.moveTo(14, 18)

      if (setting === 3) {
        this.label.set('Easy')
      } else if (setting === 22) {
        this.label.set('Normal')
      } else if (setting === 41) {
        this.label.set('Hard')
      }

      y = 37
    }

    this.label.moveTo(32 - (this.label.width / 2), y)
  }

  toggleVolume (element, event) {
    if (event != null && event.isDown) {
      this.volumeButton.frameName = 'volumeButtonP'
      this.updateLabel(1, this.volumeButton.x)
      this.title.visible = true
      this.label.visible = true
      this.volumeButPattern.alpha = 0.8
      this.volume.active = true
    } else {
      this.volumeButton.frameName = 'volumeButton'
      this.volumeButPattern.alpha = 0.4
      this.title.visible = false
      this.label.visible = false
      this.volume.active = false

      // Value between [0 = Mute, 38 = 100%]
      let volume = Math.round(((this.volumeButton.x - 3.0) / 38.0) * 100.0) / 100.0
      console.log('volume', volume)

      if (volume < 0.01) {
        this.game.sound.mute = true
      } else {
        this.game.sound.mute = false
        this.game.sound.volume = volume
      }
    }
  }

  toggleDiff (element, event) {
    if (event != null && event.isDown) {
      this.diffButton.frameName = 'diffButtonP'
      this.diffButPattern.alpha = 0.8
      this.updateLabel(2, this.diffButton.x)
      this.title.visible = true
      this.label.visible = true
      this.difficulty.active = true
    } else {
      this.diffButton.frameName = 'diffButton'
      this.diffButPattern.alpha = 0.4
      this.title.visible = false
      this.label.visible = false
      this.difficulty.active = false

      // Value between [1,3]
      if (this.diffButton.x === 3) {
        window.option.diff = 1
      } else if (this.diffButton.x === 22) {
        window.option.diff = 2
      } else if (this.diffButton.x === 41) {
        window.option.diff = 3
      }
    }
  }
}
