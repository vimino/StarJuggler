// Copyright (c) 2016-2021, VIMinO
//
// This source code file is under the GPLv3 license, which can be found at:
// https://www.gnu.org/licenses/gpl-3.0.en.html
// Or the attached LICENSE file

import PIXI from 'expose-loader?PIXI!phaser-ce/build/custom/pixi.js' // eslint-disable-line
import p2 from 'expose-loader?p2!phaser-ce/build/custom/p2.js' // eslint-disable-line
import Phaser from 'expose-loader?Phaser!phaser-ce/build/custom/phaser-split.js' // eslint-disable-line

import Label from '../objects/Label'

export default class ShopSlider extends Phaser.Group {
  constructor (game) {
    super(game)

    this.baseUpgrades = {
      'power': 0,
      'structure': 0,
      'skill': 0
    }
    this.upgrades = {
      'power': 0,
      'structure': 0,
      'skill': 0,
      'score': 0,
      'cost': 0,
      'selected': undefined
    }

    this.position.setTo(64, 64)

    this.panel = game.add.group()

    this.panelFrame = this.game.add.sprite(0, 0, 'panel', 'OrangePanel')
    this.panelPattern = this.game.add.sprite(1, 1, 'panel', 'OrangePanelPat1')

    this.panel.add(this.panelFrame)
    this.panel.add(this.panelPattern)
    this.add(this.panel)

    this.upgrade = new Label(this.game, 32, 3, 'MicroFont', 5)
    this.upgrade.label.anchor.setTo(0.5, 0)
    this.upgrade.set('Upgrades:')
    this.add(this.upgrade)

    this.power = this.game.add.group()
    this.add(this.power)

    this.powerBack = this.game.add.sprite(4, 10, 'ui', 'power')
    this.power.add(this.powerBack)

    this.powerFront = this.game.add.sprite(4, 27, 'ui', 'power+')
    this.powerFront.anchor.setTo(0.0, 1.0)
    this.powerFront.crop(new Phaser.Rectangle(0, 0, 17, 0))
    this.add(this.powerFront)

    this.powerOutline = this.game.add.sprite(4, 10, 'ui', 'powerOutline')
    this.powerOutline.visible = false
    this.add(this.powerOutline)

    this.powerBack.inputEnabled = true
    this.powerBack.events.onInputDown.add(this.switchUpgrade, this)

    this.structure = this.game.add.group()
    this.add(this.structure)

    this.structureBack = this.game.add.sprite(23, 10, 'ui', 'structure')
    this.structure.add(this.structureBack)

    this.structureFront = this.game.add.sprite(23, 27, 'ui', 'structure+')
    this.structureFront.anchor.setTo(0.0, 1.0)
    this.structureFront.crop(new Phaser.Rectangle(0, 0, 17, 0))
    this.structure.add(this.structureFront)

    this.structureOutline = this.game.add.sprite(23, 10, 'ui', 'structureOutline')
    this.structureOutline.visible = false
    this.structure.add(this.structureOutline)

    this.structureBack.inputEnabled = true
    this.structureBack.events.onInputDown.add(this.switchUpgrade, this)

    this.skill = this.game.add.group()
    this.add(this.skill)

    this.skillBack = this.game.add.sprite(42, 10, 'ui', 'skill')
    this.skill.add(this.skillBack)

    this.skillFront = this.game.add.sprite(42, 27, 'ui', 'skill+')
    this.skillFront.anchor.setTo(0.0, 1.0)
    this.skillFront.crop(new Phaser.Rectangle(0, 0, 18, 0))
    this.skill.add(this.skillFront)

    this.skillOutline = this.game.add.sprite(42, 10, 'ui', 'skillOutline')
    this.skillOutline.visible = false
    this.skill.add(this.skillOutline)

    this.skillBack.inputEnabled = true
    this.skillBack.events.onInputDown.add(this.switchUpgrade, this)

    this.upgrade = new Label(this.game, 32, 28, 'MicroFont', 5)
    this.upgrade.label.anchor.setTo(0.5, 0)
    this.upgrade.set('')
    this.add(this.upgrade)

    this.lbScore = new Label(this.game, 32, 39, 'MicroFont', 5)
    this.lbScore.label.anchor.setTo(0.5, 0)
    this.lbScore.set('Score:')
    this.add(this.lbScore)

    this.score = new Label(this.game, 32, 47, 'MicroFont', 5) // 47 : 1.0, 0.0
    this.score.label.anchor.setTo(0.5, 0)
    this.score.set('0')
    this.add(this.score)

    this.cost = new Label(this.game, 32, 54, 'MicroFont', 5) // 47 : 1.0, 0.0
    this.cost.label.anchor.setTo(0.5, 0)
    this.cost.set('-0')
    this.add(this.cost)

    this.continueButton = this.game.add.sprite(50, 39, 'ui', 'OrangeGoBut')
    this.add(this.continueButton)

    this.continueButton.inputEnabled = true
    this.continueButton.events.onInputDown.add(this.clickContinue, this)
    this.continueButton.events.onInputUp.add(this.clickContinue, this)
    this.continueButton.events.onInputOut.add(
      () => { this.clickContinue(null) },
      this)

    this.buyButton = this.game.add.sprite(0, 34, 'ui', 'OrangeBuyBut')
    this.add(this.buyButton)

    this.buyButton.inputEnabled = true
    this.buyButton.events.onInputDown.add(this.clickBuy, this)
    this.buyButton.events.onInputUp.add(this.clickBuy, this)
    this.buyButton.events.onInputOut.add(
      () => { this.clickBuy(null) },
      this)

    this.sellButton = this.game.add.sprite(0, 49, 'ui', 'OrangeSellBut')
    this.add(this.sellButton)

    this.sellButton.inputEnabled = true
    this.sellButton.events.onInputDown.add(this.clickSell, this)
    this.sellButton.events.onInputUp.add(this.clickSell, this)
    this.sellButton.events.onInputOut.add(
      () => { this.clickSell(null) },
      this)

    this.modifier = parseInt(500 * window.option.diff)

    this.framePattern = 0

    game.time.events.loop(Phaser.Timer.SECOND / 6, this.animate, this)
  }

  animate () {
    if (!this.active) {
      return
    }

    this.framePattern++
    if (this.framePattern > 6) this.framePattern = 1
    this.panelPattern.frameName = 'OrangePanelPat' + this.framePattern.toString()

    this.wait = 0
  }

  onShow (callback) {
    this.onShow = callback
  }

  show (upgrades) {
    console.log('ShopSlider.show', upgrades)

    this.game.add.tween(this).to({x: 0, y: 0}, 500, Phaser.Easing.Cubic.In, true)
    this.active = true

    this.baseUpgrades.power = upgrades.power
    this.upgrades.power = upgrades.power
    this.baseUpgrades.structure = upgrades.structure
    this.upgrades.structure = upgrades.structure
    this.baseUpgrades.skill = upgrades.skill
    this.upgrades.skill = upgrades.skill

    this.upgrades.score = upgrades.score
    this.upgrades.selected = undefined
    this.upgrades.cost = 0

    if (this.onShow) {
      this.onShow()
    }

    this.switchUpgrade()
    this.updateValues()
  }

  onHide (callback) {
    this.onHide = callback
  }

  hide () {
    console.log('ShopSlider.hide')

    if (this.onHide) {
      let upgs = {
        'power': this.upgrades.power,
        'structure': this.upgrades.structure,
        'skill': this.upgrades.skill,
        'score': this.upgrades.score - this.upgrades.cost
      }
      this.onHide(upgs)
    }
    this.game.add.tween(this).to({x: 64, y: 64}, 500,
    Phaser.Easing.Cubic.Out, true).onComplete.add(() => {
      this.active = false
    }, this)
  }

  calculateCost (currentLevel) {
    // console.log('ShopSlider.calculateCost', currentLevel, parseInt(currentLevel) * this.modifier)
    return (parseInt(currentLevel) * this.modifier)
  }

  switchUpgrade (object) {
    this.powerOutline.visible = false
    this.structureOutline.visible = false
    this.skillOutline.visible = false

    let level = 0
    if (object) {
      this.upgrades.selected = object.frameName
    }

    if (this.upgrades.selected) {
      if (this.upgrades.selected === 'power') {
        this.buyButton.visible = (this.upgrades.power !== 5 &&
          (this.upgrades.cost + this.calculateCost(this.upgrades.power + 1)) <= this.upgrades.score)
        this.sellButton.visible = (this.baseUpgrades.power !== this.upgrades.power)
        this.powerOutline.visible = true
        level = this.upgrades.power
        this.upgrade.set('Power: ' + level.toString())
      } else if (this.upgrades.selected === 'structure') {
        this.buyButton.visible = (this.upgrades.structure !== 5 &&
          (this.upgrades.cost + this.calculateCost(this.upgrades.structure + 1)) <= this.upgrades.score)
        this.sellButton.visible = (this.baseUpgrades.structure !== this.upgrades.structure)
        this.structureOutline.visible = true
        level = this.upgrades.structure
        this.upgrade.set('Structure: ' + level.toString())
      } else if (this.upgrades.selected === 'skill') {
        this.buyButton.visible = (this.upgrades.skill !== 5 &&
          (this.upgrades.cost + this.calculateCost(this.upgrades.skill + 1)) <= this.upgrades.score)
        this.sellButton.visible = (this.baseUpgrades.skill !== this.upgrades.skill)
        this.skillOutline.visible = true
        level = this.upgrades.skill
        this.upgrade.set('Skill: ' + level.toString())
      }
    } else {
      this.upgrade.set('')
      this.buyButton.visible = false
      this.sellButton.visible = false
    }

    this.cost.set((this.upgrades.cost >= 0 ? '-' : '+') + this.upgrades.cost.toString())
  }

  updateValues () {
    let power = Math.floor(this.upgrades.power * 3)
    if (power < 0) { power = 0 }
    if (power > 15) { power = 15 }
    this.powerFront.crop(new Phaser.Rectangle(0, 16 - power, 17, 17))
    if ((this.baseUpgrades.power !== this.upgrades.power) || (this.upgrades.power !== 5 &&
      (this.upgrades.cost + this.calculateCost(this.upgrades.power + 1)) <= this.upgrades.score)) {
      this.powerBack.alpha = 1.0
      this.powerFront.alpha = 1.0
    } else {
      this.powerBack.alpha = 0.5
      this.powerFront.alpha = 0.5
    }

    let structure = Math.floor(this.upgrades.structure * 3)
    if (structure < 0) { structure = 0 }
    if (structure > 15) { structure = 15 }
    this.structureFront.crop(new Phaser.Rectangle(0, 16 - structure, 17, 17))

    // If we can't buy or sell make the Upgrade half-transparent

    if ((this.baseUpgrades.structure !== this.upgrades.structure) || (this.upgrades.structure !== 5 &&
      (this.upgrades.cost + this.calculateCost(this.upgrades.structure + 1)) <= this.upgrades.score)) {
      this.structureBack.alpha = 1.0
      this.structureFront.alpha = 1.0
    } else {
      this.structureBack.alpha = 0.5
      this.structureFront.alpha = 0.5
    }

    let skill = Math.floor(this.upgrades.skill * 3)
    if (skill < 0) { skill = 0 }
    if (skill > 15) { skill = 15 }
    this.skillFront.crop(new Phaser.Rectangle(0, 16 - skill, 18, 17))
    if ((this.baseUpgrades.skill !== this.upgrades.skill) || (this.upgrades.skill !== 5 &&
      (this.upgrades.cost + this.calculateCost(this.upgrades.skill + 1)) <= this.upgrades.score)) {
      this.skillBack.alpha = 1.0
      this.skillFront.alpha = 1.0
    } else {
      this.skillBack.alpha = 0.5
      this.skillFront.alpha = 0.5
    }

    this.score.set(this.upgrades.score.toString())
  }

  clickBuy (element, event) {
    if (event != null && event.isDown) {
      this.buyButton.frameName = 'OrangeBuyButP'
      this.buyButton.cancel = false
      console.log('up')
    } else {
      if (event == null) {
        this.buyButton.cancel = true
      }

      if (!this.buyButton.cancel) {
        if (this.upgrades.selected) {
          if (this.upgrades.selected === 'power' &&
            this.upgrades.power < 5) {
            let cost = this.calculateCost(this.upgrades.power + 1) // ((parseInt(this.upgrades.power) + 1) * this.modifier)
            if ((this.upgrades.cost + cost) <= this.upgrades.score) {
              this.upgrades.cost += cost
              ++this.upgrades.power
            }
          } else if (this.upgrades.selected === 'structure' &&
            this.upgrades.structure < 5) {
            let cost = this.calculateCost(this.upgrades.structure + 1) // ((this.upgrades.structure + 1) * this.modifier)
            if ((this.upgrades.cost + cost) <= this.upgrades.score) {
              this.upgrades.cost += this.calculateCost(this.upgrades.structure + 1) // ((parseInt(this.upgrades.structure) + 1) * this.modifier)
              ++this.upgrades.structure
            }
          } else if (this.upgrades.selected === 'skill' &&
            this.upgrades.skill < 5) {
            let cost = this.calculateCost(this.upgrades.skill + 1) // ((this.upgrades.skill + 1) * this.modifier)
            if ((this.upgrades.cost + cost) <= this.upgrades.score) {
              this.upgrades.cost += cost // ((parseInt(this.upgrades.skill) + 1) * this.modifier)
              ++this.upgrades.skill
            }
          }
        }

        this.switchUpgrade()
        this.updateValues()
      }

      this.buyButton.frameName = 'OrangeBuyBut'
    }
  }

  clickSell (element, event) {
    if (event != null && event.isDown) {
      this.sellButton.frameName = 'OrangeSellButP'
      this.sellButton.cancel = false
    } else {
      if (event == null) {
        this.sellButton.cancel = true
      }

      if (!this.sellButton.cancel) {
        if (this.upgrades.selected) {
          if (this.upgrades.selected === 'power' &&
            this.upgrades.power > this.baseUpgrades.power) {
            let cost = this.calculateCost(this.upgrades.power) // Math.floor((parseInt(this.upgrades.power)) * this.modifier)
            this.upgrades.cost -= cost
            --this.upgrades.power
          } else if (this.upgrades.selected === 'structure' &&
            this.upgrades.structure > this.baseUpgrades.structure) {
            this.upgrades.cost -= this.calculateCost(this.upgrades.structure) // Math.floor((parseInt(this.upgrades.structure)) * this.modifier)
            --this.upgrades.structure
          } else if (this.upgrades.selected === 'skill' &&
            this.upgrades.skill > this.baseUpgrades.skill) {
            this.upgrades.cost -= this.calculateCost(this.upgrades.skill) // Math.floor((parseInt(this.upgrades.skill)) * this.modifier)
            --this.upgrades.skill
          }
          this.switchUpgrade()
          this.updateValues()
        }
      }

      this.sellButton.frameName = 'OrangeSellBut'
    }
  }

  clickContinue (element, event) {
    if (event != null && event.isDown) {
      this.continueButton.frameName = 'OrangeGoButP'
      this.continueButton.cancel = false
    } else {
      if (event == null) {
        this.continueButton.cancel = true
      }

      if (this.continueButton.cancel) {
        this.continueButton.frameName = 'OrangeGoBut'
      } else {
        this.hide()
      }
    }
  }
}
