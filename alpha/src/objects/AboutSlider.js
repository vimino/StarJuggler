// Copyright (c) 2016-2021, VIMinO
//
// This source code file is under the GPLv3 license, which can be found at:
// https://www.gnu.org/licenses/gpl-3.0.en.html
// Or the attached LICENSE file

import PIXI from 'expose-loader?PIXI!phaser-ce/build/custom/pixi.js' // eslint-disable-line
import p2 from 'expose-loader?p2!phaser-ce/build/custom/p2.js' // eslint-disable-line
import Phaser from 'expose-loader?Phaser!phaser-ce/build/custom/phaser-split.js' // eslint-disable-line

import Label from '../objects/Label'

export default class OptionSlider extends Phaser.Group {
  constructor (game) {
    super(game)

    this.position.setTo(-64, -64)

    this.panel = game.add.group()

    this.panelFrame = this.game.add.sprite(0, 0, 'panel', 'GreenPanel')
    this.panelPattern = this.game.add.sprite(1, 1, 'panel', 'GreenPanelPat1')

    this.panel.add(this.panelFrame)
    this.panel.add(this.panelPattern)
    this.add(this.panel)

    this.closeButton = this.game.add.sprite(0, 0, 'ui', 'GreenBackBut')
    this.add(this.closeButton)

    this.closeButton.inputEnabled = true
    this.closeButton.events.onInputDown.add(this.toggleClose, this)
    this.closeButton.events.onInputUp.add(this.toggleClose, this)
    this.closeButton.events.onInputOut.add(
      () => { this.toggleClose(null) },
      this)

    this.by = new Label(this.game, 18, 8, 'MicroFont', 5)
    this.by.setAlpha(0.8)
    this.by.set('Made By')
    this.add(this.by)

    this.vimino = this.game.add.sprite(2, 16, 'vimino') // , 60, 30)
    this.add(this.vimino)

    this.website = new Label(this.game, 4, 47, 'MicroFont', 5)
    this.website.setAlpha(0.6)
    this.website.set('www.vimino.net')
    this.add(this.website)

    this.frame = 0
    this.framePattern = 0

    game.time.events.loop(Phaser.Timer.SECOND / 6, this.animate, this)
  }

  update () {}

  animate () {
    if (!this.active) {
      return
    }

    this.frame++
    this.framePattern++
    if (this.frame > 6) this.frame = 0
    if (this.framePattern > 6) this.framePattern = 1

    this.vimino.frame = this.frame
    this.panelPattern.frameName = 'GreenPanelPat' + this.framePattern.toString()

    this.wait = 0
  }

  onShow (callback) {
    this.onShow = callback
  }

  show () {
    if (this.onShow) {
      this.onShow()
    }

    this.game.add.tween(this).to({x: 0, y: 0}, 500, Phaser.Easing.Cubic.In, true)
    this.active = true
  }

  onHide (callback) {
    this.onHide = callback
  }

  hide () {
    if (this.onHide) {
      this.onHide()
    }

    let tween = this.game.add.tween(this).to({x: -64, y: -64}, 500, Phaser.Easing.Cubic.Out, true)
    tween.onComplete.add(() => {
      this.active = false
    }, this)
  }

  toggleClose (element, event) {
    if (event != null && event.isDown) {
      this.closeButton.frameName = 'GreenBackButP'
      this.closeButton.cancel = false
    } else {
      if (event == null) {
        this.closeButton.cancel = true
      }

      if (this.closeButton.cancel) {
        this.closeButton.frameName = 'GreenBackBut'
      } else {
        this.hide()
      }
    }
  }
}
